package BackEnd.Translator.NASM.NASMTranslator;

import BackEnd.Allocator.PhysicalRegister;
import BackEnd.ControlFlowGraph.Block;
import BackEnd.ControlFlowGraph.Graph;
import BackEnd.ControlFlowGraph.Instruction.ArithmeticInstruction.ArithmeticInstruction;
import BackEnd.ControlFlowGraph.Instruction.ArithmeticInstruction.BinaryInstruction.*;
import BackEnd.ControlFlowGraph.Instruction.ArithmeticInstruction.UnaryInstruction.BitnotInstruction;
import BackEnd.ControlFlowGraph.Instruction.ArithmeticInstruction.UnaryInstruction.UnaryInstruction;
import BackEnd.ControlFlowGraph.Instruction.ControlFlowInstruction.BranchInstruction;
import BackEnd.ControlFlowGraph.Instruction.ControlFlowInstruction.ControlFlowInstruction;
import BackEnd.ControlFlowGraph.Instruction.ControlFlowInstruction.JumpInstruction;
import BackEnd.ControlFlowGraph.Instruction.FunctionInstruction.CallInstruction;
import BackEnd.ControlFlowGraph.Instruction.FunctionInstruction.ReturnInstruction;
import BackEnd.ControlFlowGraph.Instruction.Instruction;
import BackEnd.ControlFlowGraph.Instruction.MemoryInstruction.*;
import BackEnd.ControlFlowGraph.Operand.Address;
import BackEnd.ControlFlowGraph.Operand.ImmediateValue;
import BackEnd.ControlFlowGraph.Operand.Operand;
import BackEnd.ControlFlowGraph.Operand.VirtualRegister.StringRegister;
import BackEnd.ControlFlowGraph.Operand.VirtualRegister.VariableRegister.GlobalRegister;
import BackEnd.ControlFlowGraph.Operand.VirtualRegister.VariableRegister.ParameterRegister;
import BackEnd.ControlFlowGraph.Operand.VirtualRegister.VariableRegister.TemporaryRegister;
import BackEnd.ControlFlowGraph.Operand.VirtualRegister.VariableRegister.VariableRegister;
import BackEnd.ControlFlowGraph.Operand.VirtualRegister.VirtualRegister;
import BackEnd.Translator.NASM.NASMRegister;
import FrontEnd.AST.Function;
import Utility.Error.InternalError;
import jdk.nashorn.internal.codegen.CompilerConstants;

import java.io.PrintStream;

public class NASMNaiveTranslator extends NASMTranslator{
    private Graph graph;

    public NASMNaiveTranslator(PrintStream output) {
        super(output);
    }

    private void load(Operand from,PhysicalRegister to){
        if (from instanceof ImmediateValue) {
            output.printf("\tmov\t%s, %s\n",to,from);
        }else if(from instanceof VirtualRegister){
            if (from instanceof StringRegister) {
                output.printf("\tmov\t%s, %s\n",to,((StringRegister)from).message);
            }else if (from instanceof VariableRegister) {
                if (from instanceof GlobalRegister) {
                    output.printf("\tmov\t%s, qword [rel %s]\n",to,getGlobalVariableName(from));
                } else if (from instanceof TemporaryRegister) {
                    output.printf("\tmov\t%s, qword [rsp + %d]\n",to,graph.frame.getOffset(from));
                } else if (from instanceof ParameterRegister) {
                    output.printf("\tmov\t%s, qword [rsp + %d]\n",to,graph.frame.getOffset(from));
                }
            }
        }else if (from instanceof Address){
            load(((Address)from).base,NASMRegister.rax);
            output.printf("\tmov\t%s, qword [rax + %d]\n",to,((Address)from).offset.value);
        }
    }

    private void store(VirtualRegister from,PhysicalRegister to){
        if (from instanceof StringRegister){
            throw new InternalError();
        }else if (from instanceof VariableRegister){
            if (from instanceof GlobalRegister){
                output.printf("\tmov\tqword [rel %s], %s\n",getGlobalVariableName(from),to);
            }else if (from instanceof TemporaryRegister){
                output.printf("\tmov\tqword [rsp + %d], %s\n",graph.frame.getOffset(from),to);
            }else if (from instanceof ParameterRegister){
                output.printf("\tmov\tqword [rsp + %d], %s\n",graph.frame.getOffset(from),to);
            }
        }
    }

    @Override
    public void translate(Graph graph){
        this.graph = graph;
        output.printf("%s:\n",graph.function.name);
        output.printf("\tpush\trbp\n");
        output.printf("\tmov\trbp, rsp\n");
        int rspchange = graph.frame.size;
        output.printf("\tsub\trsp, %d\n",graph.frame.size);
        int whether_return = 0;
        for (int k = 0;k < graph.blocks.size();k ++){
            Block block = graph.blocks.get(k);
            output.printf("%s:\n",getBlockName(block));
            for (Instruction instruction : block.instructions){
                if (instruction instanceof ArithmeticInstruction){
                    if (instruction instanceof UnaryInstruction){
                        UnaryInstruction i = (UnaryInstruction)instruction;
                        load(i.source,NASMRegister.rax);
                        if (i instanceof BitnotInstruction){
                            output.printf("\tnot\trax\n");
                        }else{
                            output.printf("\tneg\trax\n");
                        }
                        store(i.destination,NASMRegister.rax);
                    }else if (instruction instanceof BinaryInstruction){
                        BinaryInstruction i = (BinaryInstruction)instruction;
                        load(i.source1,NASMRegister.rax);
                        load(i.source2,NASMRegister.rcx);
                        if (i instanceof AddInstruction){
                            output.printf("\tadd\trax, rcx\n");
                        }else if (i instanceof BitandInstruction){
                            output.printf("\tand\trax, rcx\n");
                        }else if (i instanceof BitorInstruction){
                            output.printf("\tor\trax, rcx\n");
                        }else if (i instanceof BitxorInstruction){
                            output.printf("\txor\trax, rcx\n");
                        }else if (i instanceof DivideInstruction){
                            output.printf("\tcqo\n");
                            output.printf("\tidiv\trcx\n");
                        }else if (i instanceof EqualInstruction){
                            output.printf("\tcmp\trax, rcx\n");
                            output.printf("\tsete\tal\n");
                            output.printf("\tmovzx\trax, al\n");
                        }else if (i instanceof GreaterInstruction){
                            output.printf("\tcmp\trax, rcx\n");
                            output.printf("\tsetg\tal\n");
                            output.printf("\tmovzx\trax, al\n");
                        }else if (i instanceof LessInstruction){
                            output.printf("\tcmp\trax, rcx\n");
                            output.printf("\tsetl\tal\n");
                            output.printf("\tmovzx\trax, al\n");
                        }else if (i instanceof MinusInstruction){
                            output.printf("\tsub\trax, rcx\n");
                        }else if (i instanceof ModInstruction){
                            output.printf("\tcqo\n");
                            output.printf("\tidiv\trcx\n");
                            output.printf("\tmov\trax, rdx\n");
                        }else if (i instanceof MultiplyInstruction){
                            output.printf("\timul\trax, rcx\n");
                        }else if (i instanceof NotequalInstruction){
                            output.printf("\tcmp\trax, rcx\n");
                            output.printf("\tsetne\tal\n");
                            output.printf("\tmovzx\trax, al\n");
                        }else if (i instanceof NotgreaterInstruction){
                            output.printf("\tcmp\trax, rcx\n");
                            output.printf("\tsetle\tal\n");
                            output.printf("\tmovzx\trax, al\n");
                        }else if (i instanceof NotlessInstruction){
                            output.printf("\tcmp\trax, rcx\n");
                            output.printf("\tsetge\tal\n");
                            output.printf("\tmovzx\trax, al\n");
                        }else if (i instanceof ShiftleftInstruction){
                            output.printf("\tshl\trax, cl\n");
                        }else if (i instanceof ShiftrightInstruction){
                            output.printf("\tsar\trax, cl\n");
                        }
                        store(i.destination,NASMRegister.rax);
                    }
                }else if (instruction instanceof ControlFlowInstruction){
                    if (instruction instanceof BranchInstruction){
                        BranchInstruction i = (BranchInstruction)instruction;
                        load(i.condition,NASMRegister.rax);
                        output.printf("\tcmp\trax, 0\n");
                        output.printf("\tjz\t%s\n",getBlockName(i.falseto.block));
                        if (k + 1 == graph.blocks.size() || graph.blocks.get(k + 1) != i.trueto.block){
                            output.printf("\tjmp\t%s\n",getBlockName(i.trueto.block));
                        }
                    }else if (instruction instanceof JumpInstruction){
                        JumpInstruction i = (JumpInstruction)instruction;
                        if (k + 1 == graph.blocks.size() || graph.blocks.get(k + 1) != i.to.block){
                            output.printf("\tjmp\t%s\n",getBlockName(i.to.block));
                        }
                    }
                }else if (instruction instanceof CallInstruction){
                    CallInstruction i = (CallInstruction)instruction;
                    Function function = i.function;
                    if (function.name.startsWith("_builtin")){
                        if (i.parameters.size() >= 1) {
                            load(i.parameters.get(0),NASMRegister.rdi);
                        }
                        if (i.parameters.size() >= 2) {
                            load(i.parameters.get(1),NASMRegister.rsi);
                        }
                        if (i.parameters.size() >= 3) {
                            load(i.parameters.get(2),NASMRegister.rdx);
                        }
                    }else{
                        //output.printf("\tsub\trsp, %d\n",rspchange);
                        for (int p = 0; p < function.parameters.size(); ++p) {
                            load(i.parameters.get(p),NASMRegister.rax);
                            int offset = 16 + function.graph.frame.size - function.graph.frame.getOffset(function.parameters.get(p).register);
                            if (rspchange % 16 == 8) offset += 8;
                            output.printf("\tmov\t[rsp - %d], rax\n",offset);
                        }
                        //output.printf("\tadd\trsp, %d\n",rspchange);
                    }
                    if (rspchange % 16 == 8){
                        output.printf("\tsub\trsp, 8\n");
                    }
                    output.printf("\tcall %s\n",getFunctionName(function));
                    if (rspchange % 16 == 8){
                        output.printf("\tadd\trsp, 8\n");
                    }
                    if (i.destination != null) {
                        store(i.destination,NASMRegister.rax);
                    }
                }else if (instruction instanceof ReturnInstruction){
                    ReturnInstruction i = (ReturnInstruction)instruction;
                    whether_return = 1;
                    load(i.source,NASMRegister.rax);
                    output.printf("\tjmp\t%s\n",getBlockName(graph.exit));
                }else if (instruction instanceof MemoryInstruction){
                    if (instruction instanceof MoveInstruction){
                        MoveInstruction i = (MoveInstruction)instruction;
                        load(i.source,NASMRegister.rax);
                        store(i.destination,NASMRegister.rax);
                    }else if (instruction instanceof LoadInstruction){
                        LoadInstruction i = (LoadInstruction)instruction;
                        load(i.address.base,NASMRegister.rax);
                        output.printf("\tmov\trcx, [rax + %d]\n",i.address.offset.value);
                        store(i.destination,NASMRegister.rcx);
                    }else if (instruction instanceof StoreInstruction){
                        StoreInstruction i = (StoreInstruction)instruction;
                        load(i.source,NASMRegister.rax);
                        load(i.address.base,NASMRegister.rcx);
                        output.printf("\tmov\t[rcx + %d], rax\n",i.address.offset.value);
                    }else if (instruction instanceof AllocateInstruction){
                        AllocateInstruction i = (AllocateInstruction)instruction;
                        load(i.size,NASMRegister.rdi);
                        output.printf("\tcall\tmalloc\n");
                        store(i.destination,NASMRegister.rax);
                    }
                }
            }
        }
        output.printf("\tadd\trsp, %d\n",graph.frame.size);
        output.printf("\tpop\trbp\n");
        if (whether_return == 0 && graph.function.name.equals("main")){
            output.printf("\txor\trax, rax\n");
            output.printf("\tmov\trax, 0\n");
        }
        output.printf("\tret\n");
    }
}