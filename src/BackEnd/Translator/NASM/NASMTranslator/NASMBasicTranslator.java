package BackEnd.Translator.NASM.NASMTranslator;

import BackEnd.Allocator.Allocator;
import BackEnd.Allocator.PhysicalRegister;
import BackEnd.ControlFlowGraph.Block;
import BackEnd.ControlFlowGraph.Graph;
import BackEnd.ControlFlowGraph.Instruction.ArithmeticInstruction.ArithmeticInstruction;
import BackEnd.ControlFlowGraph.Instruction.ArithmeticInstruction.BinaryInstruction.*;
import BackEnd.ControlFlowGraph.Instruction.ArithmeticInstruction.UnaryInstruction.BitnotInstruction;
import BackEnd.ControlFlowGraph.Instruction.ArithmeticInstruction.UnaryInstruction.UnaryInstruction;
import BackEnd.ControlFlowGraph.Instruction.ControlFlowInstruction.BranchInstruction;
import BackEnd.ControlFlowGraph.Instruction.ControlFlowInstruction.ControlFlowInstruction;
import BackEnd.ControlFlowGraph.Instruction.ControlFlowInstruction.JumpInstruction;
import BackEnd.ControlFlowGraph.Instruction.FunctionInstruction.CallInstruction;
import BackEnd.ControlFlowGraph.Instruction.FunctionInstruction.FunctionInstruction;
import BackEnd.ControlFlowGraph.Instruction.FunctionInstruction.ReturnInstruction;
import BackEnd.ControlFlowGraph.Instruction.Instruction;
import BackEnd.ControlFlowGraph.Instruction.MemoryInstruction.*;
import BackEnd.ControlFlowGraph.Operand.Address;
import BackEnd.ControlFlowGraph.Operand.ImmediateValue;
import BackEnd.ControlFlowGraph.Operand.Operand;
//import BackEnd.ControlFlowGraph.Operand.VirtualRegister.CloneRegister;
import BackEnd.ControlFlowGraph.Operand.VirtualRegister.StringRegister;
import BackEnd.ControlFlowGraph.Operand.VirtualRegister.VariableRegister.GlobalRegister;
import BackEnd.ControlFlowGraph.Operand.VirtualRegister.VariableRegister.ParameterRegister;
import BackEnd.ControlFlowGraph.Operand.VirtualRegister.VariableRegister.TemporaryRegister;
import BackEnd.ControlFlowGraph.Operand.VirtualRegister.VariableRegister.VariableRegister;
import BackEnd.ControlFlowGraph.Operand.VirtualRegister.VirtualRegister;
import BackEnd.Translator.NASM.NASMRegister;
import FrontEnd.AST.Expression.BinaryExpression.NotgreaterExpression;
import FrontEnd.AST.Function;
import Utility.Error.InternalError;

import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;

public class NASMBasicTranslator extends NASMTranslator{
    private Graph graph;
    private Allocator allocator;
    public int rspchange;

    public NASMBasicTranslator(PrintStream output) {
        super(output);
    }

    // from -> to
    private PhysicalRegister loadToRead(Operand from, PhysicalRegister to) {
        if (from instanceof VirtualRegister) {
            if (from instanceof VariableRegister) {
                if (from instanceof GlobalRegister) {
                    output.printf("\tmov\t%s, qword [rel %s]\n", to,getGlobalVariableName(from));
                } else if (from instanceof ParameterRegister) {
                    output.printf("\tmov\t%s, qword [rsp + %d]\n",to,graph.frame.getOffset(from));
                } else if (from instanceof TemporaryRegister) {
                    PhysicalRegister register = allocator.mapping.get(from);
                    if (register != null) {
                        return register;
                    } else {
                        output.printf("\tmov\t%s, qword [rsp + %d]\n",to,graph.frame.getOffset(from));
                    }
                } else {
                    throw new InternalError();
                }
            } else if (from instanceof StringRegister) {
                output.printf("\tmov\t%s, %s\n",to,getStringConstantName(from));
            } else {
                throw new InternalError();
            }
        } else if (from instanceof Address) {
            throw new InternalError();
        } else if (from instanceof ImmediateValue) {
            output.printf("\tmov\t%s, %s\n",to,from);
        } else {
            throw new InternalError();
        }
        return to;
    }

    private PhysicalRegister loadToWrite(VirtualRegister from, PhysicalRegister to) {
        if (from instanceof TemporaryRegister) {
            PhysicalRegister register = allocator.mapping.get(from);
            if (register != null) {
                return register;
            }
        }
        return to;
    }
    // to -> from
    private void store(VirtualRegister from, PhysicalRegister to) {
        if (from instanceof VariableRegister) {
            if (from instanceof GlobalRegister) {
                output.printf("\tmov\tqword [rel %s], %s\n",getGlobalVariableName(from),to);
            } else if (from instanceof ParameterRegister) {
                output.printf("\tmov\tqword [rsp + %d], %s\n",graph.frame.getOffset(from),to);
            } else if (from instanceof TemporaryRegister) {
                PhysicalRegister register = allocator.mapping.get(from);
                if (register == null) {
                    output.printf("\tmov\tqword [rsp + %d], %s\n",graph.frame.getOffset(from),to);
                }
            } else {
                throw new InternalError();
            }
        } else if (from instanceof StringRegister) {
            throw new InternalError();
        } else {
            throw new InternalError();
        }
    }
    // from -> to
    private void move(Operand from, PhysicalRegister to) {
        if (from instanceof VirtualRegister) {
            if (from instanceof VariableRegister) {
                if (from instanceof GlobalRegister) {
                    output.printf("\tmov\t%s, qword [rel %s]\n",to,getGlobalVariableName(from));
                } else if (from instanceof ParameterRegister) {
                    output.printf("\tmov\t%s, qword [rsp + %d]\n",to, graph.frame.getOffset(from));
                } else if (from instanceof TemporaryRegister) {
                    PhysicalRegister register = allocator.mapping.get(from);
                    if (register != null) {
                        if (register != to) {
                            output.printf("\tmov\t%s, %s\n",to,register);
                        }
                    } else {
                        output.printf("\tmov\t%s, qword [rsp + %d]\n",to,graph.frame.getOffset(from));
                    }
                } else {
                    throw new InternalError();
                }
            } else if (from instanceof StringRegister) {
                output.printf("\tmov\t%s, %s\n",to,getStringConstantName(from));
            } else {
                throw new InternalError();
            }
        } else if (from instanceof ImmediateValue) {
            output.printf("\tmov\t%s, %s\n",to,from);
        } else {
            throw new InternalError();
        }
    }
    // from -> to
    private void move(PhysicalRegister from, VirtualRegister to) {
        if (to instanceof VariableRegister) {
            if (to instanceof GlobalRegister) {
                output.printf("\tmov\tqword [rel %s], %s\n",getGlobalVariableName(to),from);
            } else if (to instanceof ParameterRegister) {
                output.printf("\tmov\tqword [rsp + %d], %s\n",graph.frame.getOffset(to), from);
            } else if (to instanceof TemporaryRegister) {
                PhysicalRegister register = allocator.mapping.get(to);
                if (register != null) {
                    if (register != from) {
                        output.printf("\tmov\t%s, %s\n",register,from);
                    }
                } else {
                    output.printf("\tmov\tqword [rsp + %d], %s\n",graph.frame.getOffset(to),from);
                }
            } else {
                throw new InternalError();
            }
        } else if (to instanceof StringRegister) {
            throw new InternalError();
        } else {
            throw new InternalError();
        }
    }

    @Override
    public void translate(Graph graph) {
        this.graph = graph;
        this.allocator = graph.function.allocator;
        output.printf("%s:\n",graph.function.name);
        output.printf("\tpush\trbp\n");
        output.printf("\tmov\trbp, rsp\n");
        rspchange = 8;
        callee_save_push();
        rspchange += graph.frame.size;
        output.printf("\tsub\trsp, %d\n",graph.frame.size);
        graph.refresh();
        int whether_return = 0;
        for (int k = 0;k < graph.blocks.size();k ++){
            Block block = graph.blocks.get(k);
            output.printf("%s:\n",getBlockName(block));

            Instruction reserved = null;
            for (int l = 0;l < block.instructions.size();l ++){
                Instruction instruction = block.instructions.get(l);
                if (instruction instanceof ArithmeticInstruction){
                    if (instruction instanceof BinaryInstruction){
                        BinaryInstruction i = (BinaryInstruction)instruction;
                        if (l + 1 < block.instructions.size() && block.instructions.get(l + 1) instanceof BranchInstruction) {
                            BranchInstruction j = (BranchInstruction)block.instructions.get(l + 1);
                            if (j.condition == i.destination && !block.liveliness.liveOut.contains(i.destination)) {
                                reserved = instruction;
                                continue;
                            }
                        }
                        if (i.source2 instanceof ImmediateValue){
                            PhysicalRegister a = loadToRead(i.source1,NASMRegister.temporary1);
                            PhysicalRegister c = loadToWrite(i.destination,NASMRegister.temporary2);
                            if (i instanceof AddInstruction){
                                output.printf("\tmov\t%s, %s\n",c,a);
                                output.printf("\tadd\t%s, %s\n",c,i.source2);
                            }
                            if (i instanceof BitandInstruction){
                                output.printf("\tmov\t%s, %s\n",c,a);
                                output.printf("\tand\t%s, %s\n",c,i.source2);
                            }
                            if (i instanceof ShiftleftInstruction){
                                output.printf("\tmov\t%s, %s\n",c,a);
                                output.printf("\tmov\t%s, %s\n",NASMRegister.rcx,i.source2);
                                output.printf("\tshl\t%s, cl\n",c);
                            }
                            if (i instanceof BitorInstruction){
                                output.printf("\tmov\t%s, %s\n",c,a);
                                output.printf("\tor\t%s, %s\n",c,i.source2);
                            }
                            if (i instanceof ShiftrightInstruction) {
                                output.printf("\tmov\t%s, %s\n", c, a);
                                output.printf("\tmov\t%s, %s\n", NASMRegister.rcx, i.source2);
                                output.printf("\tsar\t%s, cl\n", c);
                            }
                            if (i instanceof BitxorInstruction) {
                                output.printf("\tmov\t%s, %s\n", c, a);
                                output.printf("\txor\t%s, %s\n", c, i.source2);
                            }
                            if (i instanceof DivideInstruction) {
                                output.printf("\tmov\t%s, %s\n", NASMRegister.rax, a);
                                output.printf("\txor\t%s, %s\n", NASMRegister.rdx, NASMRegister.rdx);
                                output.printf("\tcqo\n");
                                output.printf("\tmov\t%s, %s\n", c, i.source2);
                                output.printf("\tidiv\t%s\n", c);
                                output.printf("\tmov\t%s, %s\n", c, NASMRegister.rax);
                            }
                            if (i instanceof EqualInstruction) {
                                output.printf("\tcmp\t%s, %s\n", a, i.source2);
                                output.printf("\tsete\tal\n");
                                output.printf("\tmovzx\t%s, al\n", c);
                            }
                            if (i instanceof NotlessInstruction) {
                                output.printf("\tcmp\t%s, %s\n", a, i.source2);
                                output.printf("\tsetge\tal\n");
                                output.printf("\tmovzx\t%s, al\n", c);
                            }
                            if (i instanceof GreaterInstruction) {
                                output.printf("\tcmp\t%s, %s\n", a, i.source2);
                                output.printf("\tsetg\tal\n");
                                output.printf("\tmovzx\t%s, al\n", c);
                            }
                            if (i instanceof NotgreaterInstruction) {
                                output.printf("\tcmp\t%s, %s\n", a, i.source2);
                                output.printf("\tsetle\tal\n");
                                output.printf("\tmovzx\t%s, al\n", c);
                            }
                            if (i instanceof LessInstruction) {
                                output.printf("\tcmp\t%s, %s\n", a, i.source2);
                                output.printf("\tsetl\tal\n");
                                output.printf("\tmovzx\t%s, al\n", c);
                            }
                            if (i instanceof MinusInstruction) {
                                output.printf("\tmov\t%s, %s\n", c, a);
                                output.printf("\tsub\t%s, %s\n", c, i.source2);
                            }
                            if (i instanceof ModInstruction) {
                                output.printf("\tmov\t%s, %s\n", NASMRegister.rax, a);
                                output.printf("\txor\t%s, %s\n", NASMRegister.rdx, NASMRegister.rdx);
                                output.printf("\tcqo\n");
                                output.printf("\tmov\t%s, %s\n", c, i.source2);
                                output.printf("\tidiv\t%s\n", c);
                                output.printf("\tmov\t%s, %s\n", c, NASMRegister.rdx);
                            }
                            if (i instanceof MultiplyInstruction) {
                                output.printf("\tmov\t%s, %s\n", c, a);
                                output.printf("\timul\t%s, %s\n", c, i.source2);
                            }
                            if (i instanceof NotequalInstruction) {
                                output.printf("\tcmp\t%s, %s\n", a, i.source2);
                                output.printf("\tsetne\tal\n");
                                output.printf("\tmovzx\t%s, al\n", c);
                            }
                            store(i.destination, c);
                        }else{
                            PhysicalRegister a = loadToRead(i.source1, NASMRegister.temporary1);
                            PhysicalRegister b = loadToRead(i.source2, NASMRegister.temporary2);
                            PhysicalRegister c = loadToWrite(i.destination, NASMRegister.temporary2);
                            if (i instanceof AddInstruction) {
                                output.printf("\tlea\t%s, [%s + %s]\n", c, a, b);
                            }
                            if (i instanceof BitandInstruction) {
                                output.printf("\tmov\t%s, %s\n", c, a);
                                output.printf("\tand\t%s, %s\n", c, b);
                            }
                            if (i instanceof ShiftleftInstruction) {
                                output.printf("\tmov\t%s, %s\n", c, a);
                                output.printf("\tmov\t%s, %s\n", NASMRegister.rcx, b);
                                output.printf("\tshl\t%s, cl\n", c);
                            }
                            if (i instanceof BitorInstruction) {
                                output.printf("\tmov\t%s, %s\n", c, a);
                                output.printf("\tor\t%s, %s\n", c, b);
                            }
                            if (i instanceof ShiftrightInstruction) {
                                output.printf("\tmov\t%s, %s\n", c, a);
                                output.printf("\tmov\t%s, %s\n", NASMRegister.rcx, b);
                                output.printf("\tsar\t%s, cl\n", c);
                            }
                            if (i instanceof BitxorInstruction) {
                                output.printf("\tmov\t%s, %s\n", c, a);
                                output.printf("\txor\t%s, %s\n", c, b);
                            }
                            if (i instanceof DivideInstruction) {
                                output.printf("\tmov\t%s, %s\n", NASMRegister.rax, a);
                                output.printf("\txor\t%s, %s\n", NASMRegister.rdx, NASMRegister.rdx);
                                output.printf("\tcqo\n");
                                output.printf("\tmov\t%s, %s\n", c, b);
                                output.printf("\tidiv\t%s\n", c);
                                output.printf("\tmov\t%s, %s\n", c, NASMRegister.rax);
                            }
                            if (i instanceof EqualInstruction) {
                                output.printf("\tcmp\t%s, %s\n", a, b);
                                output.printf("\tsete\tal\n");
                                output.printf("\tmovzx\t%s, al\n", c);
                            }
                            if (i instanceof NotlessInstruction) {
                                output.printf("\tcmp\t%s, %s\n", a, b);
                                output.printf("\tsetge\tal\n");
                                output.printf("\tmovzx\t%s, al\n", c);
                            }
                            if (i instanceof GreaterInstruction) {
                                output.printf("\tcmp\t%s, %s\n", a, b);
                                output.printf("\tsetg\tal\n");
                                output.printf("\tmovzx\t%s, al\n", c);
                            }
                            if (i instanceof NotgreaterInstruction) {
                                output.printf("\tcmp\t%s, %s\n", a, b);
                                output.printf("\tsetle\tal\n");
                                output.printf("\tmovzx\t%s, al\n", c);
                            }
                            if (i instanceof LessInstruction) {
                                output.printf("\tcmp\t%s, %s\n", a, b);
                                output.printf("\tsetl\tal\n");
                                output.printf("\tmovzx\t%s, al\n", c);
                            }
                            if (i instanceof MinusInstruction) {
                                output.printf("\tmov\t%s, %s\n", c, a);
                                output.printf("\tsub\t%s, %s\n", c, b);
                            }
                            if (i instanceof ModInstruction) {
                                output.printf("\tmov\t%s, %s\n", NASMRegister.rax, a);
                                output.printf("\txor\t%s, %s\n", NASMRegister.rdx, NASMRegister.rdx);
                                output.printf("\tcqo\n");
                                output.printf("\tmov\t%s, %s\n", c, b);
                                output.printf("\tidiv\t%s\n", c);
                                output.printf("\tmov\t%s, %s\n", c, NASMRegister.rdx);
                            }
                            if (i instanceof MultiplyInstruction) {
                                output.printf("\tmov\t%s, %s\n", c, a);
                                output.printf("\timul\t%s, %s\n", c, b);
                            }
                            if (i instanceof NotequalInstruction) {
                                output.printf("\tcmp\t%s, %s\n", a, b);
                                output.printf("\tsetne\tal\n");
                                output.printf("\tmovzx\t%s, al\n", c);
                            }
                            store(i.destination, c);
                        }
                    }else if (instruction instanceof UnaryInstruction){
                        UnaryInstruction i = (UnaryInstruction)instruction;
                        PhysicalRegister a = loadToRead(i.source,NASMRegister.temporary1);
                        PhysicalRegister b = loadToWrite(i.destination,NASMRegister.temporary2);
                        PrintUnaryInstruction(i,b,a);
                        store(i.destination, b);
                    }else{
                        throw new InternalError();
                    }
                }else if (instruction instanceof ControlFlowInstruction){
                    if (instruction instanceof BranchInstruction) {
                        BranchInstruction i = (BranchInstruction)instruction;
                        if (reserved == null) {
                            PhysicalRegister a = loadToRead(i.condition,NASMRegister.temporary1);
                            output.printf("\tcmp\t%s, 0\n",a);
                            output.printf("\tje\t%s\n",getBlockName(i.falseto.block));
                        } else {
                            BinaryInstruction j = (BinaryInstruction)reserved;
                            if (j instanceof BitxorInstruction) {
                                PhysicalRegister a = loadToRead(j.source1,NASMRegister.temporary1);
                                output.printf("\tcmp\t%s, 0\n",a);
                                output.printf("\tjne\t%s\n",getBlockName(i.falseto.block));
                            } else {
                                PhysicalRegister a = loadToRead(j.source1, NASMRegister.temporary1);
                                PhysicalRegister b = loadToRead(j.source2, NASMRegister.temporary2);
                                output.printf("\tcmp\t%s, %s\n", a, b);
                                if (j instanceof EqualInstruction) {
                                    output.printf("\tjne\t%s\n", getBlockName(i.falseto.block));
                                } else if (j instanceof GreaterInstruction) {
                                    output.printf("\tjle\t%s\n", getBlockName(i.falseto.block));
                                } else if (j instanceof NotlessInstruction) {
                                    output.printf("\tjl\t%s\n", getBlockName(i.falseto.block));
                                } else if (j instanceof LessInstruction) {
                                    output.printf("\tjge\t%s\n", getBlockName(i.falseto.block));
                                } else if (j instanceof NotgreaterInstruction) {
                                    output.printf("\tjg\t%s\n", getBlockName(i.falseto.block));
                                } else if (j instanceof NotequalInstruction) {
                                    output.printf("\tje\t%s\n", getBlockName(i.falseto.block));
                                }
                            }
                            reserved = null;
                        }
                        if (k + 1 == graph.blocks.size() || graph.blocks.get(k + 1) != i.trueto.block) {
                            output.printf("\tjmp\t%s\n",getBlockName(i.trueto.block));
                        }
                    } else if (instruction instanceof JumpInstruction) {
                        JumpInstruction i = (JumpInstruction)instruction;
                        if (k + 1 == graph.blocks.size() || graph.blocks.get(k + 1) != i.to.block) {
                            output.printf("\tjmp\t%s\n",getBlockName(i.to.block));
                        }
                    } else {
                        throw new InternalError();
                    }
                }else if (instruction instanceof FunctionInstruction){
                    if (instruction instanceof ReturnInstruction){
                        ReturnInstruction i = (ReturnInstruction)instruction;
                        whether_return = 1;
                        move(i.source,NASMRegister.rax);
                    }else if (instruction instanceof CallInstruction){
                        CallInstruction i = (CallInstruction)instruction;
                        Function function = i.function;
                        int ex = caller_save_push();
                        output.printf("\tadd\trsp, %d\n",ex);
                        if (function.name.startsWith("_builtin")){
                            if (i.parameters.size() == 3) {
                                move(i.parameters.get(0), NASMRegister.temporary1);
                                move(i.parameters.get(1), NASMRegister.temporary2);
                                move(i.parameters.get(2),NASMRegister.rdx);
                                output.printf("\tmov\trsi, %s\n",NASMRegister.temporary2);
                                output.printf("\tmov\trdi, %s\n",NASMRegister.temporary1);
                            }
                            if (i.parameters.size() == 2){
                                move(i.parameters.get(0), NASMRegister.temporary1);
                                move(i.parameters.get(1),NASMRegister.rsi);
                                output.printf("\tmov\trdi, %s\n",NASMRegister.temporary1);
                            }
                            if (i.parameters.size() == 1){
                                move(i.parameters.get(0),NASMRegister.rdi);
                            }
                        }else{
                            for (int p = 0; p < function.parameters.size(); ++p) {
                                PhysicalRegister a = loadToRead(i.parameters.get(p),NASMRegister.temporary1);
                                int offset = 16 + function.allocator.callee_save_size() + ex + function.graph.frame.size - function.graph.frame.getOffset(function.parameters.get(p).register);
                                if ((rspchange + 8) % 16 != 0) offset += 8;
                                output.printf("\tmov\tqword [rsp - %d], %s\n",offset,a);
                            }
                        }
                        output.printf("\tsub\trsp, %d\n",ex);
                        if ((rspchange + 8) % 16 != 0){
                            output.printf("\tsub\trsp, 8\n");
                        }
                        output.printf("\tcall %s\n",getFunctionName(function));
                        if ((rspchange + 8) % 16 != 0){
                            output.printf("\tadd\trsp, 8\n");
                        }
                        caller_save_pop();
                        if (i.destination != null) {
                            move(NASMRegister.rax,i.destination);
                        }
                    }
                }else if (instruction instanceof MemoryInstruction){
                    if (instruction instanceof StoreInstruction){
                        StoreInstruction i = (StoreInstruction)instruction;
                        PhysicalRegister a = loadToRead(i.address.base,NASMRegister.temporary1);
                        PhysicalRegister b = loadToRead(i.source, NASMRegister.temporary2);
                        output.printf("\tmov\tqword [%s + %d], %s\n",a,i.address.offset.value,b);
                    }else if (instruction instanceof MoveInstruction){
                        MoveInstruction i = (MoveInstruction)instruction;
                        if (i.source instanceof ImmediateValue) {
                            PhysicalRegister a = loadToWrite(i.destination,NASMRegister.temporary1);
                            output.printf("\tmov\t%s, %s\n", a, i.source);
                            store(i.destination, a);
                        } else {
                            PhysicalRegister a = loadToRead(i.source, NASMRegister.temporary1);
                            move(a, i.destination);
                        }
                    }else if (instruction instanceof LoadInstruction){
                        LoadInstruction i = (LoadInstruction)instruction;
                        PhysicalRegister a = loadToRead(i.address.base, NASMRegister.temporary1);
                        PhysicalRegister b = loadToWrite(i.destination, NASMRegister.temporary2);
                        output.printf("\tmov\t%s, qword [%s + %d]\n",b,a,i.address.offset.value);
                        store(i.destination, b);
                    }else if (instruction instanceof AllocateInstruction){
                        AllocateInstruction i = (AllocateInstruction)instruction;
                        int ex = caller_save_push();
                        output.printf("\tadd\trsp, %d\n",ex);
                        move(i.size,NASMRegister.rdi);
                        output.printf("\tsub\trsp, %d\n",ex);
                        if ((rspchange + 8) % 16 != 0) {
                            output.printf("\tsub\trsp, 8\n");
                        }
                        output.printf("\tcall\tmalloc\n");
                        if ((rspchange + 8) % 16 != 0) {
                            output.printf("\tadd\trsp, 8\n");
                        }
                        caller_save_pop();
                        move(NASMRegister.rax,i.destination);


                    }
                }
            }
        }
        output.printf("\tadd\trsp, %d\n",graph.frame.size);
        callee_save_pop();
        output.printf("\tpop\trbp\n");
        if (whether_return == 0 && graph.function.name.equals("main")){
            output.printf("\tmov\trax, 0\n");
        }
        output.printf("\tret\n");
    }
    public int callee_save_push(){
        int ret = 0;
        for (PhysicalRegister reg : allocator.getUsedPhysicalRegisters()){
            if (reg.callvalue() == 0){
                output.printf("\tpush\t%s\n",reg);
                rspchange += 8;
                ret += 8;
            }
        }
        return ret;
    }
    public int callee_save_pop(){
        int ret = 0;
        List<PhysicalRegister> reverse = new ArrayList<>();
        for (PhysicalRegister reg : allocator.getUsedPhysicalRegisters()){
            if (reg.callvalue() == 0){
                reverse.add(reg);
                //output.printf("\tpop\t%s\n",reg);
                rspchange -= 8;
                ret += 8;
            }
        }
        for (int i = reverse.size() - 1;i >= 0;i --){
            output.printf("\tpop\t%s\n",reverse.get(i));
        }
        return ret;
    }
    public int caller_save_push(){
        int ret = 0;
        for (PhysicalRegister reg : allocator.getUsedPhysicalRegisters()){
            if (reg.callvalue() == 1){
                output.printf("\tpush\t%s\n",reg);
                rspchange += 8;
                ret += 8;
            }
        }
        return ret;
    }
    public int caller_save_pop(){
        int ret = 0;
        List<PhysicalRegister> reverse = new ArrayList<>();
        for (PhysicalRegister reg : allocator.getUsedPhysicalRegisters()){
            if (reg.callvalue() == 1){
                reverse.add(reg);
                //output.printf("\tpop\t%s\n",reg);
                rspchange -= 8;
                ret += 8;
            }
        }
        for (int i = reverse.size() - 1;i >= 0;i --){
            output.printf("\tpop\t%s\n",reverse.get(i));
        }
        return ret;
    }
    public void PrintUnaryInstruction(UnaryInstruction i,PhysicalRegister b,PhysicalRegister a){
        // b = op a;
        output.printf("\tmov\t%s, %s\n",b,a);
        if (i instanceof BitnotInstruction){
            output.printf("\tnot\t%s\n",b);
        }else{
            output.printf("\tneg\t%s\n",b);
        }
    }
    public void PrintBinaryInstruction(BinaryInstruction i,PhysicalRegister c,PhysicalRegister a,Object b){
        // c = a op b;
        output.printf("\tmov\t%s, %s",c,a);
        output.printf("\tpush\trax\n\tpush\trcx\n\tpush\trdx\n");
        output.printf("\tmov\trax, %s\n\tmov\trcx, %s\n",a,b);
        if (i instanceof AddInstruction){
            output.printf("\tadd\trax, rcx\n");
        }else if (i instanceof BitandInstruction){
            output.printf("\tand\trax, rcx\n");
        }else if (i instanceof BitorInstruction){
            output.printf("\tor\trax, rcx\n");
        }else if (i instanceof BitxorInstruction){
            output.printf("\txor\trax, rcx\n");
        }else if (i instanceof DivideInstruction){
            output.printf("\tcqo\n\tidiv\trcx\n");
        }else if (i instanceof EqualInstruction){
            output.printf("\tcmp\trax, rcx\n");
            output.printf("\tsete\tal\n");
            output.printf("\tmovzx\trax, al\n");
        }else if (i instanceof GreaterInstruction){
            output.printf("\tcmp\trax, rcx\n");
            output.printf("\tsetg\tal\n");
            output.printf("\tmovzx\trax, al\n");
        }else if (i instanceof LessInstruction){
            output.printf("\tcmp\trax, rcx\n");
            output.printf("\tsetl\tal\n");
            output.printf("\tmovzx\trax, al\n");
        }else if (i instanceof MinusInstruction){
            output.printf("\tsub\trax, rcx\n");
        }else if (i instanceof ModInstruction){
            output.printf("\tcqo\n\tidiv\trcx\n");
            output.printf("\tmov\trax, rdx\n");
        }else if (i instanceof MultiplyInstruction){
            output.printf("\timul\trax, rcx\n");
        }else if (i instanceof NotequalInstruction){
            output.printf("\tcmp\trax, rcx\n");
            output.printf("\tsetne\tal\n");
            output.printf("\tmovzx\trax, al\n");
        }else if (i instanceof NotgreaterInstruction){
            output.printf("\tcmp\trax, rcx\n");
            output.printf("\tsetle\tal\n");
            output.printf("\tmovzx\trax, al\n");
        }else if (i instanceof NotlessInstruction){
            output.printf("\tcmp\trax, rcx\n");
            output.printf("\tsetge\tal\n");
            output.printf("\tmovzx\trax, al\n");
        }else if (i instanceof ShiftleftInstruction){
            output.printf("\tshl\trax, cl\n");
        }else if (i instanceof ShiftrightInstruction){
            output.printf("\tsar\trax, cl\n");
        }
        output.printf("\tmov\t%s, rax\n",c);
        output.printf("\tpop\trdx\n\tpop\trcx\n\tpop\trax\n");
    }
}