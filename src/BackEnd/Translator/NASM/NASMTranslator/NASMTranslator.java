package BackEnd.Translator.NASM.NASMTranslator;

import BackEnd.ControlFlowGraph.Block;
import BackEnd.ControlFlowGraph.Operand.Operand;
import BackEnd.ControlFlowGraph.Operand.VirtualRegister.StringRegister;
import BackEnd.ControlFlowGraph.Operand.VirtualRegister.VariableRegister.GlobalRegister;
import BackEnd.ControlFlowGraph.Operand.VirtualRegister.VirtualRegister;
import BackEnd.Translator.Translator;
import Environment.Environment;
import FrontEnd.AST.Function;
import FrontEnd.AST.Statement.VariableDeclarationStatement;
import Utility.Utility;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.io.PrintStream;

abstract class NASMTranslator extends Translator {
    NASMTranslator(PrintStream output) {
        super(output);
    }

    String getFunctionName(Function function){
        return String.format("%s",function.name);
    }

    String getBlockName(Block block){
        return String.format("%s_%d_%s",block.function.name,block.identity,block.name);
    }

    String getGlobalVariableName(Operand operand){
        return String.format("_global_%d_variable",((VirtualRegister)operand).identity);
    }

    String getStringConstantName(Operand operand) {
        return ((StringRegister)operand).message;
    }

    @Override
    public void translate() throws Exception {
        output.printf("default rel\n");
        for (Function function : Environment.program.functions){
            output.printf("global %s\n",function.name);
        }
        for (VariableDeclarationStatement declaration : Environment.program.globalvariables) {
            output.printf("global %s\n",getGlobalVariableName(declaration.symbol.register));
        }
        output.printf("extern printf, malloc, strcpy, scanf, strlen, sscanf, sprintf, memcpy, strcmp, puts\n");
        output.printf("SECTION .text\n");
        for (Function function : Environment.program.functions){
            translate(function.graph);
        }
        output.printf("\n%s\n",getBuiltinFunction());
        output.println(getDataSection());
        output.println(getBssSection());
        /*
        output.printf("SECTION .data\n");
        for (VariableDeclarationStatement declaration : Environment.program.globalvariables) {
            output.printf("%s:\n",declaration.symbol.name);
        }
        */
    }
}