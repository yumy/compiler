package BackEnd.ControlFlowGraph.Instruction.ControlFlowInstruction;

import BackEnd.ControlFlowGraph.Instruction.Instruction;
import BackEnd.ControlFlowGraph.Instruction.LabelInstruction;
import BackEnd.ControlFlowGraph.Operand.ImmediateValue;
import BackEnd.ControlFlowGraph.Operand.Operand;
import BackEnd.ControlFlowGraph.Operand.VirtualRegister.VirtualRegister;

import java.util.Collections;
import java.util.List;

public class BranchInstruction extends ControlFlowInstruction {
    public Operand condition;
    public LabelInstruction trueto, falseto;

    private BranchInstruction(Operand condition,LabelInstruction trueto,LabelInstruction falseto){
        this.condition = condition;
        this.trueto = trueto;
        this.falseto = falseto;
    }

    public static Instruction getInstruction(Operand condition,LabelInstruction w1,LabelInstruction w2) {
        return new BranchInstruction(condition, w1, w2).rebuild();
    }

    @Override
    public Instruction rebuild() {
        if (condition instanceof ImmediateValue) {
            int value = ((ImmediateValue)condition).value;
            if (value == 0) {
                return JumpInstruction.getInstruction(falseto);
            } else {
                return JumpInstruction.getInstruction(trueto);
            }
        }
        return this;
    }
    @Override
    public String toString(){
        return "br " + condition + " " + trueto.block + " " + falseto.block;
    }

    @Override
    public List<Operand> getDefinedOperands() {
        return Collections.emptyList();
    }

    @Override
    public List<Operand> getUsedOperands() {
        return Collections.singletonList(condition);
    }
}