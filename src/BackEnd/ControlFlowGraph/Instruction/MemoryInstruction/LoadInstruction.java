package BackEnd.ControlFlowGraph.Instruction.MemoryInstruction;

import BackEnd.ControlFlowGraph.Instruction.Instruction;
import BackEnd.ControlFlowGraph.Operand.Address;
import BackEnd.ControlFlowGraph.Operand.Operand;
import BackEnd.ControlFlowGraph.Operand.VirtualRegister.VirtualRegister;
import Utility.Error.InternalError;

import java.util.Collections;
import java.util.List;

public class LoadInstruction extends MemoryInstruction {
    public VirtualRegister destination;
    public Address address;

    private LoadInstruction(VirtualRegister destination,Address address){
        this.destination = destination;
        this.address = address;
    }

    public static Instruction getInstruction(Operand target,Operand address){
        if (target instanceof VirtualRegister && address instanceof Address){
            return new LoadInstruction((VirtualRegister)target,(Address)address);
        }
        throw new InternalError();
    }
    @Override
    public String toString(){
        return destination + " = load " + address.size + " " + address.base + " " + address.offset;
    }
    @Override
    public List<Operand> getDefinedOperands() {
        return Collections.singletonList(destination);
    }

    @Override
    public List<Operand> getUsedOperands() {
        return Collections.singletonList(address.base);
    }
}