package BackEnd.ControlFlowGraph.Instruction.MemoryInstruction;

import BackEnd.ControlFlowGraph.Instruction.Instruction;
import BackEnd.ControlFlowGraph.Operand.Operand;
import BackEnd.ControlFlowGraph.Operand.VirtualRegister.VirtualRegister;
import Utility.Error.InternalError;

import java.util.Collections;
import java.util.List;

public class MoveInstruction extends MemoryInstruction{
    public VirtualRegister destination;
    public Operand source;

    private MoveInstruction(VirtualRegister destination,Operand source){
        this.destination = destination;
        this.source = source;
    }

    public static Instruction getInstruction(Operand destination,Operand source){
        if (destination instanceof VirtualRegister){
            return new MoveInstruction((VirtualRegister)destination,source);
        }
        throw new InternalError();
    }
    @Override
    public String toString(){
        return destination + " = move " + source;
    }
    @Override
    public List<Operand> getDefinedOperands() {
        return Collections.singletonList(destination);
    }

    @Override
    public List<Operand> getUsedOperands() {
        return Collections.singletonList(source);
    }
}