package BackEnd.ControlFlowGraph.Instruction.MemoryInstruction;

import BackEnd.ControlFlowGraph.Instruction.Instruction;
import BackEnd.ControlFlowGraph.Operand.Address;
import BackEnd.ControlFlowGraph.Operand.Operand;
import BackEnd.ControlFlowGraph.Operand.VirtualRegister.VirtualRegister;
import Utility.Error.InternalError;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class StoreInstruction extends MemoryInstruction {
    public Operand source;
    public Address address;

    private StoreInstruction(Operand source,Address address){
        this.source = source;
        this.address = address;
    }

    public static Instruction getInstruction(Operand source,Operand address){
        if (address instanceof Address) {
            return new StoreInstruction(source,(Address) address);
        }
        throw new InternalError();
    }
    @Override
    public String toString(){
        return "store " + address.size + " " + address.base + " " + source + " " + address.offset;
    }
    @Override
    public List<Operand> getDefinedOperands() {
        return Collections.emptyList();
    }

    @Override
    public List<Operand> getUsedOperands() {
        return Arrays.asList(source, address.base);
    }
}