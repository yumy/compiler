package BackEnd.ControlFlowGraph.Instruction.MemoryInstruction;

import BackEnd.ControlFlowGraph.Instruction.Instruction;
import BackEnd.ControlFlowGraph.Operand.Operand;
import BackEnd.ControlFlowGraph.Operand.VirtualRegister.VirtualRegister;
import Utility.Error.InternalError;

import java.util.Collections;
import java.util.List;

public class AllocateInstruction extends MemoryInstruction{
    public VirtualRegister destination;
    public Operand size;

    private AllocateInstruction(VirtualRegister destination,Operand size){
        this.destination = destination;
        this.size = size;
    }

    public static Instruction getInstruction(Operand target,Operand size){
        if (target instanceof VirtualRegister){
            return new AllocateInstruction((VirtualRegister)target,size);
        }
        throw new InternalError();
    }
    @Override
    public List<Operand> getDefinedOperands() {
        return Collections.singletonList(destination);
    }

    @Override
    public List<Operand> getUsedOperands() {
        return Collections.singletonList(size);
    }
}