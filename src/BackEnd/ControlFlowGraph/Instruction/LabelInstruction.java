package BackEnd.ControlFlowGraph.Instruction;

import BackEnd.ControlFlowGraph.Block;
import BackEnd.ControlFlowGraph.Operand.Operand;
import BackEnd.ControlFlowGraph.Operand.VirtualRegister.VirtualRegister;

import java.util.Collections;
import java.util.List;

public class LabelInstruction extends Instruction {
    public String name;
    public Block block;

    private LabelInstruction(String name) {
        this.name = name;
    }

    public static LabelInstruction getInstruction(String name) {
        return new LabelInstruction(name);
    }
    @Override
    public String toString(){
        return "%" + name;
    }
    @Override
    public List<Operand> getDefinedOperands() {
        return Collections.emptyList();
    }

    @Override
    public List<Operand> getUsedOperands() {
        return Collections.emptyList();
    }
}
