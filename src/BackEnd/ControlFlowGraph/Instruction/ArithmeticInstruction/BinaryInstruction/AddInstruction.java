package BackEnd.ControlFlowGraph.Instruction.ArithmeticInstruction.BinaryInstruction;

import BackEnd.ControlFlowGraph.Instruction.Instruction;
import BackEnd.ControlFlowGraph.Instruction.MemoryInstruction.MoveInstruction;
import BackEnd.ControlFlowGraph.Operand.ImmediateValue;
import BackEnd.ControlFlowGraph.Operand.Operand;
import BackEnd.ControlFlowGraph.Operand.VirtualRegister.VirtualRegister;
import Utility.Error.InternalError;

public class AddInstruction extends BinaryInstruction{
    private AddInstruction(VirtualRegister des,Operand source1,Operand source2){
        super(des,source1,source2);
    }

    public static Instruction getInstruction(Operand des,Operand source1,Operand source2){
        if (des instanceof VirtualRegister){
            return new AddInstruction((VirtualRegister)des,source1,source2).rebuild();
        }
        throw new InternalError();
    }

    @Override
    public Instruction rebuild(){
        if (source1 instanceof ImmediateValue && source2 instanceof ImmediateValue){
            int v1 = ((ImmediateValue)source1).value;
            int v2 = ((ImmediateValue)source2).value;
            return MoveInstruction.getInstruction(destination,new ImmediateValue(v1 + v2));
        }
        if (source1 instanceof ImmediateValue){
            Operand tmp = source1;
            source1 = source2;
            source2 = tmp;
        }
        return this;
    }
    @Override
    public String toString(){
        return String.format("%s = add %s %s",destination,source1,source2);
    }
}