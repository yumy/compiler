package BackEnd.ControlFlowGraph.Instruction.ArithmeticInstruction.BinaryInstruction;

import BackEnd.ControlFlowGraph.Instruction.Instruction;
import BackEnd.ControlFlowGraph.Instruction.MemoryInstruction.MoveInstruction;
import BackEnd.ControlFlowGraph.Operand.ImmediateValue;
import BackEnd.ControlFlowGraph.Operand.Operand;
import BackEnd.ControlFlowGraph.Operand.VirtualRegister.VirtualRegister;
import Utility.Error.InternalError;

public class MultiplyInstruction extends BinaryInstruction {
    private MultiplyInstruction(VirtualRegister destination,Operand source1,Operand source2){
        super(destination,source1,source2);
    }

    public static Instruction getInstruction(Operand destination,Operand source1,Operand source2){
        if (destination instanceof VirtualRegister) {
            return new MultiplyInstruction((VirtualRegister)destination,source1,source2).rebuild();
        }
        throw new InternalError();
    }

    @Override
    public Instruction rebuild() {
        if (source1 instanceof ImmediateValue && source2 instanceof ImmediateValue){
            int v1 = ((ImmediateValue)source1).value;
            int v2 = ((ImmediateValue)source2).value;
            return MoveInstruction.getInstruction(destination,new ImmediateValue(v1 * v2));
        }
        if (source1 instanceof ImmediateValue) {
            Operand tmp = source1;
            source1 = source2;
            source2 = tmp;
        }
        return this;
    }
    @Override
    public String toString(){
        return String.format("%s = mul %s %s",destination,source1,source2);
    }
}