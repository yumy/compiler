package BackEnd.ControlFlowGraph.Instruction.ArithmeticInstruction.UnaryInstruction;

import BackEnd.ControlFlowGraph.Instruction.Instruction;
import BackEnd.ControlFlowGraph.Instruction.MemoryInstruction.MoveInstruction;
import BackEnd.ControlFlowGraph.Operand.ImmediateValue;
import BackEnd.ControlFlowGraph.Operand.Operand;
import BackEnd.ControlFlowGraph.Operand.VirtualRegister.VirtualRegister;

public class PrefixMinusInstruction extends UnaryInstruction {
    private PrefixMinusInstruction(VirtualRegister destination,Operand source){
        super(destination,source);
    }

    public static Instruction getInstruction(Operand destination,Operand source){
        return new PrefixMinusInstruction((VirtualRegister)destination,source).rebuild();
    }

    @Override
    public Instruction rebuild(){
        if (source instanceof ImmediateValue){
            int value = ((ImmediateValue)source).value;
            return MoveInstruction.getInstruction(destination,new ImmediateValue(-value));
        }
        return this;
    }
    @Override
    public String toString(){
        return String.format("%s = neg %s",destination,source);
    }
}