package BackEnd.ControlFlowGraph.Operand;

public class ImmediateValue extends Operand {
    public int value;

    public ImmediateValue(int value){
        this.value = value;
    }

    @Override
    public String toString(){
        return String.valueOf(value);
    }

    @Override
    public boolean equals(Object object) {
        if (object instanceof ImmediateValue){
            ImmediateValue other = (ImmediateValue)object;
            return other.value == value;
        }else{
            return false;
        }
    }
}