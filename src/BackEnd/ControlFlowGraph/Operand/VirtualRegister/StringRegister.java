package BackEnd.ControlFlowGraph.Operand.VirtualRegister;

public class StringRegister extends VirtualRegister {
    public String value;
    public String message;
    public static int cnt_str = 0;
    public StringRegister(String value){
        this.value = value;
    }
}