package BackEnd.ControlFlowGraph.Operand.VirtualRegister;

import BackEnd.ControlFlowGraph.Operand.Operand;
import Environment.Environment;

import java.util.ArrayList;
import java.util.List;

public abstract class VirtualRegister extends Operand {
    public int identity;

    public VirtualRegister(){
        this.identity = Environment.registertable.registers.size();
    }
    @Override
    public String toString(){
        return String.format("$%d",identity);
    }
}