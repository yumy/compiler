package BackEnd.ControlFlowGraph;

import BackEnd.ControlFlowGraph.Instruction.Instruction;
import BackEnd.ControlFlowGraph.Instruction.LabelInstruction;
import BackEnd.ControlFlowGraph.Operand.VirtualRegister.VirtualRegister;
import FrontEnd.AST.Function;
//import Utility.Utility;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class Block {
    public Function function;
    public String name;
    public int identity;
    public LabelInstruction label;
    public List<Instruction> instructions;
    public List<Block> successors, predecessors;
    public Liveliness liveliness;

    Block(Function function,String name,int identity,LabelInstruction label){
        this.function = function;
        this.name = name;
        this.identity = identity;
        this.label = label;
        this.instructions = new ArrayList<>();
        this.successors = new ArrayList<>();
        this.predecessors = new ArrayList<>();
        this.liveliness = new Liveliness();
    }
    public class Liveliness {
        public List<VirtualRegister> used, defined;
        public Set<VirtualRegister> liveIn, liveOut;

        public Liveliness() {
            this.used = new ArrayList<>();
            this.defined = new ArrayList<>();
            this.liveIn = new HashSet<>();
            this.liveOut = new HashSet<>();
        }
    }
}