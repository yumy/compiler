package BackEnd.Allocator.GlobalRegisterAllocator;

import BackEnd.Allocator.Allocator;
import BackEnd.Allocator.GlobalRegisterAllocator.GraphColoring.ChaitinGraphColoring;
import BackEnd.ControlFlowGraph.Block;
import BackEnd.ControlFlowGraph.Instruction.ArithmeticInstruction.BinaryInstruction.BinaryInstruction;
import BackEnd.ControlFlowGraph.Instruction.Instruction;
import BackEnd.ControlFlowGraph.Instruction.MemoryInstruction.MoveInstruction;
import BackEnd.ControlFlowGraph.Operand.VirtualRegister.VirtualRegister;
import FrontEnd.AST.Function;

import java.util.HashSet;
import java.util.Set;

public class GlobalRegisterAllocator extends Allocator {
    public GlobalRegisterAllocator(Function function) {
        super(function);
        InterferenceGraph interGraph = new InterferenceGraph();
        for (Block block : function.graph.blocks) {
            for (Instruction instruction : block.instructions) {
                for (VirtualRegister register : instruction.getDefinedRegisters()) {
                    interGraph.add(register);
                }
                for (VirtualRegister register : instruction.getUsedRegisters()) {
                    interGraph.add(register);
                }
            }
        }
        for (Block block : function.graph.blocks) {
            Set<VirtualRegister> living = new HashSet<>();
            for (VirtualRegister register : block.liveliness.liveOut) {
                living.add(register);
            }
            for (int i = block.instructions.size() - 1; i >= 0; i--) {
                Instruction instruction = block.instructions.get(i);
                if (instruction instanceof BinaryInstruction) {
                    for (VirtualRegister livingRegister : living) {
                        interGraph.forbid(((BinaryInstruction) instruction).destination, livingRegister);
                    }
                    living.remove(((BinaryInstruction) instruction).destination);
                    if (((BinaryInstruction) instruction).source2 instanceof VirtualRegister) {
                        living.add((VirtualRegister) ((BinaryInstruction) instruction).source2);
                    }

                    for (VirtualRegister livingRegister : living) {
                        interGraph.forbid(((BinaryInstruction) instruction).destination, livingRegister);
                    }
                    living.remove(((BinaryInstruction) instruction).destination);
                    if (((BinaryInstruction) instruction).source1 instanceof VirtualRegister) {
                        living.add((VirtualRegister) ((BinaryInstruction) instruction).source1);
                    }
                } else {
                    for (VirtualRegister register : instruction.getDefinedRegisters()) {
                        for (VirtualRegister livingRegister : living) {
                            interGraph.forbid(register, livingRegister);
                        }
                    }
                    for (VirtualRegister register : instruction.getDefinedRegisters()) {
                        living.remove(register);
                    }
                    for (VirtualRegister register : instruction.getUsedRegisters()) {
                        living.add(register);
                    }
                }
            }
        }
        for (Block block : function.graph.blocks) {
            for (Instruction instruction : block.instructions) {
                if (instruction instanceof MoveInstruction) {
                    if (((MoveInstruction) instruction).source instanceof VirtualRegister) {
                        interGraph.recommend(((MoveInstruction) instruction).destination,
                                (VirtualRegister) ((MoveInstruction) instruction).source);
                    }
                }
            }
        }
        mapping = new ChaitinGraphColoring(interGraph).analysis();
    }
}
