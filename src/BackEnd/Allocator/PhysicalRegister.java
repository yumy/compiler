package BackEnd.Allocator;

public abstract class PhysicalRegister{
    public int identity;
    public String name;

    protected PhysicalRegister(int identity,String name){
        this.identity = identity;
        this.name = name;
    }

    public int callvalue(){
        // callee_save 0
        // caller_save 1
        String s = this.toString();
        if (s.equals("rbx") || s.equals("rsp") || s.equals("rbp") || s.equals("r12")
                || s.equals("r13") || s.equals("r14") || s.equals("r15")){
           return 0;
        }
        return 1;
    }

    @Override
    public String toString(){
        return name;
    }
}