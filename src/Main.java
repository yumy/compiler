//import BackEnd.Allocator.GlobalRegisterAllocator.GlobalRegisterAllocator;
import BackEnd.ControlFlowGraph.Graph;
import BackEnd.Translator.NASM.NASMTranslator.NASMNaiveTranslator;
import BackEnd.Translator.NASM.NASMTranslator.NASMBasicTranslator;
import Environment.Environment;
import Environment.SymbolTable.Symbol;
import FrontEnd.AST.Function;
import FrontEnd.CST.Listener.ClassFetchListener;
import FrontEnd.CST.Listener.DeclarationFetchListener;
import FrontEnd.CST.Listener.SyntaxErrorListener;
import FrontEnd.CST.Listener.TreeBuildListener;
import FrontEnd.CST.Parser.FateLexer;
import FrontEnd.CST.Parser.FateParser;
import BackEnd.Allocator.GlobalRegisterAllocator.GlobalRegisterAllocator;
import Utility.Error.CompilationError;
//import Compiler.Utility.Utility;
import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.tree.ParseTree;
import org.antlr.v4.runtime.tree.ParseTreeWalker;

import java.io.FileInputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintStream;
import java.util.Arrays;
import java.util.HashSet;
public class Main{
    public static void main(String[] args) throws Exception{
        InputStream file = new FileInputStream("./src/a.in");
        ANTLRInputStream input = new ANTLRInputStream(file);
        FateLexer lexer = new FateLexer(input);
        CommonTokenStream tokens = new CommonTokenStream(lexer);
        FateParser parser = new FateParser(tokens);
        parser.removeErrorListeners();
        parser.addErrorListener(new SyntaxErrorListener());
        ParseTree tree = parser.program();
        ParseTreeWalker walker = new ParseTreeWalker();
        Environment.initialize();
        walker.walk(new ClassFetchListener(),tree);
        walker.walk(new DeclarationFetchListener(),tree);
        if (!(Environment.symboltable.include("main"))){
            throw new CompilationError("CE");
        }
        Symbol test = Environment.symboltable.getSymbol("main");
        if (!(test.type instanceof Function)){
            throw new CompilationError("CE");
        }
        walker.walk(new TreeBuildListener(),tree);
        for (Function function : Environment.program.functions){
            function.graph = new Graph(function);
            function.allocator = new GlobalRegisterAllocator(function);
        }

        OutputStream output = System.out;
        //new NASMNaiveTranslator(new PrintStream(output)).translate();
        new NASMBasicTranslator(new PrintStream(output)).translate();
    }
}