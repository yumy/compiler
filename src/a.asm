default rel
global func
global main
extern printf, malloc, strcpy, scanf, strlen, sscanf, sprintf, memcpy, strcmp, puts
SECTION .text
func:
	push	rbp
	mov	rbp, rsp
	push	rbx
	sub	rsp, 176
func_0_enter:
func_1_entry:
	mov	r12, qword [rsp + 152]
	mov	r13, qword [rsp + 160]
	lea	rbx, [r12 + r13]
	mov	r13, qword [rsp + 168]
	lea	rbx, [rbx + r13]
	mov	rbx, rbx
	and	rbx, 1073741823
	mov	rax, rbx
func_2_exit:
	add	rsp, 176
	pop	rbx
	pop	rbp
	ret
main:
	push	rbp
	mov	rbp, rsp
	push	r14
	push	r15
	push	rbx
	sub	rsp, 1296
main_0_enter:
main_1_entry:
	push	r10
	push	r8
	push	rdi
	push	r11
	push	rsi
	push	r9
	add	rsp, 48
	sub	rsp, 48
	sub	rsp, 8
	call _builtin_getInt
	add	rsp, 8
	pop	r9
	pop	rsi
	pop	r11
	pop	rdi
	pop	r8
	pop	r10
	mov	r11, rax
	mov	rdi, r11
	mov	rdi, rdi
	add	rdi, 1
	mov rdi, rdi
	imul rdi, 8
	push	r10
	push	r8
	push	rdi
	push	r11
	push	rsi
	push	r9
	add	rsp, 48
	sub	rsp, 48
	sub	rsp, 8
	call	malloc
	add	rsp, 8
	pop	r9
	pop	rsi
	pop	r11
	pop	rdi
	pop	r8
	pop	r10
	mov	rbx, rax
	mov	qword [rbx + 0], r11
	mov	rbx, rbx
	add	rbx, 8
	mov	rdi, r11
main_2_while_loop:
	mov rdi, rdi
	sub rdi, 1
	mov	r8, rdi
	mov rdi, rdi
	imul rdi, 8
	lea	rdi, [rdi + rbx]
	mov	rsi, r11
	mov	rsi, rsi
	add	rsi, 1
	mov rsi, rsi
	imul rsi, 8
	push	r10
	push	r8
	push	rdi
	push	r11
	push	rsi
	push	r9
	add	rsp, 48
	mov	rdi, rsi
	sub	rsp, 48
	sub	rsp, 8
	call	malloc
	add	rsp, 8
	pop	r9
	pop	rsi
	pop	r11
	pop	rdi
	pop	r8
	pop	r10
	mov	rsi, rax
	mov	qword [rsi + 0], r11
	mov	rsi, rsi
	add	rsi, 8
	mov	qword [rdi + 0], rsi
	cmp r8, 0
	setne al
	movzx rsi, al
	mov	rdi, r8
	cmp	rsi, 0
	je	main_3_while_merge
	jmp	main_2_while_loop
main_3_while_merge:
	mov	rdi, r11
	mov	rdi, rdi
	add	rdi, 1
	mov rdi, rdi
	imul rdi, 8
	push	r10
	push	r8
	push	rdi
	push	r11
	push	rsi
	push	r9
	add	rsp, 48
	sub	rsp, 48
	sub	rsp, 8
	call	malloc
	add	rsp, 8
	pop	r9
	pop	rsi
	pop	r11
	pop	rdi
	pop	r8
	pop	r10
	mov	r14, rax
	mov	qword [r14 + 0], r11
	mov	r14, r14
	add	r14, 8
	mov	rdi, r11
main_4_while_loop:
	mov rdi, rdi
	sub rdi, 1
	mov	r8, rdi
	mov rdi, rdi
	imul rdi, 8
	lea	rdi, [rdi + r14]
	mov	rsi, r11
	mov	rsi, rsi
	add	rsi, 1
	mov rsi, rsi
	imul rsi, 8
	push	r10
	push	r8
	push	rdi
	push	r11
	push	rsi
	push	r9
	add	rsp, 48
	mov	rdi, rsi
	sub	rsp, 48
	sub	rsp, 8
	call	malloc
	add	rsp, 8
	pop	r9
	pop	rsi
	pop	r11
	pop	rdi
	pop	r8
	pop	r10
	mov	rsi, rax
	mov	qword [rsi + 0], r11
	mov	rsi, rsi
	add	rsi, 8
	mov	qword [rdi + 0], rsi
	cmp r8, 0
	setne al
	movzx rsi, al
	mov	rdi, r8
	cmp	rsi, 0
	je	main_5_while_merge
	jmp	main_4_while_loop
main_5_while_merge:
	mov	rsi, r11
	mov	rsi, rsi
	add	rsi, 1
	mov rsi, rsi
	imul rsi, 8
	push	r10
	push	r8
	push	rdi
	push	r11
	push	rsi
	push	r9
	add	rsp, 48
	mov	rdi, rsi
	sub	rsp, 48
	sub	rsp, 8
	call	malloc
	add	rsp, 8
	pop	r9
	pop	rsi
	pop	r11
	pop	rdi
	pop	r8
	pop	r10
	mov	rdi, rax
	mov	qword [rdi + 0], r11
	mov	rdi, rdi
	add	rdi, 8
	mov	rsi, r11
main_6_while_loop:
	mov rsi, rsi
	sub rsi, 1
	mov	r9, rsi
	mov rsi, rsi
	imul rsi, 8
	lea	rsi, [rsi + rdi]
	mov	r8, r11
	mov	r8, r8
	add	r8, 1
	mov r8, r8
	imul r8, 8
	push	r10
	push	r8
	push	rdi
	push	r11
	push	rsi
	push	r9
	add	rsp, 48
	mov	rdi, r8
	sub	rsp, 48
	sub	rsp, 8
	call	malloc
	add	rsp, 8
	pop	r9
	pop	rsi
	pop	r11
	pop	rdi
	pop	r8
	pop	r10
	mov	r8, rax
	mov	qword [r8 + 0], r11
	mov	r8, r8
	add	r8, 8
	mov	qword [rsi + 0], r8
	cmp r9, 0
	setne al
	movzx r8, al
	mov	rsi, r9
	cmp	r8, 0
	je	main_7_while_merge
	jmp	main_6_while_loop
main_7_while_merge:
	mov	qword [rsp + 176], rdi
	mov	r12, 0
	mov	qword [rsp + 608], r12
main_8_for_condition:
	mov	r12, qword [rsp + 608]
	cmp	r12, r11
	jge	main_15_for_merge
main_9_for_body:
	mov	r12, 0
	mov	qword [rsp + 360], r12
main_10_for_condition:
	mov	r12, qword [rsp + 360]
	cmp	r12, r11
	jge	main_13_for_merge
main_11_for_body:
	mov	r12, qword [rsp + 608]
	mov rdi, r12
	imul rdi, 8
	lea	rsi, [rbx + rdi]
	mov	rsi, qword [rsi + 0]
	mov	r12, qword [rsp + 360]
	mov rdi, r12
	imul rdi, 8
	lea	r8, [rsi + rdi]
	mov	r12, qword [rsp + 608]
	mov	r13, qword [rsp + 360]
	lea	rsi, [r12 + r13]
	mov	qword [r8 + 0], rsi
main_12_for_loop:
	mov	r12, qword [rsp + 360]
	mov	r13, r12
	add	r13, 1
	mov	qword [rsp + 360], r13
	jmp	main_10_for_condition
main_13_for_merge:
main_14_for_loop:
	mov	r12, qword [rsp + 608]
	mov	r13, r12
	add	r13, 1
	mov	qword [rsp + 608], r13
	jmp	main_8_for_condition
main_15_for_merge:
	mov	r12, 0
	mov	qword [rsp + 608], r12
main_16_for_condition:
	mov	r12, qword [rsp + 608]
	cmp	r12, r11
	jge	main_30_for_merge
main_17_for_body:
	mov	r12, 0
	mov	qword [rsp + 360], r12
main_18_for_condition:
	mov	r12, qword [rsp + 360]
	cmp	r12, r11
	jge	main_28_for_merge
main_19_for_body:
	mov	r8, 0
main_20_for_condition:
	cmp	r8, r11
	jge	main_26_for_merge
main_21_for_body:
	mov	r12, qword [rsp + 360]
	mov	r13, qword [rsp + 608]
	cmp	r12, r13
	jl	main_23_if_false
main_22_if_true:
	mov	r12, qword [rsp + 608]
	mov rdi, r12
	imul rdi, 8
	lea	rsi, [r14 + rdi]
	mov	rdi, qword [rsi + 0]
	mov	r12, qword [rsp + 360]
	mov rsi, r12
	imul rsi, 8
	lea	r9, [rdi + rsi]
	mov	r12, qword [rsp + 608]
	mov rdi, r12
	imul rdi, 8
	lea	rsi, [r14 + rdi]
	mov	rsi, qword [rsi + 0]
	mov	r12, qword [rsp + 360]
	mov rdi, r12
	imul rdi, 8
	lea	rsi, [rsi + rdi]
	mov	r15, qword [rsi + 0]
	mov	r12, qword [rsp + 608]
	mov rsi, r12
	imul rsi, 8
	lea	rdi, [rbx + rsi]
	mov	rsi, qword [rdi + 0]
	mov rdi, r8
	imul rdi, 8
	lea	rsi, [rsi + rdi]
	mov	rdi, qword [rsi + 0]
	mov rsi, r8
	imul rsi, 8
	lea	r10, [rbx + rsi]
	mov	rsi, qword [r10 + 0]
	mov	r12, qword [rsp + 360]
	mov r10, r12
	imul r10, 8
	lea	rsi, [rsi + r10]
	mov	rsi, qword [rsi + 0]
	push	r10
	push	r8
	push	rdi
	push	r11
	push	rsi
	push	r9
	add	rsp, 48
	mov	qword [rsp - 104], r15
	mov	qword [rsp - 96], rdi
	mov	qword [rsp - 88], rsi
	sub	rsp, 48
	sub	rsp, 8
	call func
	add	rsp, 8
	pop	r9
	pop	rsi
	pop	r11
	pop	rdi
	pop	r8
	pop	r10
	mov	rsi, rax
	mov	qword [r9 + 0], rsi
	mov	r12, qword [rsp + 608]
	mov rdi, r12
	imul rdi, 8
	mov	r12, qword [rsp + 176]
	lea	rsi, [r12 + rdi]
	mov	rdi, qword [rsi + 0]
	mov	r12, qword [rsp + 360]
	mov rsi, r12
	imul rsi, 8
	lea	r9, [rdi + rsi]
	mov	r12, qword [rsp + 608]
	mov rsi, r12
	imul rsi, 8
	lea	rdi, [r14 + rsi]
	mov	rsi, qword [rdi + 0]
	mov	r12, qword [rsp + 360]
	mov rdi, r12
	imul rdi, 8
	lea	rsi, [rsi + rdi]
	mov	rdi, qword [rsi + 0]
	mov	r12, qword [rsp + 608]
	mov r10, r12
	imul r10, 8
	lea	rsi, [rbx + r10]
	mov	rsi, qword [rsi + 0]
	mov r10, r8
	imul r10, 8
	lea	rsi, [rsi + r10]
	mov	r15, qword [rsi + 0]
	mov r10, r8
	imul r10, 8
	lea	rsi, [rbx + r10]
	mov	rsi, qword [rsi + 0]
	mov	r12, qword [rsp + 360]
	mov r10, r12
	imul r10, 8
	lea	rsi, [rsi + r10]
	mov	rsi, qword [rsi + 0]
	push	r10
	push	r8
	push	rdi
	push	r11
	push	rsi
	push	r9
	add	rsp, 48
	mov	qword [rsp - 104], rdi
	mov	qword [rsp - 96], r15
	mov	qword [rsp - 88], rsi
	sub	rsp, 48
	sub	rsp, 8
	call func
	add	rsp, 8
	pop	r9
	pop	rsi
	pop	r11
	pop	rdi
	pop	r8
	pop	r10
	mov	rsi, rax
	mov	qword [r9 + 0], rsi
	mov	r12, qword [rsp + 608]
	mov rsi, r12
	imul rsi, 8
	mov	r12, qword [rsp + 176]
	lea	rdi, [r12 + rsi]
	mov	rsi, qword [rdi + 0]
	mov	r12, qword [rsp + 360]
	mov rdi, r12
	imul rdi, 8
	lea	rsi, [rsi + rdi]
	mov	r12, qword [rsp + 608]
	mov rdi, r12
	imul rdi, 8
	lea	r9, [r14 + rdi]
	mov	rdi, qword [r9 + 0]
	mov	r12, qword [rsp + 360]
	mov r9, r12
	imul r9, 8
	lea	rdi, [rdi + r9]
	mov	rdi, qword [rdi + 0]
	mov	r12, qword [rsp + 608]
	mov r9, r12
	imul r9, 8
	lea	r10, [rbx + r9]
	mov	r10, qword [r10 + 0]
	mov r9, r8
	imul r9, 8
	lea	r10, [r10 + r9]
	mov	r9, qword [r10 + 0]
	mov r10, r8
	imul r10, 8
	lea	r15, [rbx + r10]
	mov	r15, qword [r15 + 0]
	mov	r12, qword [rsp + 360]
	mov r10, r12
	imul r10, 8
	lea	r15, [r15 + r10]
	mov	r10, qword [r15 + 0]
	push	r10
	push	r8
	push	rdi
	push	r11
	push	rsi
	push	r9
	add	rsp, 48
	mov	qword [rsp - 104], rdi
	mov	qword [rsp - 96], r9
	mov	qword [rsp - 88], r10
	sub	rsp, 48
	sub	rsp, 8
	call func
	add	rsp, 8
	pop	r9
	pop	rsi
	pop	r11
	pop	rdi
	pop	r8
	pop	r10
	mov	rdi, rax
	mov	qword [rsi + 0], rdi
	mov	r12, qword [rsp + 608]
	mov rsi, r12
	imul rsi, 8
	mov	r12, qword [rsp + 176]
	lea	rdi, [r12 + rsi]
	mov	rdi, qword [rdi + 0]
	mov	r12, qword [rsp + 360]
	mov rsi, r12
	imul rsi, 8
	lea	r9, [rdi + rsi]
	mov	r12, qword [rsp + 608]
	mov rsi, r12
	imul rsi, 8
	lea	rdi, [r14 + rsi]
	mov	rsi, qword [rdi + 0]
	mov	r12, qword [rsp + 360]
	mov rdi, r12
	imul rdi, 8
	lea	rsi, [rsi + rdi]
	mov	r10, qword [rsi + 0]
	mov	r12, qword [rsp + 608]
	mov rdi, r12
	imul rdi, 8
	lea	rsi, [rbx + rdi]
	mov	rsi, qword [rsi + 0]
	mov rdi, r8
	imul rdi, 8
	lea	rsi, [rsi + rdi]
	mov	rsi, qword [rsi + 0]
	mov r15, r8
	imul r15, 8
	lea	rdi, [rbx + r15]
	mov	rdi, qword [rdi + 0]
	mov	r12, qword [rsp + 360]
	mov r15, r12
	imul r15, 8
	lea	rdi, [rdi + r15]
	mov	rdi, qword [rdi + 0]
	push	r10
	push	r8
	push	rdi
	push	r11
	push	rsi
	push	r9
	add	rsp, 48
	mov	qword [rsp - 104], r10
	mov	qword [rsp - 96], rsi
	mov	qword [rsp - 88], rdi
	sub	rsp, 48
	sub	rsp, 8
	call func
	add	rsp, 8
	pop	r9
	pop	rsi
	pop	r11
	pop	rdi
	pop	r8
	pop	r10
	mov	rsi, rax
	mov	qword [r9 + 0], rsi
main_24_if_merge:
main_25_for_loop:
	mov	r8, r8
	add	r8, 1
	jmp	main_20_for_condition
main_23_if_false:
	jmp	main_24_if_merge
main_26_for_merge:
main_27_for_loop:
	mov	r12, qword [rsp + 360]
	mov	r13, r12
	add	r13, 1
	mov	qword [rsp + 360], r13
	jmp	main_18_for_condition
main_28_for_merge:
main_29_for_loop:
	mov	r12, qword [rsp + 608]
	mov	r13, r12
	add	r13, 1
	mov	qword [rsp + 608], r13
	jmp	main_16_for_condition
main_30_for_merge:
	mov	rsi, 0
	mov	r12, 0
	mov	qword [rsp + 608], r12
main_31_for_condition:
	mov	r12, qword [rsp + 608]
	cmp	r12, r11
	jge	main_38_for_merge
main_32_for_body:
	mov	r12, 0
	mov	qword [rsp + 360], r12
main_33_for_condition:
	mov	r12, qword [rsp + 360]
	cmp	r12, r11
	jge	main_36_for_merge
main_34_for_body:
	mov	r12, qword [rsp + 608]
	mov rdi, r12
	imul rdi, 8
	lea	rbx, [r14 + rdi]
	mov	rdi, qword [rbx + 0]
	mov	r12, qword [rsp + 360]
	mov rbx, r12
	imul rbx, 8
	lea	rdi, [rdi + rbx]
	mov	rdi, qword [rdi + 0]
	lea	rbx, [rsi + rdi]
	mov	rsi, rbx
	and	rsi, 1073741823
main_35_for_loop:
	mov	r12, qword [rsp + 360]
	mov	r13, r12
	add	r13, 1
	mov	qword [rsp + 360], r13
	jmp	main_33_for_condition
main_36_for_merge:
main_37_for_loop:
	mov	r12, qword [rsp + 608]
	mov	r13, r12
	add	r13, 1
	mov	qword [rsp + 608], r13
	jmp	main_31_for_condition
main_38_for_merge:
	push	r10
	push	r8
	push	rdi
	push	r11
	push	rsi
	push	r9
	add	rsp, 48
	mov	rdi, rsi
	sub	rsp, 48
	sub	rsp, 8
	call _builtin_toString
	add	rsp, 8
	pop	r9
	pop	rsi
	pop	r11
	pop	rdi
	pop	r8
	pop	r10
	mov	rbx, rax
	push	r10
	push	r8
	push	rdi
	push	r11
	push	rsi
	push	r9
	add	rsp, 48
	mov	rdi, rbx
	sub	rsp, 48
	sub	rsp, 8
	call _builtin_print
	add	rsp, 8
	pop	r9
	pop	rsi
	pop	r11
	pop	rdi
	pop	r8
	pop	r10
	mov	rax, 0
main_39_exit:
	add	rsp, 1296
	pop	rbx
	pop	r15
	pop	r14
	pop	rbp
	ret

print_Int:
     mov                  rsi,                  rdi
     mov                  rdi,    __print_IntFormat
     sub                  rsp,                    8
    call               printf
     add                  rsp,                    8
     ret
println_Int:
     mov                  rsi,                  rdi
     mov                  rdi,  __println_IntFormat
     sub                  rsp,                    8
    call               printf
     add                  rsp,                    8
     ret
_builtin_print:
     mov                  rsi,                  rdi
     mov                  rdi, ___builtin_printFormat
     sub                  rsp,                    8
    call               printf
     add                  rsp,                    8
     ret
_builtin_println:
     sub                  rsp,                    8
    call                 puts
     add                  rsp,                    8
     ret
_builtin_getInt:
     mov                  rdi,       __getIntFormat
     mov                  rsi,           @getIntBuf
     sub                  rsp,                    8
    call                scanf
     add                  rsp,                    8
     mov                  rax,   qword [@getIntBuf]
     ret
_builtin_getString:
    push                  r15
     mov                  rdi,                  300
    call               malloc
     mov                  r15,                  rax
     add                  r15,                    8
     mov                  rdi,    __getStringFormat
     mov                  rsi,                  r15
    call                scanf
     mov                  rdi,                  r15
    call               strlen
     mov      qword [r15 - 8],                  rax
     mov                  rax,                  r15
     pop                  r15
     ret
_builtin_toString:
    push                  r15
    push                  rdi
     mov                  rdi,                   20
     sub                  rsp,                    8
    call               malloc
     add                  rsp,                    8
     mov                  r15,                  rax
     add                  r15,                    8
     mov                  rdi,                  r15
     mov                  rsi,     __toStringFormat
     pop                  rdx
    call              sprintf
     mov                  rdi,                  r15
    call               strlen
     mov      qword [r15 - 8],                  rax
     mov                  rax,                  r15
     pop                  r15
     ret
_builtin_array_size:
     mov                  rax,      qword [rdi - 8]
     ret
_builtin_string_length:
     mov                  rax,      qword [rdi - 8]
     ret
_builtin_string_parseInt:
     mov                  rsi,       __getIntFormat
     mov                  rdx,         @parseIntBuf
     sub                  rsp,                    8
    call               sscanf
     add                  rsp,                    8
     mov                  rax, qword [@parseIntBuf]
     ret
_builtin_string_substring:
    push                  r15
    push                  r14
     mov                  r15,                  rdi
     add                  r15,                  rsi
     mov                  r14,                  rdx
     sub                  r14,                  rsi
     add                  r14,                    1
     mov                  rdi,                  r14
     add                  rdi,                    9
     sub                  rsp,                    8
    call               malloc
     add                  rsp,                    8
     add                  rax,                    8
     mov                  rdi,                  rax
     mov                  rsi,                  r15
     mov                  rdx,                  r14
     sub                  rsp,                    8
    call               memcpy
     add                  rsp,                    8
     mov      qword [rax - 8],                  r14
     mov                  r15,                  rax
     add                  r15,                  r14
     mov                  r15,                    0
     pop                  r14
     pop                  r15
     ret
_builtin_string_ord:
     add                  rdi,                  rsi
   movsx                  rax,           byte [rdi]
     ret
_builtin_string_concatenate:
    push                  r15
    push                  r14
    push                  r13
     mov                  r15,      qword [rdi - 8]
     add                  r15,      qword [rsi - 8]
     add                  r15,                    9
     mov                  r14,                  rdi
     mov                  r13,                  rsi
     mov                  rdi,                  r15
    call               malloc
     sub                  r15,                    9
     mov          qword [rax],                  r15
     mov                  r15,                  rax
     add                  r15,                    8
     mov                  rdi,                  r15
     mov                  rsi,                  r14
    call               strcpy
     add                  r15,      qword [r14 - 8]
     mov                  r14,                  rax
     mov                  rdi,                  r15
     mov                  rsi,                  r13
    call               strcpy
     mov                  rax,                  r14
     pop                  r13
     pop                  r14
     pop                  r15
     ret
_builtin_string_equal:
     sub                  rsp,                    8
    call               strcmp
     add                  rsp,                    8
     cmp                  eax,                    0
     mov                  rax,                    0
    sete                   al
     ret
     sub                  rsp,                    8
    call               strcmp
     add                  rsp,                    8
     cmp                  eax,                    0
     mov                  rax,                    0
   setne                   al
     ret
_builtin_string_greater:
     sub                  rsp,                    8
    call               strcmp
     add                  rsp,                    8
     cmp                  eax,                    0
     mov                  rax,                    0
    setg                   al
     ret
_builtin_string_notless:
     sub                  rsp,                    8
    call               strcmp
     add                  rsp,                    8
     cmp                  eax,                    0
     mov                  rax,                    0
   setge                   al
     ret
_builtin_string_less:
     sub                  rsp,                    8
    call               strcmp
     add                  rsp,                    8
     cmp                  eax,                    0
     mov                  rax,                    0
    setl                   al
     ret
_builtin_string_notgreater:
     sub                  rsp,                    8
    call               strcmp
     add                  rsp,                    8
     cmp                  eax,                    0
     mov                  rax,                    0
   setle                   al
     ret

SECTION .data
__println_IntFormat:
      db         "%ld", 10, 0
__print_IntFormat:
      db             "%ld", 0
___builtin_printFormat:
      db              "%s", 0
__getIntFormat:
      db             "%ld", 0
__getStringFormat:
      db              "%s", 0
__toStringFormat:
      db             "%ld", 0
__parseIntFormat:
      db             "%ld", 0

SECTION .bss
@getIntBuf:
    resq                    1
@parseIntBuf:
    resq                    1
