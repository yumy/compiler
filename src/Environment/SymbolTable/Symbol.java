package Environment.SymbolTable;
import BackEnd.ControlFlowGraph.Operand.VirtualRegister.VirtualRegister;
import Environment.Environment;
import Environment.ScopeTable.Scope;
import FrontEnd.AST.Type.Type;
public class Symbol{
    public String name;
    public Type type;
    public Scope scope;
    public VirtualRegister register;
    public Symbol(String name,Type type){
        this.name = name;
        this.type = type;
        this.scope = Environment.scopetable.getScope();
        this.register = null;
    }
}