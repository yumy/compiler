// Generated from /home/void/IdeaProjects/compiler/src/FrontEnd/CST/Parser/Fate.g4 by ANTLR 4.6
package FrontEnd.CST.Parser;
import org.antlr.v4.runtime.tree.ParseTreeVisitor;

/**
 * This interface defines a complete generic visitor for a parse tree produced
 * by {@link FateParser}.
 *
 * @param <T> The return type of the visit operation. Use {@link Void} for
 * operations with no return type.
 */
public interface FateVisitor<T> extends ParseTreeVisitor<T> {
	/**
	 * Visit a parse tree produced by {@link FateParser#program}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitProgram(FateParser.ProgramContext ctx);
	/**
	 * Visit a parse tree produced by {@link FateParser#classDeclaration}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitClassDeclaration(FateParser.ClassDeclarationContext ctx);
	/**
	 * Visit a parse tree produced by {@link FateParser#functionDeclaration}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFunctionDeclaration(FateParser.FunctionDeclarationContext ctx);
	/**
	 * Visit a parse tree produced by {@link FateParser#variableDeclarationStatement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitVariableDeclarationStatement(FateParser.VariableDeclarationStatementContext ctx);
	/**
	 * Visit a parse tree produced by {@link FateParser#statement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitStatement(FateParser.StatementContext ctx);
	/**
	 * Visit a parse tree produced by {@link FateParser#blockStatement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBlockStatement(FateParser.BlockStatementContext ctx);
	/**
	 * Visit a parse tree produced by {@link FateParser#expressionStatement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExpressionStatement(FateParser.ExpressionStatementContext ctx);
	/**
	 * Visit a parse tree produced by {@link FateParser#selectionStatement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSelectionStatement(FateParser.SelectionStatementContext ctx);
	/**
	 * Visit a parse tree produced by the {@code whileStatement}
	 * labeled alternative in {@link FateParser#iterationStatement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitWhileStatement(FateParser.WhileStatementContext ctx);
	/**
	 * Visit a parse tree produced by the {@code forStatement}
	 * labeled alternative in {@link FateParser#iterationStatement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitForStatement(FateParser.ForStatementContext ctx);
	/**
	 * Visit a parse tree produced by the {@code continueStatement}
	 * labeled alternative in {@link FateParser#jumpStatement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitContinueStatement(FateParser.ContinueStatementContext ctx);
	/**
	 * Visit a parse tree produced by the {@code breakStatement}
	 * labeled alternative in {@link FateParser#jumpStatement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBreakStatement(FateParser.BreakStatementContext ctx);
	/**
	 * Visit a parse tree produced by the {@code returnStatement}
	 * labeled alternative in {@link FateParser#jumpStatement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitReturnStatement(FateParser.ReturnStatementContext ctx);
	/**
	 * Visit a parse tree produced by the {@code constantExpression}
	 * labeled alternative in {@link FateParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitConstantExpression(FateParser.ConstantExpressionContext ctx);
	/**
	 * Visit a parse tree produced by the {@code shiftExpression}
	 * labeled alternative in {@link FateParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitShiftExpression(FateParser.ShiftExpressionContext ctx);
	/**
	 * Visit a parse tree produced by the {@code subscriptExpression}
	 * labeled alternative in {@link FateParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSubscriptExpression(FateParser.SubscriptExpressionContext ctx);
	/**
	 * Visit a parse tree produced by the {@code relExpression}
	 * labeled alternative in {@link FateParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitRelExpression(FateParser.RelExpressionContext ctx);
	/**
	 * Visit a parse tree produced by the {@code logicorExpression}
	 * labeled alternative in {@link FateParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLogicorExpression(FateParser.LogicorExpressionContext ctx);
	/**
	 * Visit a parse tree produced by the {@code newExpression}
	 * labeled alternative in {@link FateParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitNewExpression(FateParser.NewExpressionContext ctx);
	/**
	 * Visit a parse tree produced by the {@code assignExpression}
	 * labeled alternative in {@link FateParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAssignExpression(FateParser.AssignExpressionContext ctx);
	/**
	 * Visit a parse tree produced by the {@code bitorExpression}
	 * labeled alternative in {@link FateParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBitorExpression(FateParser.BitorExpressionContext ctx);
	/**
	 * Visit a parse tree produced by the {@code bitxorExpression}
	 * labeled alternative in {@link FateParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBitxorExpression(FateParser.BitxorExpressionContext ctx);
	/**
	 * Visit a parse tree produced by the {@code variableExpression}
	 * labeled alternative in {@link FateParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitVariableExpression(FateParser.VariableExpressionContext ctx);
	/**
	 * Visit a parse tree produced by the {@code bitandExpression}
	 * labeled alternative in {@link FateParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBitandExpression(FateParser.BitandExpressionContext ctx);
	/**
	 * Visit a parse tree produced by the {@code addExpression}
	 * labeled alternative in {@link FateParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAddExpression(FateParser.AddExpressionContext ctx);
	/**
	 * Visit a parse tree produced by the {@code logicandExpression}
	 * labeled alternative in {@link FateParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLogicandExpression(FateParser.LogicandExpressionContext ctx);
	/**
	 * Visit a parse tree produced by the {@code suffixExpression}
	 * labeled alternative in {@link FateParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSuffixExpression(FateParser.SuffixExpressionContext ctx);
	/**
	 * Visit a parse tree produced by the {@code equalityExpression}
	 * labeled alternative in {@link FateParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitEqualityExpression(FateParser.EqualityExpressionContext ctx);
	/**
	 * Visit a parse tree produced by the {@code fieldExpression}
	 * labeled alternative in {@link FateParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFieldExpression(FateParser.FieldExpressionContext ctx);
	/**
	 * Visit a parse tree produced by the {@code unaryExpression}
	 * labeled alternative in {@link FateParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitUnaryExpression(FateParser.UnaryExpressionContext ctx);
	/**
	 * Visit a parse tree produced by the {@code subExpression}
	 * labeled alternative in {@link FateParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSubExpression(FateParser.SubExpressionContext ctx);
	/**
	 * Visit a parse tree produced by the {@code functioncallExpression}
	 * labeled alternative in {@link FateParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFunctioncallExpression(FateParser.FunctioncallExpressionContext ctx);
	/**
	 * Visit a parse tree produced by the {@code mulExpression}
	 * labeled alternative in {@link FateParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitMulExpression(FateParser.MulExpressionContext ctx);
	/**
	 * Visit a parse tree produced by the {@code arrayType}
	 * labeled alternative in {@link FateParser#type}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitArrayType(FateParser.ArrayTypeContext ctx);
	/**
	 * Visit a parse tree produced by the {@code intType}
	 * labeled alternative in {@link FateParser#type}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIntType(FateParser.IntTypeContext ctx);
	/**
	 * Visit a parse tree produced by the {@code stringType}
	 * labeled alternative in {@link FateParser#type}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitStringType(FateParser.StringTypeContext ctx);
	/**
	 * Visit a parse tree produced by the {@code voidType}
	 * labeled alternative in {@link FateParser#type}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitVoidType(FateParser.VoidTypeContext ctx);
	/**
	 * Visit a parse tree produced by the {@code boolType}
	 * labeled alternative in {@link FateParser#type}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBoolType(FateParser.BoolTypeContext ctx);
	/**
	 * Visit a parse tree produced by the {@code classType}
	 * labeled alternative in {@link FateParser#type}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitClassType(FateParser.ClassTypeContext ctx);
	/**
	 * Visit a parse tree produced by the {@code boolConstant}
	 * labeled alternative in {@link FateParser#constant}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBoolConstant(FateParser.BoolConstantContext ctx);
	/**
	 * Visit a parse tree produced by the {@code intConstant}
	 * labeled alternative in {@link FateParser#constant}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIntConstant(FateParser.IntConstantContext ctx);
	/**
	 * Visit a parse tree produced by the {@code stringConstant}
	 * labeled alternative in {@link FateParser#constant}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitStringConstant(FateParser.StringConstantContext ctx);
	/**
	 * Visit a parse tree produced by the {@code nullConstant}
	 * labeled alternative in {@link FateParser#constant}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitNullConstant(FateParser.NullConstantContext ctx);
}