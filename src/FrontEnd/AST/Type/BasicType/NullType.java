package FrontEnd.AST.Type.BasicType;

import FrontEnd.AST.Type.ArrayType;
import FrontEnd.AST.Type.ClassType.ClassType;
import FrontEnd.AST.Type.Type;

public class NullType extends Type{
    private static NullType ret = new NullType();

    public static Type getType(){
        return ret;
    }

    @Override
    public boolean compatiblewith(Type other) {
        return other instanceof NullType || other instanceof ArrayType || other instanceof ClassType;
    }
}