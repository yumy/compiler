package FrontEnd.AST.Type.BasicType;

import FrontEnd.AST.Type.Type;

public class BoolType extends Type{
    private static BoolType ret = new BoolType();

    public static Type getType(){
        return ret;
    }

    @Override
    public boolean compatiblewith(Type other) {
        return other instanceof BoolType;
    }

    @Override
    public int size() {
        return 8;
    }
}