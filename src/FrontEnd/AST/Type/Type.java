package FrontEnd.AST.Type;

import FrontEnd.AST.Node;
//import Compiler.Utility.Utility;

public abstract class Type implements  Node{
    public int size(){
        return 8;
    }
    public abstract boolean compatiblewith(Type other); // used for compare,
}