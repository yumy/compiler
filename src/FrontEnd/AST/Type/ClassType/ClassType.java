package FrontEnd.AST.Type.ClassType;
import Environment.ScopeTable.Scope;
import FrontEnd.AST.Function;
import FrontEnd.AST.Type.BasicType.NullType;
import FrontEnd.AST.Type.ClassType.Member.Member;
import FrontEnd.AST.Type.ClassType.Member.Memberfunction;
import FrontEnd.AST.Type.ClassType.Member.Membervariable;
import FrontEnd.AST.Type.Type;
import Utility.Error.CompilationError;
//import Compiler.Utility.Utility;
import java.util.HashMap;
import java.util.Map;
public class ClassType extends Type implements Scope{
    public String name;
    public Map<String,Membervariable> membervariables;
    public Map<String,Memberfunction> memberfunctions;
    public Function defaultconstructor;
    public int allocatespace;
    private ClassType(String name){
        this.name = name;
        this.allocatespace = 0;
        this.membervariables = new HashMap<>();
        this.memberfunctions = new HashMap<>();
        this.defaultconstructor = null;
    }
    public static Type getType(String name){
        return new ClassType(name);
    }
    public void addMember(String name,Type type){
        if (include(name)) {
            throw new CompilationError("one class can't have two members with same name");
        }
        if (type instanceof Function){
            Function function = (Function)type;
            function.name = this.name + "." + function.name;
            Memberfunction member = new Memberfunction(name,function);
            memberfunctions.put(name,member);
        }else{
            Membervariable member = new Membervariable(this,name,type);
            membervariables.put(name,member);
        }
    }
    public void addconstructor(Function function){
        if (function.getParameterTypes().size() > 1) {
            throw new CompilationError("construtor of class has morn then one parameter");
        }
        if (defaultconstructor != null) {
            throw new CompilationError("one class has two default constructor");
        }
        function.name = name + ".constructor";
        defaultconstructor = function;
    }
    public Member getMember(String name){
        Member member = null;
        if (membervariables.containsKey(name)){
            member = membervariables.get(name);
        }
        if (memberfunctions.containsKey(name)){
            member = memberfunctions.get(name);
        }
        if (member == null) throw new CompilationError("class has no member named" + name);
        return member;
    }
    public boolean include(String name){
        return memberfunctions.containsKey(name) || membervariables.containsKey(name);
    }
    @Override
    public boolean compatiblewith(Type other){
        return other instanceof NullType || other == this;
    }
}