package FrontEnd.AST.Type.ClassType.Member;
import FrontEnd.AST.Expression.Expression;
import FrontEnd.AST.Type.Type;
import FrontEnd.AST.Type.ClassType.ClassType;
//import Compiler.Utility.Utility;
public class Membervariable extends Member{
    public Type type;
    public Expression expression;
    public int offset;
    public Membervariable(ClassType classtype,String name,Type type){
        super(name);
        this.offset = classtype.allocatespace;
        classtype.allocatespace += 8;
        this.type = type;
    }
}