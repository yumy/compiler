package FrontEnd.AST.Type.ClassType.Member;
import FrontEnd.AST.Function;

public class Memberfunction extends Member{
    public Function function;
    public Memberfunction(String name,Function function){
        super(name);
        this.function = function;
    }
}