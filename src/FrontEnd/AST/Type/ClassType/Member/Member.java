package FrontEnd.AST.Type.ClassType.Member;
import Environment.Environment;
import FrontEnd.AST.Type.ClassType.ClassType;
//import Compiler.Utility.Error.InternalError;
public abstract class Member{
    public String name;
    public ClassType origin;
    Member(String name){
        this.name = name;
        this.origin = Environment.scopetable.getClassScope();
    }
}