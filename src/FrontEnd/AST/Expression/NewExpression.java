package FrontEnd.AST.Expression;
import BackEnd.ControlFlowGraph.Instruction.ArithmeticInstruction.BinaryInstruction.*;
import BackEnd.ControlFlowGraph.Instruction.ControlFlowInstruction.BranchInstruction;
import BackEnd.ControlFlowGraph.Instruction.ControlFlowInstruction.JumpInstruction;
import BackEnd.ControlFlowGraph.Instruction.FunctionInstruction.CallInstruction;
import BackEnd.ControlFlowGraph.Instruction.Instruction;
import BackEnd.ControlFlowGraph.Instruction.LabelInstruction;
import BackEnd.ControlFlowGraph.Instruction.MemoryInstruction.MoveInstruction;
import Environment.Environment;
import BackEnd.ControlFlowGraph.Instruction.MemoryInstruction.AllocateInstruction;
import BackEnd.ControlFlowGraph.Instruction.MemoryInstruction.StoreInstruction;
import BackEnd.ControlFlowGraph.Operand.Address;
import BackEnd.ControlFlowGraph.Operand.ImmediateValue;
import BackEnd.ControlFlowGraph.Operand.Operand;
import BackEnd.ControlFlowGraph.Operand.VirtualRegister.VirtualRegister;
import FrontEnd.AST.Function;
import FrontEnd.AST.Type.ArrayType;
import FrontEnd.AST.Type.ClassType.ClassType;
import FrontEnd.AST.Type.Type;
import Utility.Error.CompilationError;
import jdk.nashorn.internal.runtime.regexp.joni.constants.OPCode;
//import Compiler.Utility.Utility;
import java.awt.datatransfer.MimeTypeParseException;
import java.util.List;
import java.util.ArrayList;
public class NewExpression extends Expression{
    public List<Expression> expressions;
    public Function defaultconstructor;
    private NewExpression(List<Expression> expressions,Type type,boolean isleftvalue){
        super(type,isleftvalue);
        this.expressions = expressions;
        this.defaultconstructor = null;
    }
    public static Expression getExpression(Type type,List<Expression> expressions){
        if (expressions.isEmpty()){
            if (type instanceof ClassType){
                return new NewExpression(expressions,type,false);
            }
            throw new CompilationError("newexpression error");
        }
        Type x = ArrayType.getType(type,expressions.size());
        return new NewExpression(expressions,x,false);
    }
    public Operand newClass(Type x, List<Instruction> instructions) {
        VirtualRegister dest = Environment.registertable.addTemporaryRegister();
        ClassType classType = (ClassType)x;
        instructions.add(AllocateInstruction.getInstruction(dest, new ImmediateValue(classType.allocatespace)));
        classType.membervariables.forEach((name, member) -> {
            Address address = new Address((VirtualRegister)dest,member.offset);
            if (member.expression != null) {
                member.expression.emit(instructions);
                member.expression.load(instructions);
                instructions.add(StoreInstruction.getInstruction(member.expression.operand, address));
            }
        });

        if (defaultconstructor == null) {
            if (classType.defaultconstructor != null) {
                defaultconstructor = classType.defaultconstructor;
            }
        }
        if (defaultconstructor != null) {
            List<Operand> operands = new ArrayList<Operand>() {{
                add(dest);
            }};
            instructions.add(CallInstruction.getInstruction(null,defaultconstructor, operands));
        }
        return dest;
    }
    public Operand night(List<Instruction> instructions, int dim) {
        Operand size = Environment.registertable.addTemporaryRegister();
        Operand dest = Environment.registertable.addTemporaryRegister();
        if (dim == 0 && expressions.get(dim) == null) {
            return dest;
        }
        instructions.add(MoveInstruction.getInstruction(size,expressions.get(dim).operand));
        instructions.add(AddInstruction.getInstruction(size, size, new ImmediateValue(1)));
        instructions.add(MultiplyInstruction.getInstruction(size, size, new ImmediateValue(8)));
        instructions.add(AllocateInstruction.getInstruction(dest, size));
        Address address = new Address((VirtualRegister)dest,0);
        instructions.add(StoreInstruction.getInstruction(expressions.get(dim).operand, address));
        instructions.add(AddInstruction.getInstruction(dest, dest, new ImmediateValue(8)));

        int flag = 0;
        if (((ArrayType)type).baseType instanceof ClassType && dim + 1 == expressions.size() && expressions.get(dim) != null) {
            flag = 1;
        }

        if (dim + 1 < expressions.size() && expressions.get(dim + 1) != null) {
            flag = 2;
        }

        if (flag != 0) {
            instructions.add(MoveInstruction.getInstruction(size, expressions.get(dim).operand));
            LabelInstruction loop = LabelInstruction.getInstruction("while_loop");
            instructions.add(JumpInstruction.getInstruction(loop));
            instructions.add(loop);
            instructions.add(MinusInstruction.getInstruction(size, size,new ImmediateValue(1)));

            VirtualRegister tsize = Environment.registertable.addTemporaryRegister();
            instructions.add(MoveInstruction.getInstruction(tsize, size));
            instructions.add(MultiplyInstruction.getInstruction(size, size, new ImmediateValue(8)));
            instructions.add(AddInstruction.getInstruction(size, size, dest));
            address = new Address((VirtualRegister)size,0);
            if (flag == 1) {
                instructions.add(StoreInstruction.getInstruction(newClass(((ArrayType)type).baseType, instructions), address));
            } else {
                instructions.add(StoreInstruction.getInstruction(night(instructions, dim + 1), address));
            }
            LabelInstruction merge = LabelInstruction.getInstruction("while_merge");
            VirtualRegister conditon = Environment.registertable.addTemporaryRegister();
            instructions.add(NotequalInstruction.getInstruction(conditon, tsize, new ImmediateValue(0)));
            instructions.add(MoveInstruction.getInstruction(size, tsize));
            instructions.add(BranchInstruction.getInstruction(conditon, loop, merge));
            instructions.add(merge);
        }
        return dest;
    }

    @Override
    public void emit(List<Instruction> instructions) {
        expressions.stream().filter(expression -> expression != null).forEach(expression -> {
            expression.emit(instructions);
            expression.load(instructions);
        });

        if (type instanceof ClassType) {
            operand = newClass(type,instructions);
        } else {
            if (type instanceof ArrayType) {
                operand = night(instructions, 0);
            }
        }
    }
}