package FrontEnd.AST.Expression.BinaryExpression;
import BackEnd.ControlFlowGraph.Instruction.ArithmeticInstruction.BinaryInstruction.GreaterInstruction;
import BackEnd.ControlFlowGraph.Instruction.Instruction;
import Environment.Environment;
import FrontEnd.AST.Expression.ConstantExpression.Boolconstant;
import FrontEnd.AST.Expression.ConstantExpression.IntConstant;
import FrontEnd.AST.Expression.ConstantExpression.StringConstant;
import FrontEnd.AST.Expression.Expression;
import FrontEnd.AST.Expression.FunctioncallExpression;
import FrontEnd.AST.Function;
import FrontEnd.AST.Type.BasicType.BoolType;
import FrontEnd.AST.Type.BasicType.IntType;
import FrontEnd.AST.Type.BasicType.StringType;
import FrontEnd.AST.Type.Type;
import Utility.Error.CompilationError;
import java.util.ArrayList;
import java.util.List;
public class GreaterExpression extends BinaryExpression{
    GreaterExpression(Expression left,Expression right,Type type,boolean isleftvalue){
        super(left,right,type,isleftvalue);
    }
    public static Expression getExpression(Expression left,Expression right){
        if (left.type instanceof IntType && right.type instanceof IntType){
            if (left instanceof IntConstant && right instanceof IntConstant){
                int leftvalue = ((IntConstant)(left)).value;
                int rightvalue = ((IntConstant)(right)).value;
                return Boolconstant.getConstant(leftvalue > rightvalue);
            }
            return new GreaterExpression(left,right,BoolType.getType(),false);
        }else if (left.type instanceof StringType && right.type instanceof StringType){
            if (left instanceof StringConstant && right instanceof StringConstant){
                String v1 = ((StringConstant)(left)).value;
                String v2 = ((StringConstant)(right)).value;
                return Boolconstant.getConstant(v1.compareTo(v2) > 0);
            }
            return FunctioncallExpression.getExpression(
                    (Function)Environment.symboltable.getSymbol("_builtin_string_greater").type,
                    new ArrayList<Expression>() {{
                        add(left);
                        add(right);
                    }}
            );
        }
        throw new CompilationError("greater wrong");
    }
    @Override
    public void emit(List<Instruction> instructions){
        left.emit(instructions);
        left.load(instructions);
        right.emit(instructions);
        right.load(instructions);
        operand = Environment.registertable.addTemporaryRegister();
        instructions.add(GreaterInstruction.getInstruction(operand,left.operand,right.operand));
    }
}