package FrontEnd.AST.Expression.BinaryExpression;
import BackEnd.ControlFlowGraph.Instruction.Instruction;
import BackEnd.ControlFlowGraph.Instruction.MemoryInstruction.LoadInstruction;
import BackEnd.ControlFlowGraph.Instruction.MemoryInstruction.MoveInstruction;
import BackEnd.ControlFlowGraph.Instruction.MemoryInstruction.StoreInstruction;
import BackEnd.ControlFlowGraph.Operand.Address;
import FrontEnd.AST.Expression.Expression;
import Environment.Environment;
import FrontEnd.AST.Type.Type;
import Utility.Error.CompilationError;
import java.util.List;
public class AssignmentExpression extends BinaryExpression{
    private AssignmentExpression(Type type,boolean isLeftValue,Expression left, Expression right){
        super(left,right,type,isLeftValue);
    }
    public static Expression getExpression(Expression left, Expression right) {
        if (!left.isleftvalue) {
            throw new CompilationError("assignmentexpression error");
        }
        if (left.type.compatiblewith(right.type)) {
            return new AssignmentExpression(left.type, true, left, right);
        }
        throw new CompilationError("assignmentexpression error");
    }
    @Override
    public void emit(List<Instruction> instructions){
        left.emit(instructions);
        right.emit(instructions);
        right.load(instructions);
        operand = left.operand;
        if (left.operand instanceof Address){
            instructions.add(StoreInstruction.getInstruction(right.operand,left.operand));
        }else{
            instructions.add(MoveInstruction.getInstruction(left.operand,right.operand));
        }
    }

    @Override
    public void load(List<Instruction> instructions){
        if (operand instanceof Address) {
            Address address = (Address)operand;
            operand = Environment.registertable.addTemporaryRegister();
            instructions.add(LoadInstruction.getInstruction(operand,address));
        }
    }
}