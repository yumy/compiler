package FrontEnd.AST.Expression.BinaryExpression;
import BackEnd.ControlFlowGraph.Instruction.ArithmeticInstruction.BinaryInstruction.AddInstruction;
import BackEnd.ControlFlowGraph.Instruction.Instruction;
import Environment.Environment;
import FrontEnd.AST.Expression.ConstantExpression.IntConstant;
import FrontEnd.AST.Expression.ConstantExpression.StringConstant;
import FrontEnd.AST.Expression.Expression;
import FrontEnd.AST.Expression.FunctioncallExpression;
import FrontEnd.AST.Function;
import FrontEnd.AST.Type.BasicType.IntType;
import FrontEnd.AST.Type.BasicType.StringType;
import FrontEnd.AST.Type.Type;
import Utility.Error.CompilationError;
import java.util.ArrayList;
import java.util.List;
public class AddExpression extends BinaryExpression{
    private AddExpression(Expression left,Expression right,Type type,boolean isleftvalue){
        super(left,right,type,isleftvalue);
    }
    public static Expression getExpression(Expression left,Expression right){
        if (left.type instanceof IntType && right.type instanceof IntType){
            if (left instanceof IntConstant && right instanceof IntConstant){
                int leftvalue = ((IntConstant)(left)).value;
                int rightvalue = ((IntConstant)(right)).value;
                return IntConstant.getConstant(leftvalue + rightvalue);
            }
            return new AddExpression(left,right,IntType.getType(),false);
        }
        if (left.type instanceof StringType && right.type instanceof StringType){
            if (left instanceof StringConstant && right instanceof StringConstant){
                String x = ((StringConstant)(left)).value;
                String y = ((StringConstant)(right)).value;
                return StringConstant.getConstant(x + y);
            }
            return FunctioncallExpression.getExpression(
                    (Function)Environment.symboltable.getSymbol("_builtin_string_concatenate").type,
                    new ArrayList<Expression>() {{
                            add(left);
                            add(right);
                        }}
            );
        }
        throw new CompilationError("illegal addexpression");
    }
    @Override
    public void emit(List<Instruction> instructions){
        left.emit(instructions);
        left.load(instructions);
        right.emit(instructions);
        right.load(instructions);
        operand = Environment.registertable.addTemporaryRegister();
        instructions.add(AddInstruction.getInstruction(operand, left.operand, right.operand));
    }
}