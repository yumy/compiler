package FrontEnd.AST.Expression.BinaryExpression;
import BackEnd.ControlFlowGraph.Instruction.ArithmeticInstruction.BinaryInstruction.BitandInstruction;
import BackEnd.ControlFlowGraph.Instruction.Instruction;
import FrontEnd.AST.Expression.ConstantExpression.IntConstant;
import FrontEnd.AST.Expression.Expression;
import FrontEnd.AST.Type.BasicType.IntType;
import Environment.Environment;
import FrontEnd.AST.Type.Type;
import Utility.Error.CompilationError;
import java.util.List;
public class BitandExpression extends BinaryExpression{
    BitandExpression(Expression left,Expression right,Type type,boolean isleftvalue){
        super(left,right,type,isleftvalue);
    }
    public static Expression getExpression(Expression left,Expression right){
        if (left.type instanceof IntType && right.type instanceof IntType){
            if (left instanceof IntConstant && right instanceof IntConstant){
                int leftvalue = ((IntConstant)(left)).value;
                int rightvalue = ((IntConstant)(right)).value;
                return IntConstant.getConstant(leftvalue & rightvalue);
            }
            return new BitandExpression(left,right,IntType.getType(),false);
        }
        throw new CompilationError("illegal bitandexpression");
    }
    @Override
    public void emit(List<Instruction> instructions) {
        left.emit(instructions);
        left.load(instructions);
        right.emit(instructions);
        right.load(instructions);
        operand = Environment.registertable.addTemporaryRegister();
        instructions.add(BitandInstruction.getInstruction(operand,left.operand,right.operand));
    }
}