package FrontEnd.AST.Expression.BinaryExpression;
import BackEnd.ControlFlowGraph.Instruction.ArithmeticInstruction.BinaryInstruction.NotequalInstruction;
import BackEnd.ControlFlowGraph.Instruction.Instruction;
import Environment.Environment;
import FrontEnd.AST.Expression.ConstantExpression.Boolconstant;
import FrontEnd.AST.Expression.ConstantExpression.IntConstant;
import FrontEnd.AST.Expression.ConstantExpression.NullConstant;
import FrontEnd.AST.Expression.ConstantExpression.StringConstant;
import FrontEnd.AST.Expression.Expression;
import FrontEnd.AST.Expression.FunctioncallExpression;
import FrontEnd.AST.Function;
import FrontEnd.AST.Type.BasicType.BoolType;
import FrontEnd.AST.Type.BasicType.StringType;
import FrontEnd.AST.Type.Type;
import Utility.Error.CompilationError;
import java.util.ArrayList;
import java.util.List;
public class NotequalExpression extends BinaryExpression{
    public NotequalExpression(Expression left,Expression right,Type type,boolean isleftvalue){
        super(left,right,type,isleftvalue);
    }
    public static Expression getExpression(Expression left,Expression right){
        if (!left.type.compatiblewith(right.type)) {
            throw new CompilationError("CE");
        }
        if (left instanceof NullConstant && right instanceof NullConstant){
            return Boolconstant.getConstant(false);
        }
        if (left instanceof IntConstant && right instanceof IntConstant){
            int x = ((IntConstant)(left)).value;
            int y = ((IntConstant)(right)).value;
            return Boolconstant.getConstant(x != y);
        }
        if (left instanceof Boolconstant && right instanceof Boolconstant){
            boolean x = ((Boolconstant)(left)).value;
            boolean y = ((Boolconstant)(right)).value;
            return Boolconstant.getConstant(x != y);
        }
        if (left instanceof StringConstant && right instanceof StringConstant){
            String x = ((StringConstant)(left)).value;
            String y = ((StringConstant)(right)).value;
            return Boolconstant.getConstant(!x.equals(y));
        }
        if (left.type instanceof StringType && right.type instanceof StringType){
            return FunctioncallExpression.getExpression(
                    (Function)Environment.symboltable.getSymbol("_builtin_string_notequal").type,
                    new ArrayList<Expression>() {{
                        add(left);
                        add(right);
                    }}
            );
        }
        return new NotequalExpression(left,right,BoolType.getType(),false);
    }
    @Override
    public void emit(List<Instruction> instructions){
        left.emit(instructions);
        left.load(instructions);
        right.emit(instructions);
        right.load(instructions);
        operand = Environment.registertable.addTemporaryRegister();
        instructions.add(NotequalInstruction.getInstruction(operand,left.operand,right.operand));
    }
}