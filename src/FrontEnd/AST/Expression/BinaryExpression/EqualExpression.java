package FrontEnd.AST.Expression.BinaryExpression;
import Environment.Environment;
import BackEnd.ControlFlowGraph.Instruction.ArithmeticInstruction.BinaryInstruction.EqualInstruction;
import BackEnd.ControlFlowGraph.Instruction.Instruction;
import FrontEnd.AST.Expression.ConstantExpression.Boolconstant;
import FrontEnd.AST.Expression.ConstantExpression.IntConstant;
import FrontEnd.AST.Expression.ConstantExpression.NullConstant;
import FrontEnd.AST.Expression.ConstantExpression.StringConstant;
import FrontEnd.AST.Expression.Expression;
import FrontEnd.AST.Expression.FunctioncallExpression;
import FrontEnd.AST.Function;
import FrontEnd.AST.Type.BasicType.BoolType;
import FrontEnd.AST.Type.BasicType.StringType;
import FrontEnd.AST.Type.Type;
import Utility.Error.CompilationError;
import java.util.ArrayList;
import java.util.List;
public class EqualExpression extends BinaryExpression{
    EqualExpression(Expression left,Expression right,Type type,boolean isleftvalue){
        super(left,right,type,isleftvalue);
    }
    public static Expression getExpression(Expression left,Expression right){
        if (!left.type.compatiblewith(right.type)) {
            throw new CompilationError("different type over equalexpression");
        }
        if (left instanceof NullConstant && right instanceof NullConstant){
            return Boolconstant.getConstant(true);
        }
        if (left instanceof IntConstant && right instanceof IntConstant){
            int x = ((IntConstant)(left)).value;
            int y = ((IntConstant)(right)).value;
            return Boolconstant.getConstant(x == y);
        }
        if (left instanceof Boolconstant && right instanceof Boolconstant){
            boolean x = ((Boolconstant)(left)).value;
            boolean y = ((Boolconstant)(right)).value;
            return Boolconstant.getConstant(x == y);
        }
        if (left instanceof StringConstant && right instanceof StringConstant){
            String x = ((StringConstant)(left)).value;
            String y = ((StringConstant)(right)).value;
            return Boolconstant.getConstant(x.equals(y));
        }
        if (left.type instanceof StringType && right.type instanceof StringType){
            return FunctioncallExpression.getExpression(
                    (Function)Environment.symboltable.getSymbol("_builtin_string_equal").type,
                    new ArrayList<Expression>() {{
                        add(left);
                        add(right);
                    }}
            );
        }
        return new EqualExpression(left,right,BoolType.getType(),false);
    }
    @Override
    public void emit(List<Instruction> instructions){
        left.emit(instructions);
        left.load(instructions);
        right.emit(instructions);
        right.load(instructions);
        operand = Environment.registertable.addTemporaryRegister();
        instructions.add(EqualInstruction.getInstruction(operand,left.operand,right.operand));
    }
}