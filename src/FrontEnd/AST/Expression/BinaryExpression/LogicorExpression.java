package FrontEnd.AST.Expression.BinaryExpression;
import BackEnd.ControlFlowGraph.Instruction.ControlFlowInstruction.BranchInstruction;
import BackEnd.ControlFlowGraph.Instruction.ControlFlowInstruction.JumpInstruction;
import BackEnd.ControlFlowGraph.Instruction.Instruction;
import BackEnd.ControlFlowGraph.Instruction.LabelInstruction;
import BackEnd.ControlFlowGraph.Instruction.MemoryInstruction.MoveInstruction;
import BackEnd.ControlFlowGraph.Operand.ImmediateValue;
import Environment.Environment;
import FrontEnd.AST.Expression.ConstantExpression.Boolconstant;
import FrontEnd.AST.Expression.Expression;
import FrontEnd.AST.Type.BasicType.BoolType;
import FrontEnd.AST.Type.Type;
import Utility.Error.CompilationError;
import java.util.List;
public class LogicorExpression extends BinaryExpression{
    LogicorExpression(Expression left,Expression right,Type type,boolean isleftvalue){
        super(left,right,type,isleftvalue);
    }
    public static Expression getExpression(Expression left,Expression right){
        if (left.type instanceof BoolType && right.type instanceof BoolType){
            if (left instanceof Boolconstant && right instanceof Boolconstant){
                boolean leftvalue = ((Boolconstant)(left)).value;
                boolean rightvalue = ((Boolconstant)(right)).value;
                return Boolconstant.getConstant(leftvalue || rightvalue);
            }
            return new LogicorExpression(left,right,BoolType.getType(),false);
        }
        throw new CompilationError("logicor illegal");
    }
    @Override
    public void emit(List<Instruction> instructions){
        LabelInstruction trueLabel = LabelInstruction.getInstruction("logical_true");
        LabelInstruction falseLabel = LabelInstruction.getInstruction("logical_false");
        LabelInstruction mergeLabel = LabelInstruction.getInstruction("logical_merge");
		/*
			%...:
				****left****
				branch $left, %logical_true, %logical_false
			%logical_false:
				****right****
				$operand = move $right
				goto %logical_merge
			%logical_true:
				$operand = move 1
				goto %logical_merge
			%logical_merge:
				...
		 */
        operand = Environment.registertable.addTemporaryRegister();
        left.emit(instructions);
        left.load(instructions);
        instructions.add(BranchInstruction.getInstruction(left.operand, trueLabel, falseLabel));
        instructions.add(falseLabel);
        right.emit(instructions);
        right.load(instructions);
        operand = right.operand;
        instructions.add(JumpInstruction.getInstruction(mergeLabel));
        instructions.add(trueLabel);
        instructions.add(MoveInstruction.getInstruction(operand,new ImmediateValue(1)));
        instructions.add(JumpInstruction.getInstruction(mergeLabel));
        instructions.add(mergeLabel);
    }
}