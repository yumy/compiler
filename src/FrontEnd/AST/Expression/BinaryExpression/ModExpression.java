package FrontEnd.AST.Expression.BinaryExpression;
import BackEnd.ControlFlowGraph.Instruction.ArithmeticInstruction.BinaryInstruction.ModInstruction;
import BackEnd.ControlFlowGraph.Instruction.Instruction;
import Environment.Environment;
import FrontEnd.AST.Expression.ConstantExpression.IntConstant;
import FrontEnd.AST.Expression.Expression;
import FrontEnd.AST.Type.BasicType.IntType;
import FrontEnd.AST.Type.Type;
import Utility.Error.CompilationError;
import java.util.List;
public class ModExpression extends BinaryExpression{
    ModExpression(Expression left,Expression right,Type type,boolean isleftvalue){
        super(left,right,type,isleftvalue);
    }
    public static Expression getExpression(Expression left,Expression right){
        if (left.type instanceof IntType && right.type instanceof IntType){
            if (right instanceof IntConstant){
                int rightvalue = ((IntConstant)(right)).value;
                if (rightvalue == 0) throw new CompilationError("mod 0");
                if (left instanceof IntConstant){
                    int leftvalue = ((IntConstant)(left)).value;
                    return IntConstant.getConstant(leftvalue % rightvalue);
                }
            }
            return new ModExpression(left,right,IntType.getType(),false);
        }
        throw new CompilationError("mod wrong");
    }
    @Override
    public void emit(List<Instruction> instructions) {
        left.emit(instructions);
        left.load(instructions);
        right.emit(instructions);
        right.load(instructions);
        operand = Environment.registertable.addTemporaryRegister();
        instructions.add(ModInstruction.getInstruction(operand,left.operand,right.operand));
    }
}