package FrontEnd.AST.Expression.BinaryExpression;
import BackEnd.ControlFlowGraph.Instruction.ControlFlowInstruction.BranchInstruction;
import BackEnd.ControlFlowGraph.Instruction.ControlFlowInstruction.JumpInstruction;
import BackEnd.ControlFlowGraph.Instruction.Instruction;
import BackEnd.ControlFlowGraph.Instruction.LabelInstruction;
import BackEnd.ControlFlowGraph.Instruction.MemoryInstruction.MoveInstruction;
import BackEnd.ControlFlowGraph.Operand.ImmediateValue;
import FrontEnd.AST.Expression.ConstantExpression.Boolconstant;
import FrontEnd.AST.Expression.Expression;
import FrontEnd.AST.Type.BasicType.BoolType;
import FrontEnd.AST.Type.Type;
import Utility.Error.CompilationError;
import java.util.List;
public class LogicandExpression extends BinaryExpression{
    LogicandExpression(Expression left,Expression right,Type type,boolean isleftvalue){
        super(left,right,type,isleftvalue);
    }
    public static Expression getExpression(Expression left,Expression right){
        if (left.type instanceof BoolType && right.type instanceof BoolType){
            if (left instanceof Boolconstant && right instanceof Boolconstant){
                boolean leftvalue = ((Boolconstant)(left)).value;
                boolean rightvalue = ((Boolconstant)(right)).value;
                return Boolconstant.getConstant(leftvalue && rightvalue);
            }
            return new LogicandExpression(left,right,BoolType.getType(),false);
        }
        throw new CompilationError("logicand needs two bool");
    }
    @Override
    public void emit(List<Instruction> instructions){
        LabelInstruction trueLabel = LabelInstruction.getInstruction("logical_true");
        LabelInstruction falseLabel = LabelInstruction.getInstruction("logical_false");
        LabelInstruction mergeLabel = LabelInstruction.getInstruction("logical_merge");
		/*
			%...:
				****left****
				branch $left, %logical_true, %logical_false
			%logical_true:
				****right****
				$operand = move $right
				goto logical_merge
			%logical_false:
				$operand = move 0
				goto logical_merge
			%logical_merge:
				...
		 */
        left.emit(instructions);
        left.load(instructions);
        instructions.add(BranchInstruction.getInstruction(left.operand,trueLabel,falseLabel));
        instructions.add(trueLabel);
        right.emit(instructions);
        right.load(instructions);
        operand = right.operand;
        instructions.add(JumpInstruction.getInstruction(mergeLabel));
        instructions.add(falseLabel);
        instructions.add(MoveInstruction.getInstruction(operand,new ImmediateValue(0)));
        instructions.add(JumpInstruction.getInstruction(mergeLabel));
        instructions.add(mergeLabel);
    }
}