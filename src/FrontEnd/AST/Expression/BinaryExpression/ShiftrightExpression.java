package FrontEnd.AST.Expression.BinaryExpression;
import BackEnd.ControlFlowGraph.Instruction.ArithmeticInstruction.BinaryInstruction.ShiftrightInstruction;
import BackEnd.ControlFlowGraph.Instruction.Instruction;
import FrontEnd.AST.Expression.ConstantExpression.IntConstant;
import Environment.Environment;
import FrontEnd.AST.Expression.Expression;
import FrontEnd.AST.Type.BasicType.IntType;
import FrontEnd.AST.Type.Type;
import Utility.Error.CompilationError;
import java.util.List;
public class ShiftrightExpression extends BinaryExpression{
    ShiftrightExpression(Expression left,Expression right,Type type,boolean isleftvalue){
        super(left,right,type,isleftvalue);
    }
    public static Expression getExpression(Expression left,Expression right){
        if (left.type instanceof IntType && right.type instanceof IntType){
            if (left instanceof IntConstant && right instanceof IntConstant){
                int leftvalue = ((IntConstant)(left)).value;
                int rightvalue = ((IntConstant)(right)).value;
                return IntConstant.getConstant(leftvalue >> rightvalue);
            }
            return new ShiftrightExpression(left,right,IntType.getType(),false);
        }
        throw new CompilationError("wrong shiftright");
    }
    @Override
    public void emit(List<Instruction> instructions) {
        left.emit(instructions);
        left.load(instructions);
        right.emit(instructions);
        right.load(instructions);
        operand = Environment.registertable.addTemporaryRegister();
        instructions.add(ShiftrightInstruction.getInstruction(operand,left.operand,right.operand));
    }
}