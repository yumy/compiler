package FrontEnd.AST.Expression;
import BackEnd.ControlFlowGraph.Instruction.FunctionInstruction.CallInstruction;
import BackEnd.ControlFlowGraph.Instruction.Instruction;
import BackEnd.ControlFlowGraph.Operand.Operand;
import Environment.Environment;
import FrontEnd.AST.Expression.VariableExpression.FieldExpression;
import FrontEnd.AST.Function;
import FrontEnd.AST.Type.ClassType.ClassType;
import FrontEnd.AST.Type.Type;
import FrontEnd.AST.Type.BasicType.VoidType;
import Utility.Error.CompilationError;
//import Compiler.Utility.Utility;
import java.util.List;
import java.util.ArrayList;
public class FunctioncallExpression extends Expression{
    public Function func;
    public List<Expression> parameters;
    private FunctioncallExpression(Function func, List<Expression> parameters, Type type, boolean isleftvalue){
        super(type,isleftvalue);
        this.func = func;
        this.parameters = parameters;
    }
    public static Expression getExpression(Function func,List<Expression>parameters){
        return new FunctioncallExpression(func,parameters,func.type,false);
    }
    public static Expression getExpression(Expression expression,List<Expression>parameters){
        if (expression.type instanceof Function){
            Function function = (Function)expression.type;
            if (expression instanceof FieldExpression){
                //	member function call : add "this" to the parameter list
                parameters.add(0, ((FieldExpression)expression).expression);
            }
            if (parameters.size() != function.parameters.size()){
                throw new CompilationError("the number of parameters in the function-call expression is wrong");
            }
            for (int i = 0; i < parameters.size();i ++){
                if (i == 0 && expression instanceof FieldExpression){
                    //	no need to compare the type of "this"
                    continue;
                }
                if (!(parameters.get(i).type.compatiblewith(function.parameters.get(i).type))){
                    throw new CompilationError("CE");
                }
            }
            return new FunctioncallExpression(function,parameters,function.type,false);
        }
        throw new CompilationError("function call error");
    }

    @Override
    public void emit(List<Instruction> instructions) {
        List<Operand> operands = new ArrayList<>();
        for (Expression parameter : parameters) {
            parameter.emit(instructions);
            parameter.load(instructions);
            operands.add(parameter.operand);
        }
        if (type instanceof VoidType) {
            instructions.add(CallInstruction.getInstruction(null,func,operands));
        } else {
            operand = Environment.registertable.addTemporaryRegister();
            instructions.add(CallInstruction.getInstruction(operand,func,operands));
        }
    }
}