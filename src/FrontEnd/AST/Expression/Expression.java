package FrontEnd.AST.Expression;
import BackEnd.ControlFlowGraph.Instruction.Instruction;
import BackEnd.ControlFlowGraph.Operand.Operand;
import FrontEnd.AST.Node;
import FrontEnd.AST.Type.Type;
import java.util.List;
public abstract class Expression implements Node{
    public Type type;
    public boolean isleftvalue;
    public Operand operand;
    public Expression(Type type,boolean isleftvalue){
        this.type = type;
        this.isleftvalue = isleftvalue;
        operand = null;
    }
    public abstract void emit(List<Instruction> instructions);
    public void load(List<Instruction> instructions) {
    }
}