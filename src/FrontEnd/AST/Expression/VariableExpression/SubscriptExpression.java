package FrontEnd.AST.Expression.VariableExpression;
import BackEnd.ControlFlowGraph.Instruction.ArithmeticInstruction.BinaryInstruction.AddInstruction;
import BackEnd.ControlFlowGraph.Instruction.ArithmeticInstruction.BinaryInstruction.MultiplyInstruction;
import BackEnd.ControlFlowGraph.Instruction.Instruction;
import BackEnd.ControlFlowGraph.Instruction.MemoryInstruction.LoadInstruction;
import BackEnd.ControlFlowGraph.Operand.Address;
import BackEnd.ControlFlowGraph.Operand.ImmediateValue;
import BackEnd.ControlFlowGraph.Operand.VirtualRegister.VirtualRegister;
import Environment.Environment;
import FrontEnd.AST.Expression.Expression;
import FrontEnd.AST.Type.ArrayType;
import FrontEnd.AST.Type.BasicType.IntType;
import FrontEnd.AST.Type.Type;
import Utility.Error.CompilationError;
//import Compiler.Utility.Utility;
import java.util.List;
public class SubscriptExpression extends Expression{
    public Expression sup,inf;
    private SubscriptExpression(Expression sup, Expression inf, boolean isleftvalue, Type type){
        super(type,isleftvalue);
        this.sup = sup;
        this.inf = inf;
    }
    public static Expression getExpression(Expression sup,Expression inf){
        if (!(sup.type instanceof ArrayType)){
            throw new CompilationError("subscript error");
        }
        if (!(inf.type instanceof IntType)){
            throw new CompilationError("subscript error");
        }
        return new SubscriptExpression(sup,inf,sup.isleftvalue,((ArrayType)(sup.type)).reduce());
    }
    @Override
    public void emit(List<Instruction> instructions) {
        sup.emit(instructions);
        sup.load(instructions);
        inf.emit(instructions);
        inf.load(instructions);
        VirtualRegister address = Environment.registertable.addTemporaryRegister();
        VirtualRegister delta = Environment.registertable.addTemporaryRegister();
        instructions.add(MultiplyInstruction.getInstruction(delta,inf.operand,new ImmediateValue(type.size())));
        instructions.add(AddInstruction.getInstruction(address,sup.operand,delta));
        operand = new Address(address,type.size());
    }

    @Override
    public void load(List<Instruction> instructions) {
        if (operand instanceof Address) {
            Address address = (Address)operand;
            operand = Environment.registertable.addTemporaryRegister();
            instructions.add(LoadInstruction.getInstruction(operand,address));
        }
    }
}