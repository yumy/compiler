package FrontEnd.AST.Expression.VariableExpression;
import BackEnd.ControlFlowGraph.Instruction.Instruction;
import BackEnd.ControlFlowGraph.Instruction.MemoryInstruction.LoadInstruction;
import BackEnd.ControlFlowGraph.Operand.Address;
import BackEnd.ControlFlowGraph.Operand.ImmediateValue;
import BackEnd.ControlFlowGraph.Operand.VirtualRegister.VirtualRegister;
import Environment.Environment;
import FrontEnd.AST.Expression.Expression;
import FrontEnd.AST.Type.ArrayType;
import FrontEnd.AST.Type.BasicType.IntType;
import FrontEnd.AST.Type.BasicType.StringType;
import FrontEnd.AST.Type.ClassType.ClassType;
import FrontEnd.AST.Type.ClassType.Member.Member;
import FrontEnd.AST.Type.ClassType.Member.Memberfunction;
import FrontEnd.AST.Type.ClassType.Member.Membervariable;
import FrontEnd.AST.Type.Type;
import Utility.Error.CompilationError;
//import Compiler.Utility.Utility;
import java.util.List;
public class FieldExpression extends Expression{
    public Expression expression;
    public String name;
    private FieldExpression(Expression expression, String name, Type type, boolean isleftvalue){
        super(type,isleftvalue);
        this.expression = expression;
        this.name = name;
    }
    public static Expression getExpression(Expression expression,String name){
        if (expression.type instanceof ClassType){
            ClassType ct = (ClassType)(expression.type);
            Member mb = ct.getMember(name);
            if (mb instanceof Membervariable){
                return new FieldExpression(expression,name,((Membervariable)(mb)).type,expression.isleftvalue);
            }
            if (mb instanceof Memberfunction){
                return new FieldExpression(expression,name,((Memberfunction)(mb)).function,expression.isleftvalue);
            }
            throw new CompilationError("fieldexpression error");
        }else if (expression.type instanceof ArrayType) {
            if (name.equals("size")){
                return new FieldExpression(expression,name,
                        Environment.symboltable.getSymbol("_builtin_array_size").type,false);
            }
        }else if (expression.type instanceof StringType) {
            if (name.equals("length")) {
                return new FieldExpression(expression,name,
                        Environment.symboltable.getSymbol("_builtin_string_length").type,false);
            }else if (name.equals("substring")){
                return new FieldExpression(expression,name,
                        Environment.symboltable.getSymbol("_builtin_string_substring").type,false);
            }else if (name.equals("ord")){
                return new FieldExpression(expression,name,
                        Environment.symboltable.getSymbol("_builtin_string_ord").type,false);
            }else if (name.equals("parseInt")){
                return new FieldExpression(expression,name,
                        Environment.symboltable.getSymbol("_builtin_string_parseInt").type,false);
            }
        }
        throw new CompilationError("fieldexpression error");
    }
    @Override
    public void emit(List<Instruction> instructions){
        if (expression.type instanceof ClassType) {
            ClassType classType = (ClassType)expression.type;
            Member member = classType.getMember(name);
            if (member instanceof Membervariable) {
                Membervariable x = (Membervariable)member;
                expression.emit(instructions);
                expression.load(instructions);
                VirtualRegister address = (VirtualRegister)expression.operand;
                ImmediateValue delta = new ImmediateValue(x.offset);
                operand = new Address(address,delta,x.type.size());
            }
        }
    }

    @Override
    public void load(List<Instruction> instructions) {
        if (operand instanceof Address) {
            Address address = (Address)operand;
            operand = Environment.registertable.addTemporaryRegister();
            instructions.add(LoadInstruction.getInstruction(operand,address));
        }
    }
}