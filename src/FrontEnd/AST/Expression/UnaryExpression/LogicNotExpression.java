package FrontEnd.AST.Expression.UnaryExpression;
import BackEnd.ControlFlowGraph.Instruction.ArithmeticInstruction.BinaryInstruction.BitxorInstruction;
import BackEnd.ControlFlowGraph.Instruction.Instruction;
import BackEnd.ControlFlowGraph.Operand.ImmediateValue;
import Environment.Environment;
import FrontEnd.AST.Expression.ConstantExpression.Boolconstant;
import FrontEnd.AST.Expression.Expression;
import FrontEnd.AST.Type.BasicType.BoolType;
import FrontEnd.AST.Type.Type;
import Utility.Error.CompilationError;
import java.util.List;
public class LogicNotExpression extends UnaryExpression{
    private LogicNotExpression(Expression expression, Type type, boolean isleftvalue){
        super(expression,isleftvalue,type);
    }
    public static Expression getExpression(Expression expression){
        if (expression.type instanceof BoolType){
            if (expression instanceof Boolconstant){
                boolean value = ((Boolconstant)(expression)).value;
                return Boolconstant.getConstant(!value);
            }
            return new LogicNotExpression(expression,BoolType.getType(),false);
        }
        throw new CompilationError("logicnot error");
    }
    @Override
    public void emit(List<Instruction> instructions){
        expression.emit(instructions);
        expression.load(instructions);
        operand = Environment.registertable.addTemporaryRegister();
        instructions.add(BitxorInstruction.getInstruction(operand,expression.operand,new ImmediateValue(1)));
    }
}