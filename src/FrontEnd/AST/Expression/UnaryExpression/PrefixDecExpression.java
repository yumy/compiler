package FrontEnd.AST.Expression.UnaryExpression;
import BackEnd.ControlFlowGraph.Instruction.ArithmeticInstruction.BinaryInstruction.MinusInstruction;
import BackEnd.ControlFlowGraph.Instruction.Instruction;
import BackEnd.ControlFlowGraph.Instruction.MemoryInstruction.StoreInstruction;
import BackEnd.ControlFlowGraph.Operand.Address;
import BackEnd.ControlFlowGraph.Operand.ImmediateValue;
import FrontEnd.AST.Expression.Expression;
import FrontEnd.AST.Type.BasicType.IntType;
import FrontEnd.AST.Type.Type;
import Utility.Error.CompilationError;
import java.util.List;
public class PrefixDecExpression extends UnaryExpression{
    public PrefixDecExpression(Expression expression,Type type,boolean isleftvalue){
        super(expression,isleftvalue,type);
    }
    public static Expression getExpression(Expression expression){
        if (!expression.isleftvalue){
            throw new CompilationError("prefixdec error");
        }
        if (expression.type instanceof IntType){
            return new PrefixDecExpression(expression,IntType.getType(),false);
        }
        throw new CompilationError("prefix error");
    }
    @Override
    public void emit(List<Instruction> instructions){
        expression.emit(instructions);
        if (expression.operand instanceof Address){
            Address address = (Address)expression.operand;
            address = new Address(address.base,address.offset,address.size);
            expression.load(instructions);
            operand = expression.operand;
            instructions.add(MinusInstruction.getInstruction(operand,operand,new ImmediateValue(1)));
            instructions.add(StoreInstruction.getInstruction(operand,address));
        }else{
            expression.load(instructions);
            operand = expression.operand;
            instructions.add(MinusInstruction.getInstruction(operand,operand,new ImmediateValue(1)));
        }
    }
}