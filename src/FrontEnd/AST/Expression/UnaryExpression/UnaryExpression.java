package FrontEnd.AST.Expression.UnaryExpression;
import FrontEnd.AST.Expression.Expression;
import FrontEnd.AST.Type.Type;
//import Compiler.Utility.Utility;
public abstract class UnaryExpression extends Expression{
    public Expression expression;
    public UnaryExpression(Expression expression,boolean isleftvalue,Type type){
        super(type,isleftvalue);
        this.expression = expression;
    }
}