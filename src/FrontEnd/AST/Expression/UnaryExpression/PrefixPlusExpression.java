package FrontEnd.AST.Expression.UnaryExpression;
import BackEnd.ControlFlowGraph.Instruction.Instruction;
import FrontEnd.AST.Expression.ConstantExpression.IntConstant;
import FrontEnd.AST.Expression.Expression;
import FrontEnd.AST.Type.BasicType.IntType;
import FrontEnd.AST.Type.Type;
import Utility.Error.CompilationError;
import java.util.List;
public class PrefixPlusExpression extends UnaryExpression{
    public PrefixPlusExpression(Expression expression,boolean isleftvalue,Type type){
        super(expression,isleftvalue,type);
    }
    public static Expression getExpression(Expression expression){
        if (expression.type instanceof IntType){
            if (expression instanceof IntConstant){
                int value = ((IntConstant)(expression)).value;
                return IntConstant.getConstant(+value);
            }
            return new PrefixPlusExpression(expression,false,IntType.getType());
        }
        throw new CompilationError("prefixplus error");
    }
    @Override
    public void emit(List<Instruction> instructions){
        operand = expression.operand;
    }
}