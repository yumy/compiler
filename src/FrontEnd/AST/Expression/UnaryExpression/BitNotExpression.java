package FrontEnd.AST.Expression.UnaryExpression;
import BackEnd.ControlFlowGraph.Instruction.ArithmeticInstruction.UnaryInstruction.BitnotInstruction;
import BackEnd.ControlFlowGraph.Instruction.Instruction;
import Environment.Environment;
import FrontEnd.AST.Expression.ConstantExpression.IntConstant;
import FrontEnd.AST.Expression.Expression;
import FrontEnd.AST.Type.BasicType.IntType;
import FrontEnd.AST.Type.Type;
import Utility.Error.CompilationError;
import java.util.List;
public class BitNotExpression extends UnaryExpression{
    BitNotExpression(Expression expression,boolean isleftvalue,Type type){
        super(expression,isleftvalue,type);
    }
    public static Expression getExpression(Expression expression){
        if (expression.type instanceof IntType){
            if (expression instanceof IntConstant){
                int value = ((IntConstant)(expression)).value;
                return IntConstant.getConstant(~value);
            }
            return new BitNotExpression(expression,false,IntType.getType());
        }
        throw new CompilationError("bitnotexpression error");
    }
    @Override
    public void emit(List<Instruction> instructions){
        expression.emit(instructions);
        expression.load(instructions);
        operand = Environment.registertable.addTemporaryRegister();
        instructions.add(BitnotInstruction.getInstruction(operand,expression.operand));
    }
}