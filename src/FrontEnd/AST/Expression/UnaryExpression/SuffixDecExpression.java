package FrontEnd.AST.Expression.UnaryExpression;
import BackEnd.ControlFlowGraph.Instruction.ArithmeticInstruction.BinaryInstruction.MinusInstruction;
import BackEnd.ControlFlowGraph.Instruction.Instruction;
import BackEnd.ControlFlowGraph.Instruction.MemoryInstruction.MoveInstruction;
import BackEnd.ControlFlowGraph.Instruction.MemoryInstruction.StoreInstruction;
import BackEnd.ControlFlowGraph.Operand.Address;
import BackEnd.ControlFlowGraph.Operand.ImmediateValue;
import Environment.Environment;
import FrontEnd.AST.Expression.Expression;
import FrontEnd.AST.Type.BasicType.IntType;
import FrontEnd.AST.Type.Type;
import Utility.Error.CompilationError;
import java.util.List;
public class SuffixDecExpression extends UnaryExpression{
    public SuffixDecExpression(Expression expression,Type type,boolean isleftvalue){
        super(expression,isleftvalue,type);
    }
    public static Expression getExpression(Expression expression){
        if (!expression.isleftvalue){
            throw new CompilationError("suffixdec error");
        }
        if (expression.type instanceof IntType){
            return new SuffixDecExpression(expression,IntType.getType(),false);
        }
        throw new CompilationError("suffixdec error");
    }
    @Override
    public void emit(List<Instruction> instructions){
        expression.emit(instructions);
        operand = Environment.registertable.addTemporaryRegister();
        if (expression.operand instanceof Address){
            Address address = (Address)expression.operand;
            address = new Address(address.base,address.offset,address.size);
            expression.load(instructions);
            instructions.add(MoveInstruction.getInstruction(operand,expression.operand));
            instructions.add(MinusInstruction.getInstruction(expression.operand,expression.operand,new ImmediateValue(1)));
            instructions.add(StoreInstruction.getInstruction(expression.operand,address));
        }else{
            expression.load(instructions);
            instructions.add(MoveInstruction.getInstruction(operand,expression.operand));
            instructions.add(MinusInstruction.getInstruction(expression.operand,expression.operand,new ImmediateValue(1)));
        }
    }
}