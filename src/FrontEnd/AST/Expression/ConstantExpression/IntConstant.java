package FrontEnd.AST.Expression.ConstantExpression;
import BackEnd.ControlFlowGraph.Instruction.Instruction;
import BackEnd.ControlFlowGraph.Operand.ImmediateValue;
import FrontEnd.AST.Type.BasicType.IntType;
import java.util.List;
public class IntConstant extends Constant{
    public int value;
    IntConstant(int value){
        super(IntType.getType());
        this.value = value;
    }
    public static Constant getConstant(int value){
        return new IntConstant(value);
    }
    @Override
    public void emit(List<Instruction> instructions){
        operand = new ImmediateValue(value);
    }
}