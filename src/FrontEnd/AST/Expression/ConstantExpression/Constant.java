package FrontEnd.AST.Expression.ConstantExpression;
import FrontEnd.AST.Expression.Expression;
import FrontEnd.AST.Type.Type;
//import Compiler.Utility.Utility;
public abstract class Constant extends Expression{
    Constant(Type type){
        super(type,false);
    }
}