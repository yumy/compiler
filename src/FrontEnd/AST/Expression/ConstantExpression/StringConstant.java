package FrontEnd.AST.Expression.ConstantExpression;
import BackEnd.ControlFlowGraph.Instruction.Instruction;
import Environment.Environment;
import FrontEnd.AST.Type.BasicType.StringType;
import java.util.List;
public class StringConstant extends Constant{
    public String value;
    StringConstant(String value){
        super(StringType.getType());
        this.value = value;
    }
    public static Constant getConstant(String value){
        return new StringConstant(value);
    }
    @Override
    public void emit(List<Instruction> instructions){
        operand = Environment.registertable.addStringRegister(value);
    }
}