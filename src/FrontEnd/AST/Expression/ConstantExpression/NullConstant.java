package FrontEnd.AST.Expression.ConstantExpression;
import BackEnd.ControlFlowGraph.Instruction.Instruction;
import BackEnd.ControlFlowGraph.Operand.ImmediateValue;
import FrontEnd.AST.Type.BasicType.NullType;
import java.util.List;
public class NullConstant extends Constant{
    NullConstant(){
        super(NullType.getType());
    }
    public static Constant getConstant(){
        return new NullConstant();
    }
    @Override
    public void emit(List<Instruction> instructions){
        operand = new ImmediateValue(0);
    }
}