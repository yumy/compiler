package FrontEnd.AST.Expression.ConstantExpression;
import BackEnd.ControlFlowGraph.Instruction.Instruction;
import BackEnd.ControlFlowGraph.Operand.ImmediateValue;
import FrontEnd.AST.Type.BasicType.BoolType;
import java.util.List;
public class Boolconstant extends Constant{
    public boolean value;
    Boolconstant(boolean value){
        super(BoolType.getType());
        this.value = value;
    }
    public static Constant getConstant(boolean value){
        return new Boolconstant(value);
    }
    @Override
    public void emit(List<Instruction> instructions){
        operand = new ImmediateValue(value ? 1 : 0);
    }
}