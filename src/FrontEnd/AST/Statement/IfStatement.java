package FrontEnd.AST.Statement;
import BackEnd.ControlFlowGraph.Instruction.ControlFlowInstruction.BranchInstruction;
import BackEnd.ControlFlowGraph.Instruction.ControlFlowInstruction.JumpInstruction;
import BackEnd.ControlFlowGraph.Instruction.Instruction;
import BackEnd.ControlFlowGraph.Instruction.LabelInstruction;
import Environment.ScopeTable.Scope;
import FrontEnd.AST.Expression.ConstantExpression.Boolconstant;
import FrontEnd.AST.Expression.Expression;
import FrontEnd.AST.Type.BasicType.BoolType;
import Utility.Error.CompilationError;
//import Compiler.Utility.Utility;
import java.util.List;
public class IfStatement extends Statement implements Scope{
    public Expression cond;
    public Statement truestatement,falsestatement;
    public IfStatement(Expression cond,Statement truestatement,Statement falsestatement){
        this.cond = cond;
        this.truestatement = truestatement;
        this.falsestatement = falsestatement;
    }
    public static Statement getStatement(Expression cond,Statement truestatement,Statement falsestatement){
        if (!(cond.type instanceof BoolType)){
            throw new CompilationError("CE");
        }
        if (cond instanceof Boolconstant){
            boolean value = ((Boolconstant)(cond)).value;
            if (value) return truestatement; else return falsestatement;
        }
        return new IfStatement(cond,truestatement,falsestatement);
    }
    @Override
    public void emit(List<Instruction> instructions) {
        LabelInstruction trueLabel = LabelInstruction.getInstruction("if_true");
        LabelInstruction falseLabel = LabelInstruction.getInstruction("if_false");
        LabelInstruction mergeLabel = LabelInstruction.getInstruction("if_merge");
		/*
			%...:
				****condition****
				branch $condition, %if_true, %if_false
			%if_true:
				****trueStatement****
				jump %if_merge
			%if_false:
				****falseStatement****
				jump %if_merge
			%if_merge:
				...
		 */
        cond.emit(instructions);
        cond.load(instructions);
        instructions.add(BranchInstruction.getInstruction(cond.operand,trueLabel,falseLabel));
        instructions.add(trueLabel);
        if (truestatement != null) {
            truestatement.emit(instructions);
        }
        instructions.add(JumpInstruction.getInstruction(mergeLabel));
        instructions.add(falseLabel);
        if (falsestatement != null) {
            falsestatement.emit(instructions);
        }
        instructions.add(JumpInstruction.getInstruction(mergeLabel));
        instructions.add(mergeLabel);
    }
}