package FrontEnd.AST.Statement;
import BackEnd.ControlFlowGraph.Instruction.Instruction;
import FrontEnd.AST.Expression.Expression;
import java.util.List;
public class ExpressionStatement extends Statement{
    public Expression expression;
    private ExpressionStatement(Expression expression){
        this.expression = expression;
    }
    public static Statement getStatement(Expression expression){
        return new ExpressionStatement(expression);
    }
    @Override
    public void emit(List<Instruction> instructions){
        if (expression != null) {
            expression.emit(instructions);
        }
    }
}