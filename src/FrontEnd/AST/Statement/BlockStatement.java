package FrontEnd.AST.Statement;
import BackEnd.ControlFlowGraph.Instruction.Instruction;
import Environment.ScopeTable.Scope;
//import Compiler.Utility.Utility;
import java.util.ArrayList;
import java.util.List;
public class BlockStatement extends Statement implements Scope{
    public ArrayList<Statement> stmts;
    private BlockStatement(){
        stmts = new ArrayList<>();
    }
    public static Statement getStatement(){
        return new BlockStatement();
    }
    public void addStatement(Statement statement){
        stmts.add(statement);
    }
    @Override
    public void emit(List<Instruction> instructions){
        stmts.forEach(statement -> statement.emit(instructions));
    }
}