package FrontEnd.AST.Statement;
import BackEnd.ControlFlowGraph.Instruction.Instruction;
import BackEnd.ControlFlowGraph.Instruction.MemoryInstruction.MoveInstruction;
import Environment.SymbolTable.Symbol;
import FrontEnd.AST.Expression.Expression;
import FrontEnd.AST.Type.BasicType.VoidType;
import Utility.Error.CompilationError;
//import Compiler.Utility.Utility;
import java.util.List;
public class VariableDeclarationStatement extends Statement{
    public Symbol symbol;
    public Expression expression;
    private VariableDeclarationStatement(Symbol symbol,Expression expression){
        this.symbol = symbol;
        this.expression = expression;
    }
    public static Statement getStatement(Symbol symbol,Expression expression){
        if (symbol.type instanceof VoidType) {
            throw new CompilationError("CE");
        }
        if (expression == null || symbol.type.compatiblewith(expression.type)){
            return new VariableDeclarationStatement(symbol,expression);
        }
        throw new CompilationError("CE");
    }
    @Override
    public void emit(List<Instruction> instructions){
        if (expression != null) {
            expression.emit(instructions);
            expression.load(instructions);
            instructions.add(MoveInstruction.getInstruction(symbol.register,expression.operand));
        }
    }
}