package FrontEnd.AST.Statement.JumpStatement;
import BackEnd.ControlFlowGraph.Instruction.ControlFlowInstruction.JumpInstruction;
import BackEnd.ControlFlowGraph.Instruction.FunctionInstruction.ReturnInstruction;
import BackEnd.ControlFlowGraph.Instruction.Instruction;
import Environment.Environment;
import FrontEnd.AST.Expression.Expression;
import FrontEnd.AST.Function;
import FrontEnd.AST.Type.BasicType.VoidType;
import FrontEnd.AST.Statement.Statement;
import Utility.Error.CompilationError;
//import Compiler.Utility.Utility;
import java.util.List;
public class ReturnStatement extends Statement{
    public Function to;
    public Expression expression;
    private ReturnStatement(Expression expression,Function to){
        this.expression = expression;
        this.to = to;
    }
    public static Statement getStatement(Expression expression){
        Function function = Environment.scopetable.getFunctionScope();
        if (function == null) {
            throw new CompilationError("CE");
        }
        if (expression == null) {
            if (function.type instanceof VoidType) {
                return new ReturnStatement(expression, function);
            }
        } else {
            if (expression.type.compatiblewith(function.type)) {
                return new ReturnStatement(expression, function);
            }
        }
        throw new CompilationError("CE");
    }
    @Override
    public void emit(List<Instruction> instructions){
        if (expression != null) {
            expression.emit(instructions);
            expression.load(instructions);
            instructions.add(ReturnInstruction.getInstruction(expression.operand));
        }
        instructions.add(JumpInstruction.getInstruction(to.exit));
    }
}