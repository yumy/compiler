package FrontEnd.AST.Statement.JumpStatement;
import BackEnd.ControlFlowGraph.Instruction.ControlFlowInstruction.JumpInstruction;
import BackEnd.ControlFlowGraph.Instruction.Instruction;
import Environment.Environment;
import FrontEnd.AST.Statement.Statement;
import FrontEnd.AST.Statement.LoopStatement.LoopStatement;
import Utility.Error.CompilationError;
//import Compiler.Utility.Utility;
import java.util.List;
public class BreakStatement extends Statement{
    public LoopStatement to;
    private BreakStatement(LoopStatement to){
        this.to = to;
    }
    public static Statement getStatement(){
        if (Environment.scopetable.getLoopScope() == null){
            throw new CompilationError("CE");
        }
        return new BreakStatement(Environment.scopetable.getLoopScope());
    }
    @Override
    public void emit(List<Instruction> instructions) {
        instructions.add(JumpInstruction.getInstruction(to.merge));
    }
}

