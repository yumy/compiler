package FrontEnd.AST.Statement;
import BackEnd.ControlFlowGraph.Instruction.Instruction;
import FrontEnd.AST.Node;
import java.util.List;
public abstract class Statement implements Node{
    public abstract void emit(List<Instruction> instructions);
}