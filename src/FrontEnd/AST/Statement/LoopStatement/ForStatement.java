package FrontEnd.AST.Statement.LoopStatement;
import BackEnd.ControlFlowGraph.Instruction.ControlFlowInstruction.BranchInstruction;
import BackEnd.ControlFlowGraph.Instruction.ControlFlowInstruction.JumpInstruction;
import BackEnd.ControlFlowGraph.Instruction.Instruction;
import BackEnd.ControlFlowGraph.Instruction.LabelInstruction;
import FrontEnd.AST.Expression.ConstantExpression.Boolconstant;
import FrontEnd.AST.Expression.Expression;
import FrontEnd.AST.Statement.Statement;
import FrontEnd.AST.Type.BasicType.BoolType;
import Utility.Error.CompilationError;
//import Compiler.Utility.Utility;
import java.util.List;
public class ForStatement extends LoopStatement{
    public Expression initial,cond,inc;
    public Statement statement;
    public static Statement getStatement(){
        return new ForStatement();
    }
    public void addinitial(Expression initial){
        this.initial = initial;
    }
    public void addinc(Expression inc){
        this.inc = inc;
    }
    public void addstatement(Statement statement){
        this.statement = statement;
    }
    public void addcond(Expression cond){
        if (cond == null){
            this.cond = Boolconstant.getConstant(true);
            return;
        }
        if (!(cond.type instanceof BoolType)){
            throw new CompilationError("CE");
        }
        this.cond = cond;
    }
    @Override
    public void emit(List<Instruction> instructions) {
        LabelInstruction conditionLabel = LabelInstruction.getInstruction("for_condition");
        LabelInstruction bodyLabel = LabelInstruction.getInstruction("for_body");
        loop = LabelInstruction.getInstruction("for_loop");
        merge = LabelInstruction.getInstruction("for_merge");
		/*
			%...:
				****initialization****
				jump %condition
			%for_condition:
				****condition****
				branch $condition, %for_body, %for_merge
			%for_body:
				****statement****
				jump %for_loop
			%for_loop:
				****increment****
				jump %for_condition
			%for_merge:
				...
		 */
        if (initial != null) {
            initial.emit(instructions);
        }
        instructions.add(JumpInstruction.getInstruction(conditionLabel));
        instructions.add(conditionLabel);
        if (cond == null) {
            addcond(null);
        }
        cond.emit(instructions);
        instructions.add(BranchInstruction.getInstruction(cond.operand,bodyLabel,merge));
        instructions.add(bodyLabel);
        if (statement != null) {
            statement.emit(instructions);
        }
        instructions.add(JumpInstruction.getInstruction(loop));
        instructions.add(loop);
        if (inc != null) {
            inc.emit(instructions);
        }
        instructions.add(JumpInstruction.getInstruction(conditionLabel));
        instructions.add(merge);
    }
}