package FrontEnd.AST.Statement.LoopStatement;
import BackEnd.ControlFlowGraph.Instruction.LabelInstruction;
import Environment.ScopeTable.Scope;
import FrontEnd.AST.Statement.Statement;
public abstract class LoopStatement extends Statement implements Scope{
    public LabelInstruction loop, merge;
}