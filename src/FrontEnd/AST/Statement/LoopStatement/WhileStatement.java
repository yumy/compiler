package FrontEnd.AST.Statement.LoopStatement;
import BackEnd.ControlFlowGraph.Instruction.ControlFlowInstruction.BranchInstruction;
import BackEnd.ControlFlowGraph.Instruction.ControlFlowInstruction.JumpInstruction;
import BackEnd.ControlFlowGraph.Instruction.Instruction;
import BackEnd.ControlFlowGraph.Instruction.LabelInstruction;
import FrontEnd.AST.Expression.Expression;
import FrontEnd.AST.Statement.Statement;
import FrontEnd.AST.Type.BasicType.BoolType;
import Utility.Error.CompilationError;
import java.util.List;
//import Compiler.Utility.Utility;

public class WhileStatement extends LoopStatement{
    public Expression cond;
    public Statement statement;
    public static Statement getStatement(){
        return new WhileStatement();
    }
    public void addcond(Expression cond){
        if (!(cond.type instanceof BoolType)){
            throw new CompilationError("CE");
        }
        this.cond = cond;
    }
    public void addstatement(Statement statement){
        this.statement = statement;
    }
    @Override
    public void emit(List<Instruction> instructions) {
        LabelInstruction bodyLabel = LabelInstruction.getInstruction("while_body");
        loop = LabelInstruction.getInstruction("while_loop");
        merge = LabelInstruction.getInstruction("while_merge");
		/*
			%...:
				jump %while_loop
			%while_loop:
				****condition****
				branch $condition, %while_body, %while_merge
			%while_body:
				****statement****
				jump %while_loop
			%while_merge:
				...
		 */
        instructions.add(JumpInstruction.getInstruction(loop));
        instructions.add(loop);
        cond.emit(instructions);
        instructions.add(BranchInstruction.getInstruction(cond.operand,bodyLabel,merge));
        instructions.add(bodyLabel);
        statement.emit(instructions);
        instructions.add(JumpInstruction.getInstruction(loop));
        instructions.add(merge);
    }
}