package Utility;

import java.util.HashSet;
import java.util.Set;

public class Utility{
    public static String getIndent(int indents){
        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 0;i < indents;++i){
            stringBuilder.append("\t");
        }
        return stringBuilder.toString();
    }
}