default rel
global func
global main
extern printf, malloc, strcpy, scanf, strlen, sscanf, sprintf, memcpy, strcmp, puts
SECTION .text
func:
	push	rbp
	mov	rbp, rsp
	push	rbx
	sub	rsp, 176
func_0_enter:
func_1_entry:
	mov	r12, qword [rsp + 152]
	mov	r13, qword [rsp + 160]
	push	rax
	push	rcx
	push	rdx
	mov	rax, r12
	mov	rcx, r13
	add	rax, rcx
	mov	rbx, rax
	pop	rdx
	pop	rcx
	pop	rax
	mov	r13, qword [rsp + 168]
	push	rax
	push	rcx
	push	rdx
	mov	rax, rbx
	mov	rcx, r13
	add	rax, rcx
	mov	rbx, rax
	pop	rdx
	pop	rcx
	pop	rax
	push	rax
	push	rcx
	push	rdx
	mov	rax, rbx
	mov	rcx, 1073741823
	and	rax, rcx
	mov	rbx, rax
	pop	rdx
	pop	rcx
	pop	rax
	mov	rax, rbx
func_2_exit:
	add	rsp, 176
	pop	rbx
	pop	rbp
	ret
main:
	push	rbp
	mov	rbp, rsp
	push	r14
	push	r15
	push	rbx
	sub	rsp, 1296
main_0_enter:
main_1_entry:
	push	rsi
	push	r11
	push	rdi
	push	r9
	push	r8
	push	r10
	add	rsp, 48
	sub	rsp, 48
	sub	rsp, 8
	call _builtin_getInt
	add	rsp, 8
	pop	r10
	pop	r8
	pop	r9
	pop	rdi
	pop	r11
	pop	rsi
	mov	r9, rax
	mov	rsi, r9
	push	rax
	push	rcx
	push	rdx
	mov	rax, rsi
	mov	rcx, 1
	add	rax, rcx
	mov	rsi, rax
	pop	rdx
	pop	rcx
	pop	rax
	push	rax
	push	rcx
	push	rdx
	mov	rax, rsi
	mov	rcx, 8
	imul	rax, rcx
	mov	rsi, rax
	pop	rdx
	pop	rcx
	pop	rax
	push	rsi
	push	r11
	push	rdi
	push	r9
	push	r8
	push	r10
	add	rsp, 48
	mov	rdi, rsi
	sub	rsp, 48
	sub	rsp, 8
	call	malloc
	add	rsp, 8
	pop	r10
	pop	r8
	pop	r9
	pop	rdi
	pop	r11
	pop	rsi
	mov	rdi, rax
	mov	qword [rdi + 0], r9
	push	rax
	push	rcx
	push	rdx
	mov	rax, rdi
	mov	rcx, 8
	add	rax, rcx
	mov	rdi, rax
	pop	rdx
	pop	rcx
	pop	rax
	mov	rsi, r9
main_2_while_loop:
	push	rax
	push	rcx
	push	rdx
	mov	rax, rsi
	mov	rcx, 1
	sub	rax, rcx
	mov	rsi, rax
	pop	rdx
	pop	rcx
	pop	rax
	mov	r8, rsi
	push	rax
	push	rcx
	push	rdx
	mov	rax, rsi
	mov	rcx, 8
	imul	rax, rcx
	mov	rsi, rax
	pop	rdx
	pop	rcx
	pop	rax
	push	rax
	push	rcx
	push	rdx
	mov	rax, rsi
	mov	rcx, rdi
	add	rax, rcx
	mov	rsi, rax
	pop	rdx
	pop	rcx
	pop	rax
	mov	rbx, r9
	push	rax
	push	rcx
	push	rdx
	mov	rax, rbx
	mov	rcx, 1
	add	rax, rcx
	mov	rbx, rax
	pop	rdx
	pop	rcx
	pop	rax
	push	rax
	push	rcx
	push	rdx
	mov	rax, rbx
	mov	rcx, 8
	imul	rax, rcx
	mov	rbx, rax
	pop	rdx
	pop	rcx
	pop	rax
	push	rsi
	push	r11
	push	rdi
	push	r9
	push	r8
	push	r10
	add	rsp, 48
	mov	rdi, rbx
	sub	rsp, 48
	sub	rsp, 8
	call	malloc
	add	rsp, 8
	pop	r10
	pop	r8
	pop	r9
	pop	rdi
	pop	r11
	pop	rsi
	mov	rbx, rax
	mov	qword [rbx + 0], r9
	push	rax
	push	rcx
	push	rdx
	mov	rax, rbx
	mov	rcx, 8
	add	rax, rcx
	mov	rbx, rax
	pop	rdx
	pop	rcx
	pop	rax
	mov	qword [rsi + 0], rbx
	push	rax
	push	rcx
	push	rdx
	mov	rax, r8
	mov	rcx, 0
	cmp	rax, rcx
	setne	al
	movzx	rax, al
	mov	rbx, rax
	pop	rdx
	pop	rcx
	pop	rax
	mov	rsi, r8
	cmp	rbx, 0
	je	main_3_while_merge
	jmp	main_2_while_loop
main_3_while_merge:
	mov	rbx, rdi
	mov	rsi, r9
	push	rax
	push	rcx
	push	rdx
	mov	rax, rsi
	mov	rcx, 1
	add	rax, rcx
	mov	rsi, rax
	pop	rdx
	pop	rcx
	pop	rax
	push	rax
	push	rcx
	push	rdx
	mov	rax, rsi
	mov	rcx, 8
	imul	rax, rcx
	mov	rsi, rax
	pop	rdx
	pop	rcx
	pop	rax
	push	rsi
	push	r11
	push	rdi
	push	r9
	push	r8
	push	r10
	add	rsp, 48
	mov	rdi, rsi
	sub	rsp, 48
	sub	rsp, 8
	call	malloc
	add	rsp, 8
	pop	r10
	pop	r8
	pop	r9
	pop	rdi
	pop	r11
	pop	rsi
	mov	r11, rax
	mov	qword [r11 + 0], r9
	push	rax
	push	rcx
	push	rdx
	mov	rax, r11
	mov	rcx, 8
	add	rax, rcx
	mov	r11, rax
	pop	rdx
	pop	rcx
	pop	rax
	mov	rsi, r9
main_4_while_loop:
	push	rax
	push	rcx
	push	rdx
	mov	rax, rsi
	mov	rcx, 1
	sub	rax, rcx
	mov	rsi, rax
	pop	rdx
	pop	rcx
	pop	rax
	mov	r8, rsi
	push	rax
	push	rcx
	push	rdx
	mov	rax, rsi
	mov	rcx, 8
	imul	rax, rcx
	mov	rsi, rax
	pop	rdx
	pop	rcx
	pop	rax
	push	rax
	push	rcx
	push	rdx
	mov	rax, rsi
	mov	rcx, r11
	add	rax, rcx
	mov	rsi, rax
	pop	rdx
	pop	rcx
	pop	rax
	mov	rdi, r9
	push	rax
	push	rcx
	push	rdx
	mov	rax, rdi
	mov	rcx, 1
	add	rax, rcx
	mov	rdi, rax
	pop	rdx
	pop	rcx
	pop	rax
	push	rax
	push	rcx
	push	rdx
	mov	rax, rdi
	mov	rcx, 8
	imul	rax, rcx
	mov	rdi, rax
	pop	rdx
	pop	rcx
	pop	rax
	push	rsi
	push	r11
	push	rdi
	push	r9
	push	r8
	push	r10
	add	rsp, 48
	sub	rsp, 48
	sub	rsp, 8
	call	malloc
	add	rsp, 8
	pop	r10
	pop	r8
	pop	r9
	pop	rdi
	pop	r11
	pop	rsi
	mov	rdi, rax
	mov	qword [rdi + 0], r9
	push	rax
	push	rcx
	push	rdx
	mov	rax, rdi
	mov	rcx, 8
	add	rax, rcx
	mov	rdi, rax
	pop	rdx
	pop	rcx
	pop	rax
	mov	qword [rsi + 0], rdi
	push	rax
	push	rcx
	push	rdx
	mov	rax, r8
	mov	rcx, 0
	cmp	rax, rcx
	setne	al
	movzx	rax, al
	mov	rdi, rax
	pop	rdx
	pop	rcx
	pop	rax
	mov	rsi, r8
	cmp	rdi, 0
	je	main_5_while_merge
	jmp	main_4_while_loop
main_5_while_merge:
	mov	r10, r9
	push	rax
	push	rcx
	push	rdx
	mov	rax, r10
	mov	rcx, 1
	add	rax, rcx
	mov	r10, rax
	pop	rdx
	pop	rcx
	pop	rax
	push	rax
	push	rcx
	push	rdx
	mov	rax, r10
	mov	rcx, 8
	imul	rax, rcx
	mov	r10, rax
	pop	rdx
	pop	rcx
	pop	rax
	push	rsi
	push	r11
	push	rdi
	push	r9
	push	r8
	push	r10
	add	rsp, 48
	mov	rdi, r10
	sub	rsp, 48
	sub	rsp, 8
	call	malloc
	add	rsp, 8
	pop	r10
	pop	r8
	pop	r9
	pop	rdi
	pop	r11
	pop	rsi
	mov	r8, rax
	mov	qword [r8 + 0], r9
	push	rax
	push	rcx
	push	rdx
	mov	rax, r8
	mov	rcx, 8
	add	rax, rcx
	mov	r8, rax
	pop	rdx
	pop	rcx
	pop	rax
	mov	r10, r9
main_6_while_loop:
	push	rax
	push	rcx
	push	rdx
	mov	rax, r10
	mov	rcx, 1
	sub	rax, rcx
	mov	r10, rax
	pop	rdx
	pop	rcx
	pop	rax
	mov	rdi, r10
	push	rax
	push	rcx
	push	rdx
	mov	rax, r10
	mov	rcx, 8
	imul	rax, rcx
	mov	r10, rax
	pop	rdx
	pop	rcx
	pop	rax
	push	rax
	push	rcx
	push	rdx
	mov	rax, r10
	mov	rcx, r8
	add	rax, rcx
	mov	r10, rax
	pop	rdx
	pop	rcx
	pop	rax
	mov	rsi, r9
	push	rax
	push	rcx
	push	rdx
	mov	rax, rsi
	mov	rcx, 1
	add	rax, rcx
	mov	rsi, rax
	pop	rdx
	pop	rcx
	pop	rax
	push	rax
	push	rcx
	push	rdx
	mov	rax, rsi
	mov	rcx, 8
	imul	rax, rcx
	mov	rsi, rax
	pop	rdx
	pop	rcx
	pop	rax
	push	rsi
	push	r11
	push	rdi
	push	r9
	push	r8
	push	r10
	add	rsp, 48
	mov	rdi, rsi
	sub	rsp, 48
	sub	rsp, 8
	call	malloc
	add	rsp, 8
	pop	r10
	pop	r8
	pop	r9
	pop	rdi
	pop	r11
	pop	rsi
	mov	rsi, rax
	mov	qword [rsi + 0], r9
	push	rax
	push	rcx
	push	rdx
	mov	rax, rsi
	mov	rcx, 8
	add	rax, rcx
	mov	rsi, rax
	pop	rdx
	pop	rcx
	pop	rax
	mov	qword [r10 + 0], rsi
	push	rax
	push	rcx
	push	rdx
	mov	rax, rdi
	mov	rcx, 0
	cmp	rax, rcx
	setne	al
	movzx	rax, al
	mov	rsi, rax
	pop	rdx
	pop	rcx
	pop	rax
	mov	r10, rdi
	cmp	rsi, 0
	je	main_7_while_merge
	jmp	main_6_while_loop
main_7_while_merge:
	mov	qword [rsp + 184], r8
	mov	r15, 0
main_8_for_condition:
	cmp	r15, r9
	jge	main_15_for_merge
main_9_for_body:
	mov	r12, 0
	mov	qword [rsp + 360], r12
main_10_for_condition:
	mov	r12, qword [rsp + 360]
	cmp	r12, r9
	jge	main_13_for_merge
main_11_for_body:
	push	rax
	push	rcx
	push	rdx
	mov	rax, r15
	mov	rcx, 8
	imul	rax, rcx
	mov	rsi, rax
	pop	rdx
	pop	rcx
	pop	rax
	push	rax
	push	rcx
	push	rdx
	mov	rax, rbx
	mov	rcx, rsi
	add	rax, rcx
	mov	rsi, rax
	pop	rdx
	pop	rcx
	pop	rax
	mov	rsi, qword [rsi + 0]
	mov	r12, qword [rsp + 360]
	push	rax
	push	rcx
	push	rdx
	mov	rax, r12
	mov	rcx, 8
	imul	rax, rcx
	mov	rdi, rax
	pop	rdx
	pop	rcx
	pop	rax
	push	rax
	push	rcx
	push	rdx
	mov	rax, rsi
	mov	rcx, rdi
	add	rax, rcx
	mov	rsi, rax
	pop	rdx
	pop	rcx
	pop	rax
	mov	r13, qword [rsp + 360]
	push	rax
	push	rcx
	push	rdx
	mov	rax, r15
	mov	rcx, r13
	add	rax, rcx
	mov	rdi, rax
	pop	rdx
	pop	rcx
	pop	rax
	mov	qword [rsi + 0], rdi
main_12_for_loop:
	mov	r12, qword [rsp + 360]
	push	rax
	push	rcx
	push	rdx
	mov	rax, r12
	mov	rcx, 1
	add	rax, rcx
	mov	r13, rax
	pop	rdx
	pop	rcx
	pop	rax
	mov	qword [rsp + 360], r13
	jmp	main_10_for_condition
main_13_for_merge:
main_14_for_loop:
	push	rax
	push	rcx
	push	rdx
	mov	rax, r15
	mov	rcx, 1
	add	rax, rcx
	mov	r15, rax
	pop	rdx
	pop	rcx
	pop	rax
	jmp	main_8_for_condition
main_15_for_merge:
	mov	r15, 0
main_16_for_condition:
	cmp	r15, r9
	jge	main_30_for_merge
main_17_for_body:
	mov	r12, 0
	mov	qword [rsp + 360], r12
main_18_for_condition:
	mov	r12, qword [rsp + 360]
	cmp	r12, r9
	jge	main_28_for_merge
main_19_for_body:
	mov	rsi, 0
main_20_for_condition:
	cmp	rsi, r9
	jge	main_26_for_merge
main_21_for_body:
	mov	r12, qword [rsp + 360]
	cmp	r12, r15
	jl	main_23_if_false
main_22_if_true:
	push	rax
	push	rcx
	push	rdx
	mov	rax, r15
	mov	rcx, 8
	imul	rax, rcx
	mov	rdi, rax
	pop	rdx
	pop	rcx
	pop	rax
	push	rax
	push	rcx
	push	rdx
	mov	rax, r11
	mov	rcx, rdi
	add	rax, rcx
	mov	rdi, rax
	pop	rdx
	pop	rcx
	pop	rax
	mov	r8, qword [rdi + 0]
	mov	r12, qword [rsp + 360]
	push	rax
	push	rcx
	push	rdx
	mov	rax, r12
	mov	rcx, 8
	imul	rax, rcx
	mov	rdi, rax
	pop	rdx
	pop	rcx
	pop	rax
	push	rax
	push	rcx
	push	rdx
	mov	rax, r8
	mov	rcx, rdi
	add	rax, rcx
	mov	rdi, rax
	pop	rdx
	pop	rcx
	pop	rax
	push	rax
	push	rcx
	push	rdx
	mov	rax, r15
	mov	rcx, 8
	imul	rax, rcx
	mov	r8, rax
	pop	rdx
	pop	rcx
	pop	rax
	push	rax
	push	rcx
	push	rdx
	mov	rax, r11
	mov	rcx, r8
	add	rax, rcx
	mov	r8, rax
	pop	rdx
	pop	rcx
	pop	rax
	mov	r8, qword [r8 + 0]
	mov	r12, qword [rsp + 360]
	push	rax
	push	rcx
	push	rdx
	mov	rax, r12
	mov	rcx, 8
	imul	rax, rcx
	mov	r10, rax
	pop	rdx
	pop	rcx
	pop	rax
	push	rax
	push	rcx
	push	rdx
	mov	rax, r8
	mov	rcx, r10
	add	rax, rcx
	mov	r8, rax
	pop	rdx
	pop	rcx
	pop	rax
	mov	r8, qword [r8 + 0]
	push	rax
	push	rcx
	push	rdx
	mov	rax, r15
	mov	rcx, 8
	imul	rax, rcx
	mov	r10, rax
	pop	rdx
	pop	rcx
	pop	rax
	push	rax
	push	rcx
	push	rdx
	mov	rax, rbx
	mov	rcx, r10
	add	rax, rcx
	mov	r10, rax
	pop	rdx
	pop	rcx
	pop	rax
	mov	r14, qword [r10 + 0]
	push	rax
	push	rcx
	push	rdx
	mov	rax, rsi
	mov	rcx, 8
	imul	rax, rcx
	mov	r10, rax
	pop	rdx
	pop	rcx
	pop	rax
	push	rax
	push	rcx
	push	rdx
	mov	rax, r14
	mov	rcx, r10
	add	rax, rcx
	mov	r10, rax
	pop	rdx
	pop	rcx
	pop	rax
	mov	r14, qword [r10 + 0]
	push	rax
	push	rcx
	push	rdx
	mov	rax, rsi
	mov	rcx, 8
	imul	rax, rcx
	mov	r10, rax
	pop	rdx
	pop	rcx
	pop	rax
	push	rax
	push	rcx
	push	rdx
	mov	rax, rbx
	mov	rcx, r10
	add	rax, rcx
	mov	r10, rax
	pop	rdx
	pop	rcx
	pop	rax
	mov	r13, qword [r10 + 0]
	mov	qword [rsp + 232], r13
	mov	r12, qword [rsp + 360]
	push	rax
	push	rcx
	push	rdx
	mov	rax, r12
	mov	rcx, 8
	imul	rax, rcx
	mov	r10, rax
	pop	rdx
	pop	rcx
	pop	rax
	mov	r12, qword [rsp + 232]
	push	rax
	push	rcx
	push	rdx
	mov	rax, r12
	mov	rcx, r10
	add	rax, rcx
	mov	r10, rax
	pop	rdx
	pop	rcx
	pop	rax
	mov	r10, qword [r10 + 0]
	push	rsi
	push	r11
	push	rdi
	push	r9
	push	r8
	push	r10
	add	rsp, 48
	mov	qword [rsp - 104], r8
	mov	qword [rsp - 96], r14
	mov	qword [rsp - 88], r10
	sub	rsp, 48
	sub	rsp, 8
	call func
	add	rsp, 8
	pop	r10
	pop	r8
	pop	r9
	pop	rdi
	pop	r11
	pop	rsi
	mov	r8, rax
	mov	qword [rdi + 0], r8
	push	rax
	push	rcx
	push	rdx
	mov	rax, r15
	mov	rcx, 8
	imul	rax, rcx
	mov	rdi, rax
	pop	rdx
	pop	rcx
	pop	rax
	mov	r12, qword [rsp + 184]
	push	rax
	push	rcx
	push	rdx
	mov	rax, r12
	mov	rcx, rdi
	add	rax, rcx
	mov	rdi, rax
	pop	rdx
	pop	rcx
	pop	rax
	mov	rdi, qword [rdi + 0]
	mov	r12, qword [rsp + 360]
	push	rax
	push	rcx
	push	rdx
	mov	rax, r12
	mov	rcx, 8
	imul	rax, rcx
	mov	r8, rax
	pop	rdx
	pop	rcx
	pop	rax
	push	rax
	push	rcx
	push	rdx
	mov	rax, rdi
	mov	rcx, r8
	add	rax, rcx
	mov	r14, rax
	pop	rdx
	pop	rcx
	pop	rax
	push	rax
	push	rcx
	push	rdx
	mov	rax, r15
	mov	rcx, 8
	imul	rax, rcx
	mov	rdi, rax
	pop	rdx
	pop	rcx
	pop	rax
	push	rax
	push	rcx
	push	rdx
	mov	rax, r11
	mov	rcx, rdi
	add	rax, rcx
	mov	rdi, rax
	pop	rdx
	pop	rcx
	pop	rax
	mov	r8, qword [rdi + 0]
	mov	r12, qword [rsp + 360]
	push	rax
	push	rcx
	push	rdx
	mov	rax, r12
	mov	rcx, 8
	imul	rax, rcx
	mov	rdi, rax
	pop	rdx
	pop	rcx
	pop	rax
	push	rax
	push	rcx
	push	rdx
	mov	rax, r8
	mov	rcx, rdi
	add	rax, rcx
	mov	rdi, rax
	pop	rdx
	pop	rcx
	pop	rax
	mov	r13, qword [rdi + 0]
	mov	qword [rsp + 168], r13
	push	rax
	push	rcx
	push	rdx
	mov	rax, r15
	mov	rcx, 8
	imul	rax, rcx
	mov	rdi, rax
	pop	rdx
	pop	rcx
	pop	rax
	push	rax
	push	rcx
	push	rdx
	mov	rax, rbx
	mov	rcx, rdi
	add	rax, rcx
	mov	rdi, rax
	pop	rdx
	pop	rcx
	pop	rax
	mov	rdi, qword [rdi + 0]
	push	rax
	push	rcx
	push	rdx
	mov	rax, rsi
	mov	rcx, 8
	imul	rax, rcx
	mov	r8, rax
	pop	rdx
	pop	rcx
	pop	rax
	push	rax
	push	rcx
	push	rdx
	mov	rax, rdi
	mov	rcx, r8
	add	rax, rcx
	mov	rdi, rax
	pop	rdx
	pop	rcx
	pop	rax
	mov	r10, qword [rdi + 0]
	push	rax
	push	rcx
	push	rdx
	mov	rax, rsi
	mov	rcx, 8
	imul	rax, rcx
	mov	rdi, rax
	pop	rdx
	pop	rcx
	pop	rax
	push	rax
	push	rcx
	push	rdx
	mov	rax, rbx
	mov	rcx, rdi
	add	rax, rcx
	mov	rdi, rax
	pop	rdx
	pop	rcx
	pop	rax
	mov	r8, qword [rdi + 0]
	mov	r12, qword [rsp + 360]
	push	rax
	push	rcx
	push	rdx
	mov	rax, r12
	mov	rcx, 8
	imul	rax, rcx
	mov	rdi, rax
	pop	rdx
	pop	rcx
	pop	rax
	push	rax
	push	rcx
	push	rdx
	mov	rax, r8
	mov	rcx, rdi
	add	rax, rcx
	mov	rdi, rax
	pop	rdx
	pop	rcx
	pop	rax
	mov	rdi, qword [rdi + 0]
	push	rsi
	push	r11
	push	rdi
	push	r9
	push	r8
	push	r10
	add	rsp, 48
	mov	r12, qword [rsp + 168]
	mov	qword [rsp - 104], r12
	mov	qword [rsp - 96], r10
	mov	qword [rsp - 88], rdi
	sub	rsp, 48
	sub	rsp, 8
	call func
	add	rsp, 8
	pop	r10
	pop	r8
	pop	r9
	pop	rdi
	pop	r11
	pop	rsi
	mov	rdi, rax
	mov	qword [r14 + 0], rdi
	push	rax
	push	rcx
	push	rdx
	mov	rax, r15
	mov	rcx, 8
	imul	rax, rcx
	mov	rdi, rax
	pop	rdx
	pop	rcx
	pop	rax
	mov	r12, qword [rsp + 184]
	push	rax
	push	rcx
	push	rdx
	mov	rax, r12
	mov	rcx, rdi
	add	rax, rcx
	mov	rdi, rax
	pop	rdx
	pop	rcx
	pop	rax
	mov	r8, qword [rdi + 0]
	mov	r12, qword [rsp + 360]
	push	rax
	push	rcx
	push	rdx
	mov	rax, r12
	mov	rcx, 8
	imul	rax, rcx
	mov	rdi, rax
	pop	rdx
	pop	rcx
	pop	rax
	push	rax
	push	rcx
	push	rdx
	mov	rax, r8
	mov	rcx, rdi
	add	rax, rcx
	mov	r14, rax
	pop	rdx
	pop	rcx
	pop	rax
	push	rax
	push	rcx
	push	rdx
	mov	rax, r15
	mov	rcx, 8
	imul	rax, rcx
	mov	rdi, rax
	pop	rdx
	pop	rcx
	pop	rax
	push	rax
	push	rcx
	push	rdx
	mov	rax, r11
	mov	rcx, rdi
	add	rax, rcx
	mov	rdi, rax
	pop	rdx
	pop	rcx
	pop	rax
	mov	r8, qword [rdi + 0]
	mov	r12, qword [rsp + 360]
	push	rax
	push	rcx
	push	rdx
	mov	rax, r12
	mov	rcx, 8
	imul	rax, rcx
	mov	rdi, rax
	pop	rdx
	pop	rcx
	pop	rax
	push	rax
	push	rcx
	push	rdx
	mov	rax, r8
	mov	rcx, rdi
	add	rax, rcx
	mov	rdi, rax
	pop	rdx
	pop	rcx
	pop	rax
	mov	rdi, qword [rdi + 0]
	push	rax
	push	rcx
	push	rdx
	mov	rax, r15
	mov	rcx, 8
	imul	rax, rcx
	mov	r8, rax
	pop	rdx
	pop	rcx
	pop	rax
	push	rax
	push	rcx
	push	rdx
	mov	rax, rbx
	mov	rcx, r8
	add	rax, rcx
	mov	r8, rax
	pop	rdx
	pop	rcx
	pop	rax
	mov	r10, qword [r8 + 0]
	push	rax
	push	rcx
	push	rdx
	mov	rax, rsi
	mov	rcx, 8
	imul	rax, rcx
	mov	r8, rax
	pop	rdx
	pop	rcx
	pop	rax
	push	rax
	push	rcx
	push	rdx
	mov	rax, r10
	mov	rcx, r8
	add	rax, rcx
	mov	r8, rax
	pop	rdx
	pop	rcx
	pop	rax
	mov	r13, qword [r8 + 0]
	mov	qword [rsp + 584], r13
	push	rax
	push	rcx
	push	rdx
	mov	rax, rsi
	mov	rcx, 8
	imul	rax, rcx
	mov	r8, rax
	pop	rdx
	pop	rcx
	pop	rax
	push	rax
	push	rcx
	push	rdx
	mov	rax, rbx
	mov	rcx, r8
	add	rax, rcx
	mov	r8, rax
	pop	rdx
	pop	rcx
	pop	rax
	mov	r8, qword [r8 + 0]
	mov	r12, qword [rsp + 360]
	push	rax
	push	rcx
	push	rdx
	mov	rax, r12
	mov	rcx, 8
	imul	rax, rcx
	mov	r10, rax
	pop	rdx
	pop	rcx
	pop	rax
	push	rax
	push	rcx
	push	rdx
	mov	rax, r8
	mov	rcx, r10
	add	rax, rcx
	mov	r8, rax
	pop	rdx
	pop	rcx
	pop	rax
	mov	r8, qword [r8 + 0]
	push	rsi
	push	r11
	push	rdi
	push	r9
	push	r8
	push	r10
	add	rsp, 48
	mov	qword [rsp - 104], rdi
	mov	r12, qword [rsp + 584]
	mov	qword [rsp - 96], r12
	mov	qword [rsp - 88], r8
	sub	rsp, 48
	sub	rsp, 8
	call func
	add	rsp, 8
	pop	r10
	pop	r8
	pop	r9
	pop	rdi
	pop	r11
	pop	rsi
	mov	rdi, rax
	mov	qword [r14 + 0], rdi
	push	rax
	push	rcx
	push	rdx
	mov	rax, r15
	mov	rcx, 8
	imul	rax, rcx
	mov	rdi, rax
	pop	rdx
	pop	rcx
	pop	rax
	mov	r12, qword [rsp + 184]
	push	rax
	push	rcx
	push	rdx
	mov	rax, r12
	mov	rcx, rdi
	add	rax, rcx
	mov	rdi, rax
	pop	rdx
	pop	rcx
	pop	rax
	mov	rdi, qword [rdi + 0]
	mov	r12, qword [rsp + 360]
	push	rax
	push	rcx
	push	rdx
	mov	rax, r12
	mov	rcx, 8
	imul	rax, rcx
	mov	r8, rax
	pop	rdx
	pop	rcx
	pop	rax
	push	rax
	push	rcx
	push	rdx
	mov	rax, rdi
	mov	rcx, r8
	add	rax, rcx
	mov	r14, rax
	pop	rdx
	pop	rcx
	pop	rax
	push	rax
	push	rcx
	push	rdx
	mov	rax, r15
	mov	rcx, 8
	imul	rax, rcx
	mov	rdi, rax
	pop	rdx
	pop	rcx
	pop	rax
	push	rax
	push	rcx
	push	rdx
	mov	rax, r11
	mov	rcx, rdi
	add	rax, rcx
	mov	rdi, rax
	pop	rdx
	pop	rcx
	pop	rax
	mov	rdi, qword [rdi + 0]
	mov	r12, qword [rsp + 360]
	push	rax
	push	rcx
	push	rdx
	mov	rax, r12
	mov	rcx, 8
	imul	rax, rcx
	mov	r8, rax
	pop	rdx
	pop	rcx
	pop	rax
	push	rax
	push	rcx
	push	rdx
	mov	rax, rdi
	mov	rcx, r8
	add	rax, rcx
	mov	rdi, rax
	pop	rdx
	pop	rcx
	pop	rax
	mov	r8, qword [rdi + 0]
	push	rax
	push	rcx
	push	rdx
	mov	rax, r15
	mov	rcx, 8
	imul	rax, rcx
	mov	rdi, rax
	pop	rdx
	pop	rcx
	pop	rax
	push	rax
	push	rcx
	push	rdx
	mov	rax, rbx
	mov	rcx, rdi
	add	rax, rcx
	mov	rdi, rax
	pop	rdx
	pop	rcx
	pop	rax
	mov	rdi, qword [rdi + 0]
	push	rax
	push	rcx
	push	rdx
	mov	rax, rsi
	mov	rcx, 8
	imul	rax, rcx
	mov	r10, rax
	pop	rdx
	pop	rcx
	pop	rax
	push	rax
	push	rcx
	push	rdx
	mov	rax, rdi
	mov	rcx, r10
	add	rax, rcx
	mov	rdi, rax
	pop	rdx
	pop	rcx
	pop	rax
	mov	r10, qword [rdi + 0]
	push	rax
	push	rcx
	push	rdx
	mov	rax, rsi
	mov	rcx, 8
	imul	rax, rcx
	mov	rdi, rax
	pop	rdx
	pop	rcx
	pop	rax
	push	rax
	push	rcx
	push	rdx
	mov	rax, rbx
	mov	rcx, rdi
	add	rax, rcx
	mov	rdi, rax
	pop	rdx
	pop	rcx
	pop	rax
	mov	rdi, qword [rdi + 0]
	mov	r12, qword [rsp + 360]
	push	rax
	push	rcx
	push	rdx
	mov	rax, r12
	mov	rcx, 8
	imul	rax, rcx
	mov	r13, rax
	pop	rdx
	pop	rcx
	pop	rax
	mov	qword [rsp + 328], r13
	mov	r13, qword [rsp + 328]
	push	rax
	push	rcx
	push	rdx
	mov	rax, rdi
	mov	rcx, r13
	add	rax, rcx
	mov	rdi, rax
	pop	rdx
	pop	rcx
	pop	rax
	mov	rdi, qword [rdi + 0]
	push	rsi
	push	r11
	push	rdi
	push	r9
	push	r8
	push	r10
	add	rsp, 48
	mov	qword [rsp - 104], r8
	mov	qword [rsp - 96], r10
	mov	qword [rsp - 88], rdi
	sub	rsp, 48
	sub	rsp, 8
	call func
	add	rsp, 8
	pop	r10
	pop	r8
	pop	r9
	pop	rdi
	pop	r11
	pop	rsi
	mov	rdi, rax
	mov	qword [r14 + 0], rdi
main_24_if_merge:
main_25_for_loop:
	push	rax
	push	rcx
	push	rdx
	mov	rax, rsi
	mov	rcx, 1
	add	rax, rcx
	mov	rsi, rax
	pop	rdx
	pop	rcx
	pop	rax
	jmp	main_20_for_condition
main_23_if_false:
	jmp	main_24_if_merge
main_26_for_merge:
main_27_for_loop:
	mov	r12, qword [rsp + 360]
	push	rax
	push	rcx
	push	rdx
	mov	rax, r12
	mov	rcx, 1
	add	rax, rcx
	mov	r13, rax
	pop	rdx
	pop	rcx
	pop	rax
	mov	qword [rsp + 360], r13
	jmp	main_18_for_condition
main_28_for_merge:
main_29_for_loop:
	push	rax
	push	rcx
	push	rdx
	mov	rax, r15
	mov	rcx, 1
	add	rax, rcx
	mov	r15, rax
	pop	rdx
	pop	rcx
	pop	rax
	jmp	main_16_for_condition
main_30_for_merge:
	mov	rbx, 0
	mov	r15, 0
main_31_for_condition:
	cmp	r15, r9
	jge	main_38_for_merge
main_32_for_body:
	mov	r12, 0
	mov	qword [rsp + 360], r12
main_33_for_condition:
	mov	r12, qword [rsp + 360]
	cmp	r12, r9
	jge	main_36_for_merge
main_34_for_body:
	push	rax
	push	rcx
	push	rdx
	mov	rax, r15
	mov	rcx, 8
	imul	rax, rcx
	mov	rsi, rax
	pop	rdx
	pop	rcx
	pop	rax
	push	rax
	push	rcx
	push	rdx
	mov	rax, r11
	mov	rcx, rsi
	add	rax, rcx
	mov	rsi, rax
	pop	rdx
	pop	rcx
	pop	rax
	mov	rsi, qword [rsi + 0]
	mov	r12, qword [rsp + 360]
	push	rax
	push	rcx
	push	rdx
	mov	rax, r12
	mov	rcx, 8
	imul	rax, rcx
	mov	rdi, rax
	pop	rdx
	pop	rcx
	pop	rax
	push	rax
	push	rcx
	push	rdx
	mov	rax, rsi
	mov	rcx, rdi
	add	rax, rcx
	mov	rsi, rax
	pop	rdx
	pop	rcx
	pop	rax
	mov	rsi, qword [rsi + 0]
	push	rax
	push	rcx
	push	rdx
	mov	rax, rbx
	mov	rcx, rsi
	add	rax, rcx
	mov	rbx, rax
	pop	rdx
	pop	rcx
	pop	rax
	push	rax
	push	rcx
	push	rdx
	mov	rax, rbx
	mov	rcx, 1073741823
	and	rax, rcx
	mov	rbx, rax
	pop	rdx
	pop	rcx
	pop	rax
main_35_for_loop:
	mov	r12, qword [rsp + 360]
	push	rax
	push	rcx
	push	rdx
	mov	rax, r12
	mov	rcx, 1
	add	rax, rcx
	mov	r13, rax
	pop	rdx
	pop	rcx
	pop	rax
	mov	qword [rsp + 360], r13
	jmp	main_33_for_condition
main_36_for_merge:
main_37_for_loop:
	push	rax
	push	rcx
	push	rdx
	mov	rax, r15
	mov	rcx, 1
	add	rax, rcx
	mov	r15, rax
	pop	rdx
	pop	rcx
	pop	rax
	jmp	main_31_for_condition
main_38_for_merge:
	push	rsi
	push	r11
	push	rdi
	push	r9
	push	r8
	push	r10
	add	rsp, 48
	mov	rdi, rbx
	sub	rsp, 48
	sub	rsp, 8
	call _builtin_toString
	add	rsp, 8
	pop	r10
	pop	r8
	pop	r9
	pop	rdi
	pop	r11
	pop	rsi
	mov	rbx, rax
	push	rsi
	push	r11
	push	rdi
	push	r9
	push	r8
	push	r10
	add	rsp, 48
	mov	rdi, rbx
	sub	rsp, 48
	sub	rsp, 8
	call _builtin_print
	add	rsp, 8
	pop	r10
	pop	r8
	pop	r9
	pop	rdi
	pop	r11
	pop	rsi
	mov	rax, 0
main_39_exit:
	add	rsp, 1296
	pop	rbx
	pop	r15
	pop	r14
	pop	rbp
	ret

print_Int:
     mov                  rsi,                  rdi
     mov                  rdi,    __print_IntFormat
     sub                  rsp,                    8
    call               printf
     add                  rsp,                    8
     ret
println_Int:
     mov                  rsi,                  rdi
     mov                  rdi,  __println_IntFormat
     sub                  rsp,                    8
    call               printf
     add                  rsp,                    8
     ret
_builtin_print:
     mov                  rsi,                  rdi
     mov                  rdi, ___builtin_printFormat
     sub                  rsp,                    8
    call               printf
     add                  rsp,                    8
     ret
_builtin_println:
     sub                  rsp,                    8
    call                 puts
     add                  rsp,                    8
     ret
_builtin_getInt:
     mov                  rdi,       __getIntFormat
     mov                  rsi,           @getIntBuf
     sub                  rsp,                    8
    call                scanf
     add                  rsp,                    8
     mov                  rax,   qword [@getIntBuf]
     ret
_builtin_getString:
    push                  r15
     mov                  rdi,                  300
    call               malloc
     mov                  r15,                  rax
     add                  r15,                    8
     mov                  rdi,    __getStringFormat
     mov                  rsi,                  r15
    call                scanf
     mov                  rdi,                  r15
    call               strlen
     mov      qword [r15 - 8],                  rax
     mov                  rax,                  r15
     pop                  r15
     ret
_builtin_toString:
    push                  r15
    push                  rdi
     mov                  rdi,                   20
     sub                  rsp,                    8
    call               malloc
     add                  rsp,                    8
     mov                  r15,                  rax
     add                  r15,                    8
     mov                  rdi,                  r15
     mov                  rsi,     __toStringFormat
     pop                  rdx
    call              sprintf
     mov                  rdi,                  r15
    call               strlen
     mov      qword [r15 - 8],                  rax
     mov                  rax,                  r15
     pop                  r15
     ret
_builtin_array_size:
     mov                  rax,      qword [rdi - 8]
     ret
_builtin_string_length:
     mov                  rax,      qword [rdi - 8]
     ret
_builtin_string_parseInt:
     mov                  rsi,       __getIntFormat
     mov                  rdx,         @parseIntBuf
     sub                  rsp,                    8
    call               sscanf
     add                  rsp,                    8
     mov                  rax, qword [@parseIntBuf]
     ret
_builtin_string_substring:
    push                  r15
    push                  r14
     mov                  r15,                  rdi
     add                  r15,                  rsi
     mov                  r14,                  rdx
     sub                  r14,                  rsi
     add                  r14,                    1
     mov                  rdi,                  r14
     add                  rdi,                    9
     sub                  rsp,                    8
    call               malloc
     add                  rsp,                    8
     add                  rax,                    8
     mov                  rdi,                  rax
     mov                  rsi,                  r15
     mov                  rdx,                  r14
     sub                  rsp,                    8
    call               memcpy
     add                  rsp,                    8
     mov      qword [rax - 8],                  r14
     mov                  r15,                  rax
     add                  r15,                  r14
     mov                  r15,                    0
     pop                  r14
     pop                  r15
     ret
_builtin_string_ord:
     add                  rdi,                  rsi
   movsx                  rax,           byte [rdi]
     ret
_builtin_string_concatenate:
    push                  r15
    push                  r14
    push                  r13
     mov                  r15,      qword [rdi - 8]
     add                  r15,      qword [rsi - 8]
     add                  r15,                    9
     mov                  r14,                  rdi
     mov                  r13,                  rsi
     mov                  rdi,                  r15
    call               malloc
     sub                  r15,                    9
     mov          qword [rax],                  r15
     mov                  r15,                  rax
     add                  r15,                    8
     mov                  rdi,                  r15
     mov                  rsi,                  r14
    call               strcpy
     add                  r15,      qword [r14 - 8]
     mov                  r14,                  rax
     mov                  rdi,                  r15
     mov                  rsi,                  r13
    call               strcpy
     mov                  rax,                  r14
     pop                  r13
     pop                  r14
     pop                  r15
     ret
_builtin_string_equal:
     sub                  rsp,                    8
    call               strcmp
     add                  rsp,                    8
     cmp                  eax,                    0
     mov                  rax,                    0
    sete                   al
     ret
     sub                  rsp,                    8
    call               strcmp
     add                  rsp,                    8
     cmp                  eax,                    0
     mov                  rax,                    0
   setne                   al
     ret
_builtin_string_greater:
     sub                  rsp,                    8
    call               strcmp
     add                  rsp,                    8
     cmp                  eax,                    0
     mov                  rax,                    0
    setg                   al
     ret
_builtin_string_notless:
     sub                  rsp,                    8
    call               strcmp
     add                  rsp,                    8
     cmp                  eax,                    0
     mov                  rax,                    0
   setge                   al
     ret
_builtin_string_less:
     sub                  rsp,                    8
    call               strcmp
     add                  rsp,                    8
     cmp                  eax,                    0
     mov                  rax,                    0
    setl                   al
     ret
_builtin_string_notgreater:
     sub                  rsp,                    8
    call               strcmp
     add                  rsp,                    8
     cmp                  eax,                    0
     mov                  rax,                    0
   setle                   al
     ret

SECTION .data
__println_IntFormat:
      db         "%ld", 10, 0
__print_IntFormat:
      db             "%ld", 0
___builtin_printFormat:
      db              "%s", 0
__getIntFormat:
      db             "%ld", 0
__getStringFormat:
      db              "%s", 0
__toStringFormat:
      db             "%ld", 0
__parseIntFormat:
      db             "%ld", 0

SECTION .bss
@getIntBuf:
    resq                    1
@parseIntBuf:
    resq                    1
